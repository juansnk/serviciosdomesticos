<%-- 
    Document   : esperarConfirmacion
    Created on : 6/11/2020, 06:29:19 PM
    Author     : Admin
--%>

<%@include file="sesiones.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Webstrot Admin : Invoice</title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
    <link href="CSS/lib/themify-icons.css" rel="stylesheet">
    <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
    <link href="CSS/lib/unix.css" rel="stylesheet">
    <link href="CSS/styleee.css" rel="stylesheet">
    </head>

    <body class="bg-success">

        <div class="unix-login">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="index.html"><span><img src="Imagenes/logoBlanco.png" alt="logo" height="50" ></span></a>
                        </div>
                        <div class="login-form">
                            <p class="nombrePa" style="text-align: center">Confirma tu registro</p>
                            <p class="introPa" style="text-align: center">Dirígete al correo con el que te registraste</p>
                             <a href="https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin"  class="btn btn-warning btn-flat m-b-10 m-t-10">Gmail</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>

</html>

