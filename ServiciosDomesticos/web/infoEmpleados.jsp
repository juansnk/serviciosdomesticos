<%@page import="modeloDAO.EmpleadoDAO"%>
<%@page import="modeloVO.EmpleadoVO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modeloDAO.UsuarioDAO"%>
<%@page import="modeloVO.UsuarioVO"%>
<%@include file="sesiones.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Webstrot Admin : Student Profile</title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link href="CSS/lib/calendar/fullcalendar.css" rel="stylesheet" />
        <link href="CSS/lib/sweetalert/sweetalert.css" rel="stylesheet">
        <link href="CSS/lib/menubar/sidebar.css" rel="stylesheet">
        <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/lib/unix.css" rel="stylesheet">
        <link href="CSS/styleee.css" rel="stylesheet">
    </head>

    <body>
        <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <ul>
                        <li class="label">Principal</li>
                        <li class="active"><a href="indexCliente.jsp"><i class="ti-home"></i> Inicio  </a></li>
                        <li class="label">Citas</li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i>Listas <span class="badge badge-primary">3</span><span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="citasActivas.jsp">Activas</a></li>
                                <li><a href="citasInactivas.jsp">Terminadas</a></li>
                                <li><a href="citasCanceladas.jsp">Canceladas</a></li>   
                            </ul>
                        </li>

                        <li class="label">Empleados</li>
                        <li><a href="buscarEmpleada.jsp"><i class="ti-search"></i> Busca tu empleada </a></li>

                        <li class="label">Informacion</li>
                        <li><a href="calendarioCliente.jsp"><i class="ti-calendar"></i> Calendario </a></li>
                        <li><a href="perfilCliente.jsp"><i class="ti-user"></i> Perfil</a></li>

                        <li><a href="Sesiones"><i class="ti-close"></i> Cerrar sesion</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /# sidebar -->

        <div class="header">
            <div class="pull-left">
                <div class="logo"><a href="indexCliente.jsp"><span><img src="Imagenes/logoNegro.png" height="50" /></span></a></div><div class="hamburger sidebar-toggle">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>
            </div>
            <div class="pull-right p-r-15">
                <ul>
                    <li class="header-icon dib"><img class="avatar-img" src="assets/images/avatar/1.jpg" alt="" /> <span class="user-avatar"> <%=usuario%> <i class="ti-angle-down f-s-10"></i></span>
                        <div class="drop-down dropdown-profile">
                            <div class="dropdown-content-body">
                                <ul>
                                    <li><a href="perfilCliente.jsp"><i class="ti-user"></i> <span>Perfil</span></a></li>
                                    <li><a href="calendarioCliente.jsp"><i class="ti-calendar"></i> <span>Calendario</span></a></li>
                                    <li><a href="modificarContraseñaCliente.jsp"><i class="ti-lock"></i> <span>Cambiar contraseña</span></a></li>
                                    <li><a href="Sesiones"><i class="ti-power-off"></i> <span>Cerrar sesión</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="content-wrap">
            <div class="main">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-8 p-r-0 title-margin-right">
                            <div class="page-header">
                                <div class="page-title">
                                    <h1>Información empleado</h1>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                        <div class="col-lg-4 p-l-0 title-margin-left">
                            <div class="page-header">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="indexCliente.jsp">Inicio</a></li>
                                        <li><a href="buscarEmpleada.jsp.jsp">Buscar empleado</a></li>
                                       <li class="active">Información empleado</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                    <div id="main-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header">
                                        <h4>Perfil empleada</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="user-profile m-t-15">
                                            <div class="row">
                                                <% UsuarioVO usuVO = (UsuarioVO) request.getAttribute("Empleados");
                                                    if (usuVO != null) {%>
                                                <div class="col-lg-4">
                                                    <div class="user-photo m-b-30">
                                                        <img class="img-responsive" src="ImagenControlador?opcionImg=7&idUsuario=<%=usuVO.getIdUsuario()%> " alt="" width="400" height="400" />
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-8">
                                                    <div class="user-profile-name"><%=usuVO.getUsuNombre()%> <%=usuVO.getUsuApellido()%></div>
                                                    <div class="custom-tab user-profile-tab">
                                                        <ul class="nav nav-tabs" role="tablist">
                                                            <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">Informacion personal</a></li>
                                                            <li role="presentation"><a href="#2" aria-controls="2" role="tab" data-toggle="tab">Contacto</a></li>
                                                            <li role="presentation"><a href="#3" aria-controls="3" role="tab" data-toggle="tab">Experiencia</a></li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane active" id="1">
                                                                <div class="contact-information">
                                                                    <div class="phone-content">
                                                                        <span class="contact-title">Nombre:</span>
                                                                        <span class="phone-number"><%=usuVO.getUsuNombre()%></span>
                                                                    </div>
                                                                    <div class="website-content">
                                                                        <span class="contact-title">Apellido:</span>
                                                                        <span class="contact-website"><%=usuVO.getUsuApellido()%></span>
                                                                    </div>
                                                                    <div class="skype-content">
                                                                        <span class="contact-title">Ciudad:</span>
                                                                        <span class="contact-skype"><%=usuVO.getUsuCiudad()%></span>
                                                                    </div>
                                                                    <div class="skype-content">
                                                                        <span class="contact-title">Años:</span>
                                                                        <span class="contact-skype"><%=usuVO.getUsuFecha()%> años</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane" id="2">
                                                                <div class="contact-information">
                                                                    <div class="phone-content">
                                                                        <span class="contact-title">Correo:</span>
                                                                        <span class="phone-number"><%=usuVO.getUsuCorreo()%></span>
                                                                    </div>
                                                                    <div class="website-content">
                                                                        <span class="contact-title">Telefono:</span>
                                                                        <span class="contact-website"><%=usuVO.getUsuTelefono()%></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane" id="3">
                                                                <div class="contact-information">
                                                                    <div class="phone-content">
                                                                        <span class="contact-title">Experiencia laboral:</span>
                                                                        <span class="phone-number"><%=usuVO.getEmpExperiencia()%></span>
                                                                    </div>
                                                                    <div class="website-content">
                                                                        <span class="contact-title">Descripcion:</span>
                                                                        <span class="contact-website"><%=usuVO.getEmpDescripcion()%></span>
                                                                    </div>
                                                                    <div class="website-content">
                                                                        <span class="contact-title">Promedio:</span>      

                                                                                <span><%=usuVO.getEmpPromedio()%></span>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <%}%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /# column -->
                            </div>
                            <!-- /# row -->
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="card alert">
                                        <div class="card-header">
                                            <h4>Disponibilidad </h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Dias</th>
                                                            <th>Hora de inicio</th>
                                                            <th>Hora de fin</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <%
                                                            UsuarioVO usuuVO = new UsuarioVO();
                                                            UsuarioDAO usuDAO = new UsuarioDAO();
                                                            ArrayList<UsuarioVO> listaHoras = usuDAO.listaDias(usuVO.getIdAgenda());
                                                            for (int i = 0; i < listaHoras.size(); i++) {
                                                                usuuVO = listaHoras.get(i);
                                                        %>
                                                        <tr>
                                                            <td><span class="badge badge-primary"><%=usuuVO.getProDia()%></span></td>
                                                            <td><%=usuuVO.getProHoraInicio()%></td>
                                                            <td style="text-align: center"><%=usuuVO.getProHoraFin()%></td>
                                                        </tr>
                                                        <%}%>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /# column -->
                                <div class="col-lg-6">
                                    <div class="card alert">
                                        <div class="card-header">
                                            <h4>Comentarios </h4>
                                        </div>
                                        <div class="recent-comment m-t-20">
                                            <%
                                                EmpleadoVO empVO = new EmpleadoVO();
                                                EmpleadoDAO empDAO = new EmpleadoDAO();
                                                ArrayList<EmpleadoVO> listaOpi = empDAO.listarOpiniones(usuVO.getIdEmpleado());
                                                for (int i = 0; i < listaOpi.size(); i++) {
                                                    empVO = listaOpi.get(i);
                                            %>
                                            <div class="media">
                                                <div class="media-left">
                                                    <a href="#"><img class="media-object" src="ImagenControlador?opcionImg=3&idUsuario=<%=empVO.getIdUsuario()%>" alt="..." width="70px" height="70px"></a>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading"><%=empVO.getUsuNombre()%> <%=empVO.getUsuApellido()%></h4>
                                                    <p><%=empVO.getCitOpinion()%></p>
                                                    <div class="comment-action">
                                                        <div class="badge badge-warning">Terminado</div>
                                                        <span class="m-l-10">
                                                            <a href="#"><i class="fa fa-reply color-primary"></i></a>
                                                        </span>
                                                    </div>
                                                    <p class="comment-date"><%=empVO.getCitFecha()%></p>
                                                </div>                                                 
                                            </div>
                                            <%}%>
                                        </div>
                                    </div>
                                    <!-- /# card -->
                                </div>
                                <!-- /# column -->
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card alert">
                                        <div class="card-header">
                                            <h4>Solicita tu cita</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="container">
                                                    <div class="abs-center">
                                                        <div class="col-md-9">
                                                            <div class="card-box">
                                                                <%
                                                        EmpleadoVO emppVO= new EmpleadoVO();
                                                        ArrayList<EmpleadoVO> listaCitasActivas = empDAO.listarCitasActivasEmpleada(empVO.getIdEmpleado());
                                                        for (int i = 0; i < listaCitasActivas.size(); i++) {
                                                            emppVO = listaCitasActivas.get(i);
                                                    %>
                                                    <input type="hidden"  name="id" value="<%=emppVO.getIdCita()%>">
                                                    <input type="hidden" name="fecha" value="<%=emppVO.getCitFecha()%>">
                                                    <input type="hidden" name="nombre" value="<%=emppVO.getUsuNombre()%> <%=emppVO.getUsuApellido()%>"> 
                                                    <input type="hidden"  name="horaInicio" value="<%=emppVO.getCitHoraInicio()%>">
                                                    <input type="hidden"  name="horaFin" value="<%=emppVO.getCitHoraFin()%>"><br>
                                                    <%}%>
                                                                <div id="calendar"></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- end col -->
                                                <!-- BEGIN MODAL -->
                                                <form method='post' action='Cita' onsubmit="return validarCita()">
                                                    <div class="modal fade none-border" id="event-modal">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                    <h4 class="modal-title"><strong>Nueva cita</strong></h4>
                                                                </div>
                                                                <div class="modal-header">
                                                                    <div class='row'>
                                                                        <div class='col-md-6'>
                                                                            <div class='form-group'>
                                                                                <label class='control-label'>Fecha</label>
                                                                                <input id="fecha" type="date" class="form-control" name="textFecha" value="">                                             
                                                                            </div>
                                                                        </div>
                                                                        <div class='col-md-6'>
                                                                            <div class='form-group'>
                                                                                <label class='control-label'>Direccion</label>
                                                                                <input class='form-control' placeholder='Ej: calle 18 #10M-20 2 piso' type='text' name='textDireccion' id="direccion"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class='col-md-6'>
                                                                            <div class='form-group'>
                                                                                <label class='control-label'>Hora de inicio</label>
                                                                                <input id="horaInicio" class='form-control' type='time' name='textHoraInicio' value=""/>
                                                                            </div>
                                                                        </div>
                                                                        <div class='col-md-6'>
                                                                            <div class='form-group'>
                                                                                <label class='control-label'>Hora de fin</label>
                                                                                <input id="horaFin" class='form-control'type='time' name='textHoraFin' value=""/>
                                                                            </div>
                                                                        </div>
                                                                        <input class='form-control'type='hidden' name='textEstado' value='4'/>
                                                                        <input type="hidden" name="textIdCliente" value="<%=idCliente%>">
                                                                        <input type="hidden" name="textIdAgenda" value="<%=usuVO.getIdAgenda()%>">
                                                                        <input type="hidden" name="textIdUsuario" value="<%=usuVO.getIdUsuario()%>">
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <a href="#" type="button" class="btn btn-default waves-effect" data-dismiss="modal" onclick="recargar()">Cerrar</a>
                                                                    <button class="btn btn-success save-event waves-effect waves-light">Solicitar</button>

                                                                    <input type="hidden" name="opcion" value="1">
                                                                    <script>
                                                                        window.onload = function () {
                                                                        <% if (request.getAttribute("mensajeError") != null) {%>
                                                                            swal("Espera!", "<%=request.getAttribute("mensajeError")%>", "warning");
                                                                        <% }%>
                                                                        };
                                                                    </script>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /# card -->
                                </div>
                                <!-- /# column -->
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="footer">
                                        <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="search">
                <button type="button" class="close">×</button>
                <form>
                    <input type="search" value="" placeholder="type keyword(s) here" />
                    <button type="submit" class="btn btn-primary">Search</button>
                </form>
            </div>

            <script>
                function recargar() {
                    location.reload();
                }
            </script>
            <script src="js/lib/rating1/jRate.min.js"></script>
            <!-- scripit init-->
            <script src="js/lib/rating1/jRate.init.js"></script>
            <script src="js/validaciones/cita.js" type="text/javascript"></script>
            <!-- jquery vendor -->
            <script src="js/lib/jquery.min.js"></script>
            <script src="js/lib/jquery.nanoscroller.min.js"></script>
            <!-- nano scroller -->
            <script src="js/lib/menubar/sidebar.js"></script>
            <script src="js/lib/preloader/pace.min.js"></script>
            <!-- sidebar -->
            <script src="js/lib/bootstrap.min.js"></script>
            <!-- bootstrap -->
            <script src="js/lib/jquery-ui/jquery-ui.min.js"></script>
            <script src="js/lib/moment/moment.js"></script>
            <script src="js/lib/sweetalert/sweetalert.min.js"></script>
            <script src="js/lib/calendar/fullcalendar.min.js"></script>
            <script src="js/lib/calendar/fullcalendar-init.js"></script>
            <script src="js/lib/calendar/es.js" type="text/javascript"></script>
            <script src="js/lib/calendar/locales-all.js" type="text/javascript"></script>
            <script src="js/scripts.js"></script>
            <!-- scripit init-->



    </body>

</html>