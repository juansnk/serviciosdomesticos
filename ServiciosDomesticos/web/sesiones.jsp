<%-- 
    Document   : sesiones
    Created on : 24/06/2020, 05:24:11 PM
    Author     : Admin
--%>

<%@page import="modeloVO.UsuarioVO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    response.setHeader("Pragma", "No-cache");
    response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
    response.setDateHeader("Expires", 0);
%>
<% 
    HttpSession miSesion =(HttpSession)request.getSession();
    String idUsuario="", nombre="", apellido="", identificacion="", nacimiento="", ciudad="", correo="", telefono="", usuario="",
            contrasena="", idEmpleado="", experiencia="", promedio="", descripcion="", idCliente="",direccion="",localidad="", barrio="",
            rol="";
    if(miSesion.getAttribute("datosUsuario")== null){
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }else{
         UsuarioVO usuVO = (UsuarioVO)miSesion.getAttribute("datosUsuario");
         idUsuario = usuVO.getIdUsuario();
         nombre = usuVO.getUsuNombre();
         apellido = usuVO.getUsuApellido();
         identificacion = usuVO.getUsuIdentificacion();
         nacimiento = usuVO.getUsuFecha();
         ciudad = usuVO.getUsuCiudad();
         correo = usuVO.getUsuCorreo();
         telefono = usuVO.getUsuTelefono();
         usuario = usuVO.getUsuario();
         contrasena= usuVO.getContrasena();
         idEmpleado = usuVO.getIdEmpleado();
         experiencia = usuVO.getEmpExperiencia();
         promedio = usuVO.getEmpPromedio();
         descripcion = usuVO.getEmpDescripcion();
         idCliente = usuVO.getIdCliente();
         direccion = usuVO.getCliDireccion();
         localidad = usuVO.getCliLocalidad();
         barrio = usuVO.getCliBarrio();
         rol = usuVO.getRol_idRol();
         
    }

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ServiHogar</title>
    </head>
    <body>
        
    </body>
</html>
