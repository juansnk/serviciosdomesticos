<%-- 
    Document   : infoCitaInactiva
    Created on : 15/09/2020, 10:45:04 AM
    Author     : Admin
--%>

<%@page import="modeloVO.CitaServicioVO"%>
<%@page import="modeloDAO.CitaServicioDAO"%>
<%@page import="modeloVO.CitaServicioVO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modeloDAO.CitaDAO"%>
<%@page import="modeloVO.CitaVO"%>
<%@include file="sesiones.jsp" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Webstrot Admin : Student Details</title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link href="CSS/lib/menubar/sidebar.css" rel="stylesheet">
        <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/lib/unix.css" rel="stylesheet">
        <link href="CSS/styleee.css" rel="stylesheet">
    </head>

    <body>
        <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <ul>
                        <li class="label">Principal</li>
                        <li class="active"><a href="indexCliente.jsp"><i class="ti-home"></i> Inicio  </a></li>
                        <li class="label">Citas</li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i>Listas <span class="badge badge-primary">3</span><span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="citasActivas.jsp">Activas</a></li>
                                <li><a href="citasInactivas.jsp">Terminadas</a></li>
                                <li><a href="citasCanceladas.jsp">Canceladas</a></li>   
                            </ul>
                        </li>

                        <li class="label">Empleados</li>
                        <li><a href="buscarEmpleada.jsp"><i class="ti-search"></i> Busca tu empleada </a></li>

                        <li class="label">Informacion</li>
                        <li><a href="calendarioCliente.jsp"><i class="ti-calendar"></i> Calendario </a></li>
                        <li><a href="perfilCliente.jsp"><i class="ti-user"></i> Perfil</a></li>

                        <li><a href="Sesiones"><i class="ti-close"></i> Cerrar sesion</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /# sidebar -->

        <div class="header">
            <div class="pull-left">
                <div class="logo"><a href="indexCliente.jsp"><span><img src="Imagenes/logoNegro.png" height="50" /></span></a></div><div class="hamburger sidebar-toggle">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>
            </div>
            <div class="pull-right p-r-15">
                <ul>
                    <li class="header-icon dib"><img class="avatar-img" src="assets/images/avatar/1.jpg" alt="" /> <span class="user-avatar"> <%=usuario%> <i class="ti-angle-down f-s-10"></i></span>
                        <div class="drop-down dropdown-profile">
                            <div class="dropdown-content-body">
                                <ul>
                                    <li><a href="perfilCliente.jsp"><i class="ti-user"></i> <span>Perfil</span></a></li>
                                    <li><a href="calendarioCliente.jsp"><i class="ti-calendar"></i> <span>Calendario</span></a></li>
                                    <li><a href="modificarContraseñaCliente.jsp"><i class="ti-lock"></i> <span>Cambiar contraseña</span></a></li>
                                    <li><a href="Sesiones"><i class="ti-power-off"></i> <span>Cerrar sesión</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="content-wrap">
            <div class="main">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-8 p-r-0 title-margin-right">
                            <div class="page-header">
                                <div class="page-title">
                                    <h1>Inicio</h1>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                        <div class="col-lg-4 p-l-0 title-margin-left">
                            <div class="page-header">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="indexCliente.jsp">Inicio</a></li>
                                        <li class="active">Informacion cita</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                    <div id="main-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header">
                                        <h4>Informacion citas terminadas</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="user-profile m-t-15">
                                            <div class="row">
                                                <% CitaVO citVO = (CitaVO) request.getAttribute("idCita");
                                                    if (citVO != null) {
                                                %>
                                                <div class="col-lg-4">
                                                    <div class="user-photo m-b-30">
                                                        <img class="img-responsive" src="ImagenControlador?opcionImg=2&idCita=<%=citVO.getIdCita()%>" alt="" />
                                                    </div>
                                                </div>

                                                <div class="col-lg-8">
                                                    <div class="user-profile-name dib"><%=citVO.getUsuNombre()%> <%=citVO.getUsuApellido()%></div>
                                                    <div class="custom-tab user-profile-tab">
                                                        <ul class="nav nav-tabs" role="tablist">
                                                            <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">Informacion</a></li>
                                                            <li role="presentation"><a href="#2" aria-controls="1" role="tab" data-toggle="tab">Servicios</a></li>
                                                            <li role="presentation"><a href="#3" aria-controls="1" role="tab" data-toggle="tab">Calificacion</a></li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane active" id="1">

                                                                <div class="contact-information">
                                                                    <div class="phone-content">
                                                                        <span class="contact-title"><strong>Nombre:</strong></span>
                                                                        <span class="phone-number"><%=citVO.getUsuNombre()%></span>
                                                                    </div>
                                                                    <div class="website-content">
                                                                        <span class="contact-title"><strong>Apellido:</strong></span>
                                                                        <span class="contact-website"><%=citVO.getUsuApellido()%></span>
                                                                    </div>
                                                                    <div class="skype-content">
                                                                        <span class="contact-title"><strong>Ciudad:</strong></span>
                                                                        <span class="contact-skype"><%=citVO.getUsuCiudad()%></span>
                                                                    </div>
                                                                    <div class="address-content">
                                                                        <span class="contact-title"><strong>Telefono:</strong></span>
                                                                        <span class="mail-address"><%=citVO.getUsuTelefono()%></span>
                                                                    </div>
                                                                    <div class="gender-content">
                                                                        <span class="contact-title"><strong>Fecha de cita:</strong></span>
                                                                        <span class="gender"><%=citVO.getCitFecha()%></span>
                                                                    </div>
                                                                    <div class="phone-content">
                                                                        <span class="contact-title"><strong>Dia:</strong></span>
                                                                        <span class="phone-number"><%=citVO.getProDia()%></span>
                                                                    </div>
                                                                    <div class="phone-content">
                                                                        <span class="contact-title"><strong>Direccion:</strong></span>
                                                                        <span class="phone-number"><%=citVO.getCitDireccion()%></span>
                                                                    </div>
                                                                    <div class="birthday-content">
                                                                        <span class="contact-title"><strong>Hora de inicio:</strong></span>
                                                                        <span class="birth-date"><%=citVO.getCitHoraInicio()%></span>
                                                                    </div>
                                                                    <div class="phone-content">
                                                                        <span class="contact-title"><strong>Hora de Fin:</strong></span>
                                                                        <span class="phone-number"><%=citVO.getCitHoraFin()%></span>
                                                                    </div>
                                                                    <div class="phone-content">
                                                                        <span class="contact-title"><strong>Total precio:</strong></span>
                                                                        <span class="phone-number"><%=citVO.getCitPrecio()%></span>
                                                                    </div>
                                                                </div>
                                                                <%}%>
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane" id="2">
                                                                <div class="contact-information">
                                                                    <div class="phone-content">
                                                                        <span class="contact-title"><strong>Servicios:</strong></span>
                                                                        <%
                                                                            CitaServicioVO cisVO = new CitaServicioVO();
                                                                            CitaServicioDAO cisDAO = new CitaServicioDAO();
                                                                            ArrayList<CitaServicioVO> listaServicios = cisDAO.listarServiciosCitas(citVO.getIdCita());
                                                                            for (int i = 0; i < listaServicios.size(); i++) {
                                                                                cisVO = listaServicios.get(i);

                                                                        %> 
                                                                        <ul>
                                                                            <li><%=cisVO.getSerDescripcion()%></li>
                                                                        </ul> 
                                                                        <%}%>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane" id="3">
                                                                <div class="contact-information">
                                                                    <div class="phone-content">
                                                                        <% CitaVO cittVO = (CitaVO) request.getAttribute("idCitaCalificacion");
                                                                            if (cittVO != null) {
                                                                        %>

                                                                        <div class="phone-content">
                                                                            <span class="contact-title"><strong>Opinion:</strong></span>
                                                                            <span class="phone-number"><%=cittVO.getCitOpinion()%></span>
                                                                        </div>
                                                                        <div class="phone-content">                                                                          
                                                                            <span class="contact-title" style="float: left"><strong>Calificacion:</strong></span>
                                                                            <div class="rating1">
                                                                                <div id="read-only-stars"></div>
                                                                                <input value="<%=cittVO.getCitCalificacion()%>" id="calificacion" type="hidden">
                                                                            </div>
                                                                        </div>

                                                                        <%}%>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col text-center">
                                                            <a href="citasInactivas.jsp" class="btn-lg btn-default">Volver</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->
                        </div>
                        <!-- /# row -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="footer">
                                    <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>







        <div id="search">
            <button type="button" class="close">×</button>
            <form>
                <input type="search" value="" placeholder="type keyword(s) here" />
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
        <!-- jquery vendor -->
        <script src="js/lib/jquery.min.js"></script>
        <script src="js/lib/jquery.nanoscroller.min.js"></script>
        <!-- nano scroller -->
        <script src="js/lib/menubar/sidebar.js"></script>
        <script src="js/lib/preloader/pace.min.js"></script>
        <!-- sidebar -->
        <script src="js/lib/bootstrap.min.js"></script>
        <!-- bootstrap -->
        <script src="js/lib/rating1/jRate.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/rating1/jRate.init.js"></script>

        <script src="js/scripts.js"></script>
        <!-- scripit init-->





    </body>

</html>
