<%-- 
    Document   : facturaCita
    Created on : 23/10/2020, 08:30:48 PM
    Author     : Admin
--%>

<%@page import="modeloVO.CitaVO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modeloDAO.CitaServicioDAO"%>
<%@page import="modeloVO.CitaServicioVO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="sesiones.jsp" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Webstrot Admin : Invoice</title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/lib/unix.css" rel="stylesheet">
        <link href="CSS/styleee.css" rel="stylesheet">
    </head>

    <body class="bg-primary">

        <div class="unix-invoice">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="invoice" class="effect2 m-t-120">
                            <div id="invoice-top">
                                <div class="invoice-logo"></div>
                                <div class="invoice-info">
                                    <h2><%=nombre%> <%=apellido%></h2>
                                    <p> <%=correo%> <br> <%=telefono%>
                                    </p>
                                </div>
                                <!--End Info-->
                                <div class="title">
                                    <h4>Total a pagar</h4>
                                    <p>Recuerda que el pago debe ser presencial</p>
                                    <p>cuando el empleado halla culminado sus servicios</p>
                                </div>
                                <!--End Title-->
                            </div>
                            <!--End InvoiceTop-->



                            <div id="invoice-mid">
                                 <%CitaVO citVO = (CitaVO) request.getAttribute("facturaCita");
                                            if(citVO != null){
                                                
                                 %>

                                 <div class="invoice-info" style="text-align: center">
                                    <img src="ImagenControlador?opcionImg=3&idUsuario=<%=citVO.getIdUsuario()%>" alt="" width="100px" height="100px" style="float: left;margin-left: 10px">
                                    <h2><%=citVO.getUsuNombre()%> <%=citVO.getUsuApellido()%></h2>
                                    <p><%=citVO.getUsuCorreo()%><br> <%=citVO.getUsuTelefono()%>
                                        <br>
                                </div>

                            </div>
                            <!--End Invoice Mid-->

                            <div id="invoice-bot">

                                <div id="invoice-table">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr class="tabletitle">
                                                <td class="table-item">
                                                    <h2>Descripcion de la cita</h2>
                                                </td>
                                                <td class="Hours">
                                                    <h2>Precio</h2>
                                                </td>
                                                <td class="subtotal">
                                                    <h2>Sub-total</h2>
                                                </td>
                                            </tr>
                                            <%                                           
                                            CitaServicioVO cisVO= new CitaServicioVO();
                                            CitaServicioDAO cisDAO = new CitaServicioDAO(); 
                                            ArrayList<CitaServicioVO>listaServiciosFac = cisDAO.listarServiciosFactura(citVO.getIdCita());
                                            for(int i=0; i < listaServiciosFac.size();i++){
                                                cisVO = listaServiciosFac.get(i);
                                            %>
                                            <tr class="service">
                                                <td class="tableitem">
                                                    <p class="itemtext"><%=cisVO.getSerDescripcion()%></p>
                                                </td>
                                                <td class="tableitem">
                                                    <p class="itemtext">$<%=cisVO.getCisCosto()%></p>
                                                </td>
                                                <td class="tableitem">
                                                    <p class="itemtext">$<%=cisVO.getCisCosto()%></p>
                                                </td>
                                            </tr>
                                            <%}%>
                                            <tr class="service">
                                                <td class="tableitem">
                                                    <p class="itemtext">Empleado</p>
                                                </td>
                                                <td class="tableitem">
                                                    <p class="itemtext">$<%=citVO.getEmpPrecio()%></p>
                                                </td>
                                                <td class="tableitem">
                                                    <p class="itemtext">$<%=citVO.getEmpPrecio()%></p>
                                                </td>
                                            </tr>

                                            <tr class="tabletitle">
                                                <td></td>
                                                <td class="Rate">
                                                    <h2>Total</h2>
                                                </td>
                                                <td class="payment">
                                                    <h2>$<%=citVO.getCitPrecio()%></h2>
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </div>
                                </div>
                                <!--End Table-->
                    <div class="container text-center  d-flex justify-content-center align-items-center">
                                <div class="col-xs-5">
                                    <form action="Cita" method="post">
                                         <input type='hidden' name='textId' value='<%=citVO.getIdCita()%>'/>                                        
                                        <button class="btn-lg btn-danger">Cancelar</button>
                                        <input type="hidden" name="opcion" value="11">
                                    </form>
                                </div>
                                <div class="col-xs-5">
                                    <form action="Cita" method="post">
                                        <input type='hidden' name='textId' value='<%=citVO.getIdCita()%>'/>
                                        <input class='form-control'type='hidden' name='textEstado' value='3'/>
                                        <button class="btn-lg btn-success">Aceptar</button>
                                        <input type="hidden" name="correo" value="<%=citVO.getUsuCorreo()%>">
                                        <input type="hidden" name="tipoCorreo" value="2">
                                        <input type="hidden" name="opcion" value="11">
                                    </form>
                                    <%}%>
                                </div>
                            </div>
                            </div>
                            <!--End InvoiceBot-->

                            
                        </div>

                        <!--End Invoice-->
                    </div>
                </div>
            </div>
        </div>

    </body>

</html>