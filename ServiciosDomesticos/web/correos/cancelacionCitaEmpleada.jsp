<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta name="x-apple-disable-message-reformatting">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="telephone=no" name="format-detection">
        <title></title>
    </head>
    <body>
        <div >
            <table width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td valign="top">
                            <table cellpadding="0" cellspacing="0" align="center" >
                                <tbody>
                                    <tr>
                                        <td  align="center" bgcolor="#f8f9fd" style="background-color: #f8f9fd;">
                                            <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600">
                                                <tbody>
                                                    <tr>
                                                        <td  align="left">
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tbody>
                                                                    <tr>
                                                                        <td width="540"  align="center" valign="top">
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td align="center" class="esd-block-image" style="font-size: 0px;"><a target="_blank"><img src="https://demo.stripocdn.email/content/guids/a7c0e2eb-81f6-4c6a-8358-fceec96dc133/images/99351604618066628.png" alt style="display: block;" width="230"></a></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table cellpadding="0" cellspacing="0" class="es-content" align="center">
                                <tbody>
                                    <tr>
                                        <td align="center" bgcolor="#f8f9fd" style="background-color: #f8f9fd;">
                                            <table bgcolor="transparent" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="background: white" >
                                                <tbody>
                                                    <tr>
                                                        <td align="left">
                                                            <table cellpadding="0" cellspacing="0" width="100%" >
                                                                <tbody>
                                                                    <tr>
                                                                        <td width="560" align="center" valign="top">
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td align="center" >
                                                                                            <h1 style="font-size: 48px; font-family: roboto, 'helvetica neue'">Lamento notificar</h1>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <p style="font-size: 30px; font-family: roboto, 'helvetica neue'; ">Tu empleada ha cancelado tu cita <br><span style="font-size:15px;">Inicia sesi�n y averigua la raz�n</span><br></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table align="center"  style="background: #20c997; width: 100%; font-family: roboto, 'helvetica neue';">
                                <tbody>
                                    <tr>
                                        <td class="esd-stripe" align="center" bgcolor="#20c997" style="background-color: #20c997;">
                                            <table bgcolor="rgba(0, 0, 0, 0)" class="es-content-body" align="center"  width="600" style="background: #20c997; margin-top: 30px; margin-bottom: 30px;">
                                                <tbody>
                                                    <tr>
                                                        <td align="left" >
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tbody >
                                                                    <tr>
                                                                        <td width="600"align="center" valign="top">
                                                                            <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#f0f3fe" style="background-color: #f0f3fe; border-radius: 20px; border-collapse: separate;">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <h1 style="text-align: center; line-height: 150%;">Inicia sesi�n y dir�jase al apartado de citas canceladas</h1>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center" style="padding: 25px;"><span style="background: #00C9FB; border-radius: 5px; padding: 10px" ><a href="http://localhost:8080/ServiciosDomesticos/login.jsp" style="border-width: 10px 20px;margin-left: 20px;margin-right: 20px; margin-top: 10px; margin-bottom: 10px; text-decoration: none; color: white; font-size: 23px">Iniciar sesi�n
                                                                                                    <!--[if !mso]><!-- --><img src="https://uxyja.stripocdn.email/content/guids/CABINET_1ce849b9d6fc2f13978e163ad3c663df/images/32371592816290258.png" alt="icon" width="16" class="esd-icon-right" align="absmiddle" style="margin-left:10px;">
                                                                                                    <!--<![endif]--></a></span></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>
