<%@page import="modeloDAO.EmpleadoDAO"%>
<%@page import="modeloVO.EmpleadoVO"%>
<%@page import="modeloVO.AgendaVO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modeloDAO.ProAgendaDAO"%>
<%@page import="modeloVO.ProAgendaVO"%>
<%@include file="sesiones.jsp" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ServiHogar</title>

    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

    <!-- Styles -->
    <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
    <link href="CSS/lib/themify-icons.css" rel="stylesheet">
    <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
    <link href="CSS/lib/unix.css" rel="stylesheet">
    <link href="CSS/styleee.css" rel="stylesheet">
</head>

<body class="bg-success">

    <div class="unix-login">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="index.html"><span><img src="Imagenes/logoBlanco.png" alt="logo" height="50" ></span></a>
                        </div>
                        <div class="login-form">
                             <div class="card-header m-b-20">
                                    <h4>Registrar programacion por d�as</h4>
                                </div>
                            <p class="introPa">registrar tu disponibilidad por d�as</p>                             
                                <form method="post" action="ProAgenda">
                                    <%                        AgendaVO ageVO = (AgendaVO) request.getAttribute("proAgenda");
                                        ProAgendaVO proVO = new ProAgendaVO();
                                        ProAgendaDAO proDAO = new ProAgendaDAO();
                                        ArrayList<ProAgendaVO> listaProAgenda = proDAO.listarFK(ageVO.getIdAgenda());
                                        for (int i = 0; i < listaProAgenda.size(); i++) {
                                            proVO = listaProAgenda.get(i);
                                    %>
                                    <input type="hidden" name="textIdEmpleado" value="<%=idEmpleado%>">
                                    <button value="<%=proVO.getIdProgramacionAgenda()%>" name="textId" class="btn-dark btn-lg"><i class="ti-calendar"></i><%=proVO.getProDia()%></button>
                                    <input type="hidden" value="3" name="opcion">
                                    
                                    <%}%>
                                    <% AgendaVO ageeVO = (AgendaVO) request.getAttribute("proAgenda");
                                        if (ageeVO != null) {%>
                                    <input type="hidden" name="textIdAgenda" value="<%=ageeVO.getIdAgenda()%>">
                                    <%}%>
                                </form><br>
                                <div class="text-center">
                                <form method="post" action="ProAgenda"> 
                                    <%
                                        String idProgramacionAgenda = request.getParameter("textId");
                                        ProAgendaVO prooVO = new ProAgendaVO();
                                        ProAgendaDAO prooDAO = new ProAgendaDAO();
                                        ArrayList<ProAgendaVO> listarHoras = prooDAO.listarHoras(idProgramacionAgenda);
                                        for (int i = 0; i < listarHoras.size(); i++) {
                                            prooVO = listarHoras.get(i);
                                    %>
                                    <% ProAgendaVO progVO = (ProAgendaVO) request.getAttribute("horasDisponibilidad");
                                        if (progVO != null) {%>
                                    <input type="hidden"  value="<%=progVO.getAgenda_idAgenda()%>">
                                    <%}%>                                    
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="basic-form">
                                                <div class="form-group">
                                                    <label>Hora de Inicio</label>
                                                    <input type="time" name="textHoraInicio" class="form-control border-none input-flat bg-ash" value="<%=prooVO.getProHoraInicio()%>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="basic-form">
                                                <div class="form-group">
                                                    <label>Hora de fin</label>
                                                    <input type="time" name="textHoraFin" class="form-control border-none input-flat bg-ash" value="<%=prooVO.getProHoraFin()%>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden"  value="<%=prooVO.getIdProgramacionAgenda()%>">
                                    <input type="hidden" name="textIdEmpleado" value="<%=idEmpleado%>">
                                    <button value="<%=prooVO.getIdProgramacionAgenda()%>" name="textId" class="btn btn-primary btn-flat m-b-10 m-t-10">Registrar</button>
                                    <a href="Sesiones"  class="btn btn-warning btn-flat m-b-10 m-t-10">Volver</a>
                                    <input type="hidden" value="1" name="opcion">
                                    <%}%>
                                </form>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>