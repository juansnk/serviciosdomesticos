<%-- 
    Document   : modificarEmpleado
    Created on : 27/10/2020, 07:54:47 PM
    Author     : Admin
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="modeloDAO.EmpleadoDAO"%>
<%@page import="modeloVO.EmpleadoVO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="sesiones.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Perfil</title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link href="CSS/lib/menubar/sidebar.css" rel="stylesheet">
        <link href="CSS/lib/sweetalert/sweetalert.css" rel="stylesheet">
        <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/lib/unix.css" rel="stylesheet">
        <link href="CSS/styleee.css" rel="stylesheet">
    </head>

    <body>

        <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <ul>
                        <li class="label">Principal</li>
                        <li class="active"><a href="indexEmpleado.jsp"><i class="ti-home"></i> Inicio</a></li>
                        <li class="label">Agenda</li>
                        <li><a href="listaAgenda.jsp"><i class="ti-pencil-alt"></i>Mi agenda</a>
                        </li>
                        <li class="label">Citas</li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i>Listas <span class="badge badge-primary">3</span><span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="citasActivasEmpleado.jsp">Activas</a></li>
                                <li><a href="citasInactivasEmpleado.jsp">Terminadas</a></li>
                            </ul>
                        </li>
                        <li class="label">Informacion</li>
                        <li><a href="calendarioEmpleado.jsp"><i class="ti-calendar"></i> Calendario </a></li>                    
                        <li><a href="perfilEmpleado.jsp"><i class="ti-user"></i> Perfil</a></li>
                        <li><a href="Sesiones"><i class="ti-close"></i> Cerrar Sesión</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /# sidebar -->
        <div class="header">
            <div class="pull-left">
                <div class="logo"><a href="index.jsp"><span><img src="Imagenes/logoNegro.png" height="50" /></span></a></div>

                <div class="hamburger sidebar-toggle">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>
            </div>

            <div class="pull-right p-r-15">
                <ul>
                    <li class="header-icon dib"><i class="ti-bell"><span class="badge badge-danger">.</span></i>
                        <div class="drop-down">
                            <div class="dropdown-content-heading">
                                <span class="text-left">Notificaciones recientes</span>
                            </div>
                            <div class="dropdown-content-body">
                                <ul>
                                    <%                                        EmpleadoVO empVO = new EmpleadoVO();
                                        EmpleadoDAO empDAO = new EmpleadoDAO();
                                        ArrayList<EmpleadoVO> listaCitasNo = empDAO.listarEmpleadaNotificacion(idEmpleado);
                                        for (int i = 0; i < listaCitasNo.size(); i++) {
                                            empVO = listaCitasNo.get(i);
                                    %>
                                    <li>
                                        <a href="#">
                                            <h4>Tienes una cita nueva</h4>
                                            <div class="notification-content">
                                                <small class="notification-timestamp pull-right"></small>
                                                <div class="notification-heading"><%=empVO.getUsuNombre()%> <%=empVO.getUsuApellido()%></div>
                                                <div class="notification-text"><%=empVO.getCitFecha()%>---<%=empVO.getCitHoraInicio()%> </div>

                                                <a href="Cita?opcion=9&textId=<%=empVO.getIdCita()%>&tipoCorreo=4&correo=<%=empVO.getUsuCorreo()%>" class="btn btn-danger">Cancelar</a> <a href="Cita?textEstado=1&opcion=10&textId=<%=empVO.getIdCita()%>" class="btn btn-success">Aceptar</a> <a href="Cita?opcion=5&textId=<%=empVO.getIdCita()%>" class="btn btn-default">Ver</a>
                                            </div>
                                        </a>
                                    </li>
                                    <%}%>
                                    <li class="text-center">
                                        <a href="#" class="more-link">Cerrar</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li class="header-icon dib"><img class="avatar-img" src="Imagenes/usuario.png" alt="" /> <span class="user-avatar"> <%=usuario%> <i class="ti-angle-down f-s-10"></i></span>
                        <div class="drop-down dropdown-profile">
                            <div class="dropdown-content-heading">
                            </div>
                            <div class="dropdown-content-body">
                                <ul>
                                    <li><a href="perfilEmpleado.jsp"><i class="ti-user"></i> <span>Perfil</span></a></li>
                                    <li><a href="calendarioEmpleado.jsp"><i class="ti-calendar"></i> <span>Calendario</span></a></li>
                                    <li><a href="modificarContraseñaEmpleado.jsp"><i class="ti-lock"></i> <span>Cambiar contraseña</span></a></li>
                                    <li><a href="Sesiones"><i class="ti-power-off"></i> <span>Cerrar sesión</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="content-wrap">
            <div class="main">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-8 p-r-0 title-margin-right">
                            <div class="page-header">
                                <div class="page-title">
                                    <h1>Mi Perfil</h1>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                        <div class="col-lg-4 p-l-0 title-margin-left">
                            <div class="page-header">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="#">Inicio</a></li>
                                        <li class="active">Perfil</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                    <div id="main-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card alert">
                                    <div class="card-body">
                                        <div class="user-profile">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="user-photo m-b-30">
                                                        <img class="img-responsive" src="ImagenControlador?opcionImg=4&idUsuario=<%=idUsuario%>" alt="" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <form method="post" action="Usuario" onsubmit="return validar()">    
                                                        <input type="hidden" name="textId" value="<%=idUsuario%>">
                                                        <div class="user-profile-name"><%=nombre%> <%=apellido%></div>
                                                        <div class="user-Location"><i class="ti-location-pin"></i> <%=ciudad%></div>
                                                        <div class="user-job-title">Cliente</div>
                                                        <div class="custom-tab user-profile-tab">
                                                            <ul class="nav nav-tabs" role="tablist">
                                                                <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">Acerca de</a></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                                <div role="tabpanel" class="tab-pane active" id="1">
                                                                    <div class="contact-information">
                                                                        <h4>Información personal</h4>
                                                                        <div class="phone-content">
                                                                            <span class="contact-title">Nombre:</span>
                                                                            <span class="phone-number"><input type="text" name="textNombre" value="<%=nombre%>" id="nombre" required></span>
                                                                        </div>
                                                                        <div class="phone-content">
                                                                            <span class="contact-title">Apellido:</span>
                                                                            <span class="phone-number"><input type="text" name="textApellido" value="<%=apellido%>" id="apellido" required></span>
                                                                        </div>
                                                                        <div class="address-content">
                                                                            <span class="contact-title">Identificación:</span>
                                                                            <span class="mail-address"><input type="number" name="textIdentificacion" value="<%=identificacion%>" id="identificacion" required></span>
                                                                        </div>
                                                                        <div class="email-content">
                                                                            <span class="contact-title">Fecha de nacimiento:</span>
                                                                            <span class="contact-email"><input type="date" name="textFecha" value="<%=nacimiento%>" id="nacimiento" required></span>
                                                                        </div>
                                                                        <div class="website-content">
                                                                            <span class="contact-title">Ciudad:</span>
                                                                            <span class="contact-address">
                                                                                <select name="textCiudad"  id="ciudad" required style="width: 39%; height: 35px">
                                                                                <option value="<%=ciudad%>"><%=ciudad%></option>
                                                                                <option value="Barranquilla" style="color: black">Barranquilla</option>
                                                                                <option value="Bogotá" style="color: black">Bogotá</option>
                                                                                <option value="Cali" style="color: black">Cali</option>
                                                                                <option value="Cartagena" style="color: black">Cartagena</option>
                                                                                <option value="Cúcuta" style="color: black">Cúcuta</option>
                                                                                <option value="Medellín" style="color: black">Medellín</option>
                                                                            </select> 
                                                                            </span>
                                                                        </div>
                                                                        <div class="skype-content">
                                                                            <span class="contact-title">Correo:</span>
                                                                            <span class="contact-skype"><input type="email" name="textCorreo" value="<%=correo%>" id="correo" required></span>
                                                                        </div>
                                                                        <div class="birthday-content">
                                                                            <span class="contact-title">Teléfono:</span>
                                                                            <span class="birth-date"><input type="number" name="textTelefono" value="<%=telefono%>" id="telefono" required></span>
                                                                        </div>
                                                                        <div class="birthday-content">
                                                                            <span class="contact-title">Usuario:</span>
                                                                            <span class="birth-date"><input type="text" name="textUsuario" value="<%=usuario%>"  id="usuario" required></span>
                                                                        </div>
                                                                        <input type="hidden" value="<%=rol%>" name="textRol">
                                                                    </div>
                                                                </div>
                                                                <a href="perfilEmpleado.jsp" class="btn btn-danger">Cancelar</a>
                                                                <button class="btn btn-success">Actualizar</button>
                                                                <input type="hidden" value="2" name="opcion">
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->
                            <div class="col-lg-6">
                                <div class="row">
                                    <a href="citasActivasEmpleado.jsp">
                                        <div class="col-lg-3">
                                            <div class="card">
                                                <div class="stat-widget-one">
                                                    <div class="profile-widget">
                                                        <div class="stat-text">Citas activas</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="citasInactivasEmpleado.jsp">
                                        <div class="col-lg-3">
                                            <div class="card">
                                                <div class="stat-widget-one">
                                                    <div class="profile-widget">
                                                        <div class="stat-text">Citas terminadas</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card alert">
                                            <div class="card-header pr">
                                                <h4>Citas pendientes</h4>
                                                <div class="card-header-right-icon">
                                                    <ul>
                                                        <li class="card-option drop-menu"><i class="ti-settings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="link"></i></li>   
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table student-data-table m-t-20">
                                                        <thead>
                                                            <tr>
                                                                <th>Nombre empleada</th>
                                                                <th>Fecha</th>
                                                                <th>Dirección</th>
                                                                <th>Hora de inicio</th>
                                                                <th>Estado</th>          
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <%
                                                                EmpleadoVO emppVO = new EmpleadoVO();
                                                                ArrayList<EmpleadoVO> listaCitas = empDAO.listarCitasEmpleada(idEmpleado);
                                                                for (int i = 0; i < listaCitas.size(); i++) {
                                                                    emppVO = listaCitas.get(i);
                                                            %>
                                                            <tr>
                                                                <td><%=emppVO.getUsuNombre()%> <%=emppVO.getUsuApellido()%></td>
                                                                <td><%=emppVO.getCitFecha()%></td>
                                                                <td><%=emppVO.getCitDireccion()%></td>
                                                                <td><%=emppVO.getCitHoraInicio()%></td>
                                                                <td style="text-align: center"><span class="badge badge-primary">Activa</span></td>
                                                            </tr>
                                                            <%}%>   
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /# card -->
                                    </div>
                                </div>
                            </div>

                            <!-- /# card -->
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer">
                        <p>ServiHogar <span id="date-time"></span></p>
                    </div>
                </div>
            </div> 
        </div>







        <div id="search">
            <button type="button" class="close">×</button>
            <form>
                <input type="search" value="" placeholder="type keyword(s) here" />
                <button type="submit" class="btn btn-primary">Buscar</button>
            </form>
        </div>
        <!-- jquery vendor -->
        <script src="js/validaciones/modificarUsuario.js" type="text/javascript"></script>
        <script src="js/lib/jquery.min.js"></script>
        <script src="js/lib/jquery.nanoscroller.min.js"></script>
        <!-- nano scroller -->
        <script src="js/lib/menubar/sidebar.js"></script>
        <script src="js/lib/preloader/pace.min.js"></script>
        <script src="js/lib/sweetalert/sweetalert.min.js"></script>
        <!-- sidebar -->
        <script src="js/lib/bootstrap.min.js"></script>
        <!-- bootstrap -->
        <script src="js/scripts.js"></script>
        <!-- scripit init-->
    </body>
</html>

