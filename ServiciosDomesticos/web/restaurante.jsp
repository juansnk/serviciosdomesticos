<%-- 
    Document   : restaurante
    Created on : 9/08/2020, 12:53:33 PM
    Author     : navid
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Restaurante</title>
    <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="CSS/popupRegistrar.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="CSS/open-iconic-bootstrap.min.css">
        <link rel="stylesheet" href="CSS/animate.css">

        <link rel="stylesheet" href="CSS/owl.carousel.min.css">
        <link rel="stylesheet" href="CSS/owl.theme.default.min.css">
        <link rel="stylesheet" href="CSS/magnific-popup.css">

        <link rel="stylesheet" href="CSS/aos.css">

        <link rel="stylesheet" href="CSS/ionicons.min.css">

        <link rel="stylesheet" href="CSS/flaticon.css">
        <link rel="stylesheet" href="CSS/icomoon.css">
        <link rel="stylesheet" href="CSS/style.css">
    </head>
    <body>
       <div class="bg-top navbar-light d-flex flex-column-reverse">
            <div class="container py-3">
                <div class="row no-gutters d-flex align-items-center align-items-stretch">
                    <div class="col-md-4 d-flex align-items-center py-4">
                        <img src="Imagenes/Logo.jpg" alt=""/>
                    </div>
                    <div class="col-lg-8 d-block">
                        <div class="row d-flex">
                            <div class="col-md d-flex topper align-items-center align-items-stretch py-md-4">
                                <div class="icon d-flex justify-content-center align-items-center"><span class="ion-ios-paper-plane"></span></div>
                                <div class="text">
                                    <span>Servicios</span>
                                    <span>Hogar</span>
                                </div>
                            </div>
                            <div class="col-md d-flex topper align-items-center align-items-stretch py-md-4">
                                <div class="icon d-flex justify-content-center align-items-center"><span class="ion-ios-call"></span></div>
                                <div class="text">
                                   <span>Servicios</span>
                                    <span>Oficina</span>                                 
                                </div>
                            </div>
                            <div class="col-md d-flex topper align-items-center align-items-stretch py-md-4">
                                <div class="icon d-flex justify-content-center align-items-center"><span class="ion-ios-time"></span></div>
                                <div class="text">
                                    <span>Servicios</span>
                                    <span>Restaurante</span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
            <div class="container d-flex align-items-center">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="oi oi-menu"></span> Menu
                </button>
                <form action="#" class="searchform order-lg-last">
                    <div class="form-group d-flex">
                        <input type="text" class="form-control pl-3" placeholder="Search">
                        <button type="submit" placeholder="" class="form-control search"><span class="ion-ios-search"></span></button>
                    </div>
                </form>
                <div class="collapse navbar-collapse" id="ftco-nav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active"><a href="index.jsp" class="nav-link">Inicio</a></li>
                        <li class="nav-item"><a href="acerca.jsp" class="nav-link">Acerca de</a></li>
                        <li class="nav-item"><a href="hogar.jsp" class="nav-link">Hogar</a></li>
                        <li class="nav-item"><a href="oficina.jsp" class="nav-link">Oficina</a></li>
                        <li class="nav-item"><a href="restaurante.jsp" class="nav-link">Restaurante</a></li>
                        <li class="nav-item"><a href="login.jsp" class="nav-link" class="btn-abrir-popup" id="btn-abrir-popup">Iniciar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- END nav -->

        <section class="home-slider owl-carousel">
            <div class="slider-item" style="background-image:url(Imagenes/portada1.jpg);">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
                        <div class="col-md-7 ftco-animate mb-md-5">
                            <span class="subheading">ServiHogar</span>
                            <h1 class="mb-4">Solícita tu servicio rápido y sencillo</h1>
                            <p><a href="login.jsp" class="btn btn-primary px-4 py-3 mt-3">Iniciar Sesion</a></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="slider-item" style="background-image:url(Imagenes/portada3.jpg);">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
                        <div class="col-md-7 ftco-animate mb-md-5">
                            <span class="subheading">ServiHogar</span>
                            <h1 class="mb-4">Ofrece tu servicio</h1>
                            <p><a href="login.jsp" class="btn btn-primary px-4 py-3 mt-3">Iniciar Sesion</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="ftco-section ftco-no-pt">
            <div class="container">
                <div class="row justify-content-center mb-5">
                    <div class="col-md-8 text-center heading-section ftco-animate">
                        <span class="subheading">Servicios</span>
                        <h2 class="mb-4">Para tu restaurante</h2>                      
                    </div>
                </div>
                <div class="row tabulation mt-4 ftco-animate">
                    <div class="col-md-4">
                        <ul class="nav nav-pills nav-fill d-md-flex d-block flex-column">
                            <li class="nav-item text-left">
                                <a class="nav-link active py-4" data-toggle="tab" href="#services-1"><span class="flaticon-lavadora mr-2"></span> Limpieza de Superficies</a>
                            </li>
                            <li class="nav-item text-left">
                                <a class="nav-link py-4" data-toggle="tab" href="#services-2"><span class="flaticon-limpiador mr-2"></span> Limpieza de Paredes</a>
                            </li>
                            <li class="nav-item text-left">
                                <a class="nav-link py-4" data-toggle="tab" href="#services-3"><span class="flaticon-aspiradora mr-2"></span> Limpieza de Pisos</a>
                            </li>
                            <li class="nav-item text-left">
                                <a class="nav-link py-4" data-toggle="tab" href="#services-4"><span class="flaticon-planchar mr-2"></span> Limpieza de Vajillas</a>
                            </li>
                            <li class="nav-item text-left">
                                <a class="nav-link py-4" data-toggle="tab" href="#services-5"><span class="flaticon-aerosol-de-limpieza mr-2"></span> Limpieza de Cristales</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-8">
                        <div class="tab-content">
                            <div class="tab-pane container p-0 active" id="services-1">
                                <div class="img" style="background-image: url(Imagenes/superficies.jpg);"></div>
                                <h3><a href="#">Limpieza de Superficies</a></h3>
                                <p>Limpiamos superficies de todo tipo y material.</p>
                            </div>
                            <div class="tab-pane container p-0 fade" id="services-2">
                                <div class="img" style="background-image: url(Imagenes/paredes.jpg);"></div>
                                <h3><a href="#">Limpieza de Paredes</a></h3>
                                <p>Los muros de tu restaurante estarán impecables como el primer día.</p>
                            </div>
                            <div class="tab-pane container p-0 fade" id="services-3">
                                <div class="img" style="background-image: url(Imagenes/pisos.jpg);"></div>
                                <h3><a href="#">Limpieza de Suelos</a></h3>
                                <p>Limpiamos tu suelo de la manera mas cuidadosa y profesional.</p>
                            </div>
                            <div class="tab-pane container p-0 fade" id="services-4">
                                <div class="img" style="background-image: url(Imagenes/platos.jpg);"></div>
                                <h3><a href="#">Limpieza de Vajillas</a></h3>
                                <p>Dejaremos tu vajilla como nueva.</p>
                            </div>
                            <div class="tab-pane container p-0 fade" id="services-5">
                                <div class="img" style="background-image: url(Imagenes/cristales.jpeg);"></div>
                                <h3><a href="#">Limpieza de Cristales</a></h3>
                                <p>Limpiamos todo tipo de cristal.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        
    <footer class="ftco-footer ftco-bg-dark ftco-section">
            <div class="container">
                <div class="row mb-5">
                    <div class="col-md-6 col-lg-3">
                        <div class="ftco-footer-widget mb-5">
                            <h2 class="ftco-heading-2">Tienes preguntas?</h2>
                            <div class="block-23 mb-3">
                                <ul>
                                    <li><span class="icon icon-map-marker"></span><span class="text">Bogotá D.C, Colombia</span></li>
                                    <li><a href="#"><span class="icon icon-phone"></span><span class="text">+57 300 615 9435</span></a></li>
                                    <li><a href="#"><span class="icon icon-phone"></span><span class="text">+57 305 388 6400</span></a></li>
                                    <li><a href="#"><span class="icon icon-phone"></span><span class="text">+57 321 903 8839</span></a></li>
                                    <li><a href="#"><span class="icon icon-envelope"></span><span class="text">lffranco27@misena.edu.co</span></a></li>
                                    <li><a href="#"><span class="icon icon-envelope"></span><span class="text">jdchaves@misena.edu.co</span></a></li>
                                    <li><a href="#"><span class="icon icon-envelope"></span><span class="text">jsgalvis@misena.edu.co</span></a></li>
                                </ul>
                            </div>
                            <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
                                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-2">
                        <div class="ftco-footer-widget mb-5 ml-md-4">
                            <h2 class="ftco-heading-2">Principales ciudades</h2>
                            <ul class="list-unstyled">
                                <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Bogotá</a></li>
                                <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Medellín</a></li>
                                <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Cali</a></li>
                                <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Barranquilla</a></li>
                                <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Cartagena</a></li>
                                <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Cúcuta</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">

                        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Esta obra de arte fue hecha por <i class="icon-heart" aria-hidden="true"></i><a href="https://colorlib.com" target="_blank"> Franchalvis </a><i class="icon-heart" aria-hidden="true"></i>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                </div>
            </div>
        </footer>



        <!-- loader -->
        <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

 
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery-migrate-3.0.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/jquery.stellar.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/aos.js"></script>
        <script src="js/jquery.animateNumber.min.js"></script>
        <script src="js/scrollax.min.js"></script>
        <script src="js/main.js"></script>

    
    </body>
</html>
