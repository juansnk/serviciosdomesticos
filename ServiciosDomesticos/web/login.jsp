<%-- 
    Document   : longin
    Created on : 14/06/2020, 10:55:13 AM
    Author     : Admin
--%>

<%@page import="modeloDAO.UsuarioDAO"%>
<%@page import="modeloVO.UsuarioVO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Login</title>
  <link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="CSS/login.css">
</head>
<body>
    
  <main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
    <div class="container">
      <div class="card login-card">
        <div class="row no-gutters">
          <div class="col-md-5">
            <img src="Imagenes/mona.jpg"alt="login" class="login-card-img">
          </div>
          <div class="col-md-7">
            <div class="card-body">
              <div class="brand-wrapper">
                  <img src="Imagenes/logoGrande.jpg" alt="logo" class="logo" >
              </div>
              <p class="login-card-description">Iniciar Sesión</p>
              <form action="Usuario" method="post">
                  <div class="form-group">
                    <label for="email" >Usuario</label>
                    <input type="text" name="textUsuario" id="email" class="form-control" placeholder="Usuario">
                  </div>
                  <div class="form-group mb-4">
                    <label for="password">Contraseña</label>
                    <input type="password" name="textContrasena" id="password" class="form-control" placeholder="***********">
                  </div>
                  <div style="color: red">
                        <% if (request.getAttribute("mensajeError") != null) { %>
                        ${mensajeError}
                        <% } else { %>
                        ${mensajeExito}
                        <% }%>
                    </div>
                    <button name="login" id="login" class="btn btn-block login-btn mb-4">Iniciar</button>
                    <input type="hidden" value="4" name="opcion">
                </form>
                <a href="contraseñaCorreo.jsp" class="forgot-password-link">¿olvidaste tu contraseña?</a>
            </div>
          </div>
        </div>
      </div>
  
    </div>
  </main>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>
</html>
