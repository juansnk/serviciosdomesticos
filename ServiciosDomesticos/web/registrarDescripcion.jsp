<%-- 
    Document   : registrarDescripcion
    Created on : 21/08/2020, 04:42:02 PM
    Author     : Admin
--%>

<%@page import="modeloVO.CitasCanceladasVO"%>
<%@page import="java.util.ArrayList"%>
<%@include file="sesiones.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Webstrot Admin : Book Information </title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="CSS/lib/calendar2/pignose.calendar.min.css" rel="stylesheet">
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link href="CSS/lib/menubar/sidebar.css" rel="stylesheet">
        <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/lib/unix.css" rel="stylesheet">
        <link href="CSS/styleee.css" rel="stylesheet">
    </head>

    <body>
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <ul>
                        <li class="label">Principal</li>
                        <li class="active"><a href="indexCliente.jsp"><i class="ti-home"></i> Inicio  </a></li>
                        <li class="label">Citas</li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i>Listas <span class="badge badge-primary">3</span><span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="citasActivas.jsp">Activas</a></li>
                                <li><a href="citasInactivas.jsp">Terminadas</a></li>
                                <li><a href="citasCanceladas.jsp">Canceladas</a></li>   
                            </ul>
                        </li>

                        <li class="label">Empleados</li>
                        <li><a href="buscarEmpleada.jsp"><i class="ti-search"></i> Busca tu empleada </a></li>

                        <li class="label">Informacion</li>
                        <li><a href="calendarioCliente.jsp"><i class="ti-calendar"></i> Calendario </a></li>
                        <li><a href="perfilCliente.jsp"><i class="ti-user"></i> Perfil</a></li>

                        <li><a href="Sesiones"><i class="ti-close"></i> Cerrar sesion</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /# sidebar -->

        <div class="header">
            <div class="pull-left">
                <div class="logo"><a href="indexCliente.jsp"><span><img src="Imagenes/logoNegro.png" height="50" /></span></a></div><div class="hamburger sidebar-toggle">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>
            </div>
            <div class="pull-right p-r-15">
                <ul>
                    <li class="header-icon dib"><img class="avatar-img" src="assets/images/avatar/1.jpg" alt="" /> <span class="user-avatar"> <%=usuario%> <i class="ti-angle-down f-s-10"></i></span>
                        <div class="drop-down dropdown-profile">
                            <div class="dropdown-content-body">
                                <ul>
                                    <li><a href="perfilCliente.jsp"><i class="ti-user"></i> <span>Perfil</span></a></li>
                                    <li><a href="calendarioCliente.jsp"><i class="ti-calendar"></i> <span>Calendario</span></a></li>
                                    <li><a href="modificarContraseñaCliente.jsp"><i class="ti-lock"></i> <span>Cambiar contraseña</span></a></li>
                                    <li><a href="Sesiones"><i class="ti-power-off"></i> <span>Cerrar sesión</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="content-wrap">
            <div class="main">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-8 p-r-0 title-margin-right">
                            <div class="page-header">
                                <div class="page-title">
                                    <h1>Citas</h1>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                        <div class="col-lg-4 p-l-0 title-margin-left">
                            <div class="page-header">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="indexCliente.jsp">Inicio</a></li>
                                        <li class="active"><a href="citasActivas.jsp">Citas activas</a></li>
                                        <li class="active">Cancelar cita</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                    <div id="main-content">
                        <div class="card alert">
                            <div class="card-body">
                                <div class="card-header m-b-20">
                                    <h4>¿Por que deseas cancelar tu cita?</h4>
                                </div>
                                <form method="POST" action="Cita" class="formularioTablas">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="basic-form">
                                                <div class="form-group">
                                                    <% CitasCanceladasVO cicVO = (CitasCanceladasVO) request.getAttribute("idCitaC");
                                                if (cicVO != null) {%>
                                                    <input type="hidden" name="textId"  class="form-control border-none input-flat bg-ash" value="<%=cicVO.getIdCita()%>">
                                                    <%}%>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="basic-form">
                                                <div class="form-group">
                                                    <textarea id="comentarios" cols="10" rows="10" class="form-control border-none input-flat bg-ash" name="textDescripcion"></textarea>
                                                    <div id="contador"></div>
                                                </div>
                                            </div>

                                                </div>
                                    </div>
                                                <div class="col text-center">
                                    <button class="btn btn-default btn-lg m-b-10 m-l-5 sbmt-btn" style="text-align: center">Confirmar</button>
                                                </div>                                    
                                                <input type="hidden" value="4" name="opcion">
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="footer">
                                    <p>ServiHogar <span id="date-time"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>





        <div id="search">
            <button type="button" class="close">×</button>
            <form>
                <input type="search" value="" placeholder="type keyword(s) here" />
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
        <!-- jquery vendor -->
        <script src="js/lib/jquery.min.js"></script>
        <script src="js/lib/jquery.nanoscroller.min.js"></script>
        <!-- nano scroller -->
        <script src="js/lib/menubar/sidebar.js"></script>
        <script src="js/lib/preloader/pace.min.js"></script>
        <!-- sidebar -->
        <script src="js/lib/bootstrap.min.js"></script>
        <!-- bootstrap -->


        <script src="js/lib/calendar-2/moment.latest.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/semantic.ui.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/prism.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/pignose.calendar.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/pignose.init.js"></script>
        <!-- scripit init-->

        <script src="js/scripts.js"></script>
        <!-- scripit init-->





    </body>

</html>