<%-- 
    Document   : solicitarServicio
    Created on : 10/07/2020, 12:25:20 PM
    Author     : Admin
--%>


<%@page import="modeloDAO.ServicioDAO"%>
<%@page import="modeloVO.ServicioVO"%>
<%@page import="modeloVO.CitaVO"%>
<%@page import="modeloDAO.CategoriaDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modeloVO.CategoriaVO"%>
<%@include file="sesiones.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Webstrot Admin : Invoice</title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/lib/sweetalert/sweetalert.css" rel="stylesheet">
        <link href="CSS/lib/unix.css" rel="stylesheet">
        <link rel="stylesheet" href="CSS/flaticon.css">
        <link href="CSS/styleee.css" rel="stylesheet">
    </head>

    <body class="bg-primary">

        <div class="unix-invoice">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="invoice" class="effect2 m-t-120">
                            <div id="invoice-top">
                                <div class="invoice-logo"></div>
                                <div class="invoice-info">
                                    <h2><%=nombre%> <%=apellido%></h2>
                                    <p> <%=correo%> <br> <%=telefono%>
                                    </p>
                                </div>
                                <!--End Info-->
                                <div class="title">
                                    <h4>Total a pagar</h4>
                                    <p>Recuerda que el pago debe ser presencial</p>
                                    <p>cuando el empleado halla culminado sus servicios</p>
                                </div>
                                <!--End Title-->
                            </div>
                            <!--End InvoiceTop-->



                            <div id="invoice-mid">                              
                                <p class="introPa" style="text-align: center">Elige el lugar y los servicios que necesitas para tu cita</p>                                 

                            </div>
                            <!--End Invoice Mid-->                    
                            <div class="container-fluid h-100"> 
                                <div class="row w-100 align-items-center">
                                    <div class="col text-center">
                                        <form method="post" action="Servicio">
                                            <%
                                                CategoriaVO catVO = new CategoriaVO();
                                                CategoriaDAO catDAO = new CategoriaDAO();
                                                ArrayList<CategoriaVO> lista = catDAO.listarCategorias();
                                                for (int i = 0; i < lista.size(); i++) {
                                                    catVO = lista.get(i);
                                            %>
                                            <button value="<%=catVO.getIdCategoria()%>" name="textIdCategoria" class="btn btn-dark btn-lg m-b-10 m-l-5 sbmt-btn"><span class="flaticon-aerosol-de-limpieza mr-2" style="color: #0095e1; font-size: 25px"></span> <%=catVO.getCatNombre()%></button>
                                            <input type="hidden" value="1" name="opcion">

                                            <%}%>
                                            <% CitaVO citVO = (CitaVO) request.getAttribute("idCita");
                                                if (citVO != null) {%>
                                            <input type="hidden" name="textIdCita" value="<%=citVO.getIdCita()%>">
                                            <%}%>

                                        </form>
                                    </div>	
                                </div>


                            </div>

                            <form method="post" action="CitaServicio" onsubmit="return servicio()">
                                <%
                                    String idCategoria = request.getParameter("textIdCategoria");
                                    ServicioVO serVO = new ServicioVO();
                                    ServicioDAO serDAO = new ServicioDAO();
                                    ArrayList<ServicioVO> listar = serDAO.listarServicios(idCategoria);
                                    for (int i = 0; i < listar.size(); i++) {
                                        serVO = listar.get(i);
                                %>      
                                <% CitaVO cittVO = (CitaVO) request.getAttribute("idCitaa");
                                    if (cittVO != null) {%>
                                <input type="hidden" name="textIdCita" value="<%=cittVO.getIdCita()%>">
                                <%}%>


                                <div class="row checkbox-row">
                                    <div class="col-xs-4 col-xs-offset-5">
                                        <div class="checkbox-inline" style="color: black">
                                            <input type="checkbox"  value="<%=serVO.getIdServicio()%>" name="textIdServicio"  class="mail-checkbox" id="servicios">
                                            <label class="checkbox-inline"><%=serVO.getSerDescripcion()%></label>     
                                            <label><strong>$<%=serVO.getSerCosto()%></strong></label>  

                                        </div>
                                    </div>
                                </div>

                                <%}%>
                                <div class="container-fluid h-100"> 
                                    <div class="row w-100 align-items-center">
                                        <div class="col text-center">
                                            <button class="btn btn-success btn-lg m-b-10 m-l-5 sbmt-btn">Registrar</button>
                                            <input type="hidden" name="opcion" value="1">
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <!--End InvoiceBot-->


                        </div>

                        <!--End Invoice-->
                    </div>
                </div>
            </div>
        </div>
        <script src="js/validaciones/servicios.js" type="text/javascript"></script>
        <script src="js/lib/sweetalert/sweetalert.min.js"></script>
    </body>

</html>