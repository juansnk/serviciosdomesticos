<%@page import="modeloDAO.EmpleadoDAO"%>
<%@page import="modeloVO.EmpleadoVO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modeloDAO.ProAgendaDAO"%>
<%@page import="modeloVO.ProAgendaVO"%>
<%@include file="sesiones.jsp" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Webstrot Admin : Class Information </title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="CSS/lib/calendar2/pignose.calendar.min.css" rel="stylesheet">
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link href="CSS/lib/menubar/sidebar.css" rel="stylesheet">
        <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/lib/unix.css" rel="stylesheet">
        <link href="CSS/styleee.css" rel="stylesheet">
    </head>

    <body>
        <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <ul>
                        <li class="label">Principal</li>
                        <li class="active"><a href="indexEmpleado.jsp"><i class="ti-home"></i> Inicio</a></li>
                        <li class="label">Agenda</li>
                        <li><a href="listaAgenda.jsp"><i class="ti-pencil-alt"></i>Mi agenda</a>
                        </li>
                        <li class="label">Citas</li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i>Listas <span class="badge badge-primary">3</span><span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="citasActivasEmpleado.jsp">Activas</a></li>
                                <li><a href="citasInactivasEmpleado.jsp">Terminadas</a></li>
                            </ul>
                        </li>
                         <li class="label">Informacion</li>
                        <li><a href="calendarioEmpleado.jsp"><i class="ti-calendar"></i> Calendario </a></li>                    
                        <li><a href="perfilEmpleado.jsp"><i class="ti-user"></i> Perfil</a></li>
                        <li><a href="Sesiones"><i class="ti-close"></i> Cerrar Sesión</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /# sidebar -->
        <div class="header">
            <div class="pull-left">
                <div class="logo"><a href="index.jsp"><span><img src="Imagenes/logoNegro.png" height="50" /></span></a></div>

                <div class="hamburger sidebar-toggle">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>
            </div>

            <div class="pull-right p-r-15">
                <ul>
                    <li class="header-icon dib"><i class="ti-bell"><span class="badge badge-danger">.</span></i>
                        <div class="drop-down">
                            <div class="dropdown-content-heading">
                                <span class="text-left">Notificaciones recientes</span>
                            </div>
                            <div class="dropdown-content-body">
                                <ul>
                                    <%                                        EmpleadoVO empVO = new EmpleadoVO();
                                        EmpleadoDAO empDAO = new EmpleadoDAO();
                                        ArrayList<EmpleadoVO> listaCitasNo = empDAO.listarEmpleadaNotificacion(idEmpleado);
                                        for (int i = 0; i < listaCitasNo.size(); i++) {
                                            empVO = listaCitasNo.get(i);
                                    %>
                                    <li>
                                        <a href="#">
                                            <h4>Tienes una cita nueva</h4>
                                            <div class="notification-content">
                                                <small class="notification-timestamp pull-right"></small>
                                                <div class="notification-heading"><%=empVO.getUsuNombre()%> <%=empVO.getUsuApellido()%></div>
                                                <div class="notification-text"><%=empVO.getCitFecha()%>---<%=empVO.getCitHoraInicio()%> </div>

                                                <a href="Cita?opcion=9&textId=<%=empVO.getIdCita()%>&tipoCorreo=4&correo=<%=empVO.getUsuCorreo()%>" class="btn btn-danger">Cancelar</a> <a href="Cita?textEstado=1&opcion=10&textId=<%=empVO.getIdCita()%>" class="btn btn-success">Aceptar</a> <a href="Cita?opcion=5&textId=<%=empVO.getIdCita()%>" class="btn btn-default">Ver</a>
                                            </div>
                                        </a>
                                    </li>
                                    <%}%>
                                    <li class="text-center">
                                        <a href="#" class="more-link">Cerrar</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li class="header-icon dib"><img class="avatar-img" src="Imagenes/usuario.png" alt="" /> <span class="user-avatar"> <%=usuario%> <i class="ti-angle-down f-s-10"></i></span>
                        <div class="drop-down dropdown-profile">
                            <div class="dropdown-content-heading">
                            </div>
                            <div class="dropdown-content-body">
                                <ul>
                                    <li><a href="perfilEmpleado.jsp"><i class="ti-user"></i> <span>Perfil</span></a></li>
                                    <li><a href="calendarioEmpleado.jsp"><i class="ti-calendar"></i> <span>Calendario</span></a></li>
                                    <li><a href="modificarContraseñaEmpleado.jsp"><i class="ti-lock"></i> <span>Cambiar contraseña</span></a></li>
                                    <li><a href="Sesiones"><i class="ti-power-off"></i> <span>Cerrar sesión</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>



        <div class="content-wrap">
            <div class="main">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-8 p-r-0 title-margin-right">
                            <div class="page-header">
                                <div class="page-title">
                                    <h1>Agenda</h1>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                        <div class="col-lg-4 p-l-0 title-margin-left">
                            <div class="page-header">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="#">Inicio</a></li>
                                        <li class="active">Agenda</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                    <div id="main-content">
                        <div class="card alert">
                            <div class="card-body">
                                <div class="card-header m-b-20">
                                    <h4>Modifica tus horas</h4>
                                </div>
                                <form method="post" action="ProAgenda">
                                    <% ProAgendaVO proVO = (ProAgendaVO) request.getAttribute("ProAgendaa");
                                         if (proVO != null) {%>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="basic-form">
                                                <div class="form-group">
                                                    <input type="hidden" name="textId" value="<%=proVO.getIdProgramacionAgenda()%>">
                                                    <label>Dia</label>
                                                    <input type="text" name="textDia" class="form-control border-none input-flat bg-ash" value="<%=proVO.getProDia()%>" readonly="readonly">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="basic-form">
                                                <div class="form-group">
                                                    <label>Hora inicio</label>
                                                    <input type="time" name="textHoraInicio" value="<%=proVO.getProHoraInicio()%>" class="form-control border-none input-flat bg-ash" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="basic-form">
                                                <div class="form-group">
                                                    <label>Hora fin</label>
                                                    <input type="time" name="textHoraFin" value="<%=proVO.getProHoraFin()%>" class="form-control border-none input-flat bg-ash" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                 <%}%>
                                    <a href="listaAgenda.jsp" class="btn btn-default btn-lg m-b-10 bg-warning border-none m-r-5 sbmt-btn" type="button">Cancelar</a>
                                    <button class="btn btn-default btn-lg m-b-10 m-l-5 sbmt-btn">Modificar</button>
                                    <input type="hidden" value="2" name="opcion">
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="footer">
                                    <p>ServiHogar <span id="date-time"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jquery vendor -->
        <script src="js/lib/jquery.min.js"></script>
        <script src="js/lib/jquery.nanoscroller.min.js"></script>
        <!-- nano scroller -->
        <script src="js/lib/menubar/sidebar.js"></script>
        <script src="js/lib/preloader/pace.min.js"></script>
        <!-- sidebar -->
        <script src="js/lib/bootstrap.min.js"></script>
        <!-- bootstrap -->
        <script src="js/scripts.js"></script>






    </body>

</html>