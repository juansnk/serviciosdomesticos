<%-- 
    Document   : registrarDescripcionE
    Created on : 13/10/2020, 05:42:30 PM
    Author     : Admin
--%>

<%@page import="modeloDAO.EmpleadoDAO"%>
<%@page import="modeloVO.EmpleadoVO"%>
<%@page import="modeloVO.CitasCanceladasVO"%>
<%@page import="java.util.ArrayList"%>
<%@include file="sesiones.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Webstrot Admin : Book Information </title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="CSS/lib/calendar2/pignose.calendar.min.css" rel="stylesheet">
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link href="CSS/lib/menubar/sidebar.css" rel="stylesheet">
        <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/lib/unix.css" rel="stylesheet">
        <link href="CSS/styleee.css" rel="stylesheet">
    </head>

    <body>
 <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <ul>
                        <li class="label">Principal</li>
                        <li class="active"><a href="indexEmpleado.jsp"><i class="ti-home"></i> Inicio</a></li>
                        <li class="label">Agenda</li>
                        <li><a href="listaAgenda.jsp"><i class="ti-pencil-alt"></i>Mi agenda</a>
                        </li>
                        <li><a href="app-event-calender.html"><i class="ti-calendar"></i> Calendario </a></li>
                        <li class="label">Apps</li>
                        <li><a href="perfilEmpleado.jsp"><i class="ti-user"></i> Perfil</a></li>
                        <li class="label">Features</li>
                        
                        <li><a href="Sesiones"><i class="ti-close"></i> Cerrar Sesión</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /# sidebar -->
        <div class="header">
            <div class="pull-left">
               <div class="logo"><a href="index.jsp"><span><img src="Imagenes/logoNegro.png" height="50" /></span></a></div>
                
                <div class="hamburger sidebar-toggle">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>
            </div>

            <div class="pull-right p-r-15">
                <ul>
                    <li class="header-icon dib"><i class="ti-bell"></i>
                        <div class="drop-down">
                            <div class="dropdown-content-heading">
                                <span class="text-left">Notificaciones recientes</span>
                            </div>
                            <div class="dropdown-content-body">
                                <ul>
                                    <%                                        EmpleadoVO empVO = new EmpleadoVO();
                                        EmpleadoDAO empDAO = new EmpleadoDAO();
                                        ArrayList<EmpleadoVO> listaCitas = empDAO.listarCitasEmpleada(idEmpleado);
                                        for (int i = 0; i < listaCitas.size(); i++) {
                                            empVO = listaCitas.get(i);
                                    %>
                                    <li>
                                        <a href="#">
                                            <h4>Tienes una cita nueva</h4>
                                            <div class="notification-content">
                                                <small class="notification-timestamp pull-right"></small>
                                                <div class="notification-heading"><%=empVO.getUsuNombre()%> <%=empVO.getUsuApellido()%></div>
                                                <div class="notification-text"><%=empVO.getCitFecha()%>---<%=empVO.getCitHoraInicio()%> </div>
                                                <a href="Cita?opcion=9&textId=<%=empVO.getIdCita()%>&tipoCorreo=4&correo=<%=empVO.getUsuCorreo()%>" class="btn btn-danger">Cancelar</a> <a href="Cita?textEstado=1&opcion=10&textId=<%=empVO.getIdCita()%>" class="btn btn-success">Aceptar</a> <a href="Cita?opcion=5&textId=<%=empVO.getIdCita()%>" class="btn btn-default">Ver</a>
                                            </div>
                                        </a>
                                    </li>
                                    <%}%>
                                    <li class="text-center">
                                        <a href="#" class="more-link">Cerrar</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li class="header-icon dib"><img class="avatar-img" src="assets/images/avatar/1.jpg" alt="" /> <span class="user-avatar"> <%=usuario%> <i class="ti-angle-down f-s-10"></i></span>
                        <div class="drop-down dropdown-profile">
                            <div class="dropdown-content-body">
                                <ul>
                                    <li><a href="perfilEmpleado.jsp"><i class="ti-user"></i> <span>Perfil</span></a></li>
                                    <li><a href="calendarioEmpleado.jsp"><i class="ti-calendar"></i> <span>Calendario</span></a></li>
                                    <li><a href="modificarContraseñaEmpleado.jsp"><i class="ti-lock"></i> <span>Cambiar contraseña</span></a></li>
                                    <li><a href="Sesiones"><i class="ti-power-off"></i> <span>Cerrar sesión</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="content-wrap">
            <div class="main">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-8 p-r-0 title-margin-right">
                            <div class="page-header">
                                <div class="page-title">
                                    <h1>Notificacion</h1>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                        <div class="col-lg-4 p-l-0 title-margin-left">
                            <div class="page-header">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="indexCliente.jsp">Inicio</a></li>
                                        <li class="active">Cancelar cita</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                    <div id="main-content">
                        <div class="card alert">
                            <div class="card-body">
                                <div class="card-header m-b-20">
                                    <h4>¿Por que deseas cancelar cita?</h4>
                                </div>
                                <form method="POST" action="CitasCanceladas" class="formularioTablas">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="basic-form">
                                                <div class="form-group">
                                                    <% CitasCanceladasVO cicVO = (CitasCanceladasVO) request.getAttribute("idCitaCancelar");
                                                if (cicVO != null) {%>
                                                    <input type="hidden" name="textId"  class="form-control border-none input-flat bg-ash" value="<%=cicVO.getIdCita()%>">
                                                    <%}%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="basic-form">
                                                <div class="form-group">
                                                    <textarea id="comentarios" cols="10" rows="10" class="form-control border-none input-flat bg-ash" name="textDescripcion"></textarea>
                                                    <div id="contador"></div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="textRol" value="<%=rol%>">

                                                </div>
                                    </div>
                                                <div class="col text-center">
                                    <button class="btn btn-default btn-lg m-b-10 m-l-5 sbmt-btn" style="text-align: center">Confirmar</button>
                                                </div>                                    
                                                <input type="hidden" value="1" name="opcion">
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="footer">
                                    <p>ServiHogar <span id="date-time"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>





        <div id="search">
            <button type="button" class="close">×</button>
            <form>
                <input type="search" value="" placeholder="type keyword(s) here" />
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
        <!-- jquery vendor -->
        <script src="js/lib/jquery.min.js"></script>
        <script src="js/lib/jquery.nanoscroller.min.js"></script>
        <!-- nano scroller -->
        <script src="js/lib/menubar/sidebar.js"></script>
        <script src="js/lib/preloader/pace.min.js"></script>
        <!-- sidebar -->
        <script src="js/lib/bootstrap.min.js"></script>
        <!-- bootstrap -->


        <script src="js/lib/calendar-2/moment.latest.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/semantic.ui.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/prism.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/pignose.calendar.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/pignose.init.js"></script>
        <!-- scripit init-->
        <script src="js/scripts.js" type="text/javascript"></script> 
       <script src="js/scripts.js"></script>
        <!-- scripit init-->





    </body>

</html>
