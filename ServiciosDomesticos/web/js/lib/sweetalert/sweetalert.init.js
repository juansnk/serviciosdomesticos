
function cancelarCita(ev){
    ev.preventDefault();
var urlToRedirect = ev.currentTarget.getAttribute('href');; 
console.log(urlToRedirect);

    swal({
            title: "¿Estás seguro de cancelar tu cita?",
            text: "No podras revertir esta accion",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, cancelar",
            cancelButtonText: "No cancelar",
            closeOnConfirm: false
        },function(confirm){
      if(confirm){
        console.log('confirmado');
        window.location.href = urlToRedirect;
      }
    });
}

function citasCanceladas(ev){
    ev.preventDefault();
var urlToRedirect = ev.currentTarget.getAttribute('href');; 
console.log(urlToRedirect);

    swal({
            title: "¿Estás seguro de borrar este registro?",
            text: "No podras recuperar este registro",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, borrar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
        },function(confirm){
      if(confirm){
        console.log('confirmado');
        window.location.href = urlToRedirect;
      }
    }, function(){
            swal("Eliminado !!", "El registro se borro para siempre", "success");
        });
};