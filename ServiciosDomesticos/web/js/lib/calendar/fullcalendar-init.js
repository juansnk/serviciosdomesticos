
!function ($) {
    "use strict";

    var CalendarApp = function () {
        this.$body = $("body"),
                this.$modal = $('#event-modal'),
                this.$event = ('#external-events div.external-event'),
                this.$calendar = $('#calendar'),
                this.$saveCategoryBtn = $('.save-category'),
                this.$categoryForm = $('#add-category form'),
                this.$extEvents = $('#external-events'),
                this.$calendarObj = null
    };


    /* on drop */
    CalendarApp.prototype.onDrop = function (eventObj, date) {
        var $this = this;
        // retrieve the dropped element's stored Event Object
        var originalEventObject = eventObj.data('eventObject');
        var $categoryClass = eventObj.attr('data-class');
        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject);
        // assign it the date that was reported
        copiedEventObject.start = date;
        start = moment(start).format('YYYY/MM/DD hh:mm');

        if ($categoryClass)
            copiedEventObject['className'] = [$categoryClass];
        // render the event on the calendar
        $this.$calendar.fullCalendar('renderEvent', copiedEventObject, true);
        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
            // if so, remove the element from the "Draggable Events" list
            eventObj.remove();
        }
    },
            /* on click on event */
            CalendarApp.prototype.onEventClick = function (calEvent, jsEvent, view) {
                var $this = this;
                var form = $("<form action='Cita' method='post'></form>");
                form.append("<label>Change event name</label>");
                form.append("<div class='input-group'><input class='form-control' type=text value='" + calEvent.title + "' /><span class='input-group-btn'><button type='submit' class='btn btn-success waves-effect waves-light'><i class='fa fa-check'></i> Save</button></span></div>");
                $this.$modal.modal({
                    backdrop: 'static'
                });
                $this.$modal.find('.delete-event').show().end().find('.save-event').hide().end().find('.modal-body').empty().prepend(form).end().find('.delete-event').unbind('click').on("click", function () {
                    $this.$calendarObj.fullCalendar('removeEvents', function (ev) {
                        return (ev._id == calEvent._id);
                    });
                    $this.$modal.modal('hide');
                });
                $this.$modal.find('form').on('submit', function () {
                    calEvent.title = form.find("input[type=text]").val();
                    $this.$calendarObj.fullCalendar('updateEvent', calEvent);
                    $this.$modal.modal('hide');
                    return false;
                });
            },
            /* on select */
            CalendarApp.prototype.onSelect = function (start, end, allDay) {
                $("#fecha").val($.fullCalendar.formatDate(start, 'YYYY-MM-DD'));
                $("#horaInicio").val($.fullCalendar.formatDate(start, 'HH:MM:SS'));
                $("#horaFin").val($.fullCalendar.formatDate(end, 'HH:MM:SS'));
                var f = new Date();
                var hoy =(f.getFullYear() + "-" + (f.getMonth() + 1) + "-" +('0' + f.getDate()).slice(-2) );               
                var fechaCita = document.getElementById("fecha").value;
                var diaCita = document.getElementsByName("fecha");
                var diaci = "";
                var diaCitas=[];

                for(var y = 0; y < diaCita.length; y++) {
                 diaCitas = [diaci + diaCita[y].value];
                 diaCitas.push();
                }
                
                if (fechaCita <= hoy ) {
                    swal("Espera!", "La fecha ya paso", "warning");
                    console.log(fechaCita);
                console.log(hoy);
                    return true;
                } else if(fechaCita===diaCitas){
                    swal("Espera!", "Tu empleada ya tiene una cita este dia", "warning");
                    return true;
                }else{
                    var $this = this;
                    $this.$modal.modal({
                        backdrop: 'static'
                    });

                    var form = $("<form method='post' action='Cita'></form>");
                    form.append("<div class='row'></div>");
                    form.find(".row")
                            .append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Direccion</label><input class='form-control' type='date' name='fecha' id='fecha'/></div></div>")
                            .append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Direccion</label><input class='form-control' placeholder='Insert Event Name' type='text' name='title'/></div></div>")
                            .append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Hora de inicio</label><input class='form-control' type='time' name='textHoraInicio'/></div></div>")
                            .append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Hora de fin</label><input class='form-control'type='time' name='textHoraFin'/></div></div>")
                            .append("<input class='form-control'type='hidden' name='textEstado' value='1'/>")
                            .append("</div></div>");
                    $this.$modal.find('.delete-event').hide().end().find('.save-event').show().end().find('.modal-body').empty().prepend(form).end().find('.save-event').unbind('click').on("click", function () {
                        form.submit();
                    });

                    $this.$modal.find('form').on('submit', function () {
                        var title = form.find("input[name='title']").val();
                        var beginning = form.find("input[name='beginning']").val();
                        var ending = form.find("input[name='ending']").val();
                        var categoryClass = form.find("select[name='category'] option:checked").val();
                        if (title !== null && title.length != 0) {
                            $this.$calendarObj.fullCalendar('renderEvent', {
                                title: title,
                                start: start,
                                end: end,
                                allDay: false,
                                className: categoryClass
                            }, true);
                            $this.$modal.modal('hide');
                        }
                        else {
                            alert('You have to give a title to your event');
                        }
                        return false;

                    });
                    $this.$calendarObj.fullCalendar('unselect');

                }
                
            },
            CalendarApp.prototype.enableDrag = function () {
                //init events
                $(this.$event).each(function () {
                    // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                    // it doesn't need to have a start or end
                    var eventObject = {
                        title: $.trim($(this).text()) // use the element's text as the event title
                    };
                    // store the Event Object in the DOM element so we can get to it later
                    $(this).data('eventObject', eventObject);
                    // make the event draggable using jQuery UI
                    $(this).draggable({
                        zIndex: 999,
                        revert: true, // will cause the event to go back to its
                        revertDuration: 0  //  original position after the drag
                    });
                });
            };
    /* Initializing */

    CalendarApp.prototype.init = function () {

        this.enableDrag();

        /*  Initialize the calendar  */
        var ide = document.getElementsByName("id");
        var fecha = document.getElementsByName("fecha");
        var nombre = document.getElementsByName("nombre");
        var horaInicio = document.getElementsByName("horaInicio");
        var horaFin = document.getElementsByName("horaFin");
        var id = "";
        var dia = "";
        var titulo = "";
        var inicio = "";
        var fin = "";
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var form = '';
        var today = new Date($.now());
        var citas = [];

        for (var x = 0; x < nombre.length; x++) {
            var defaultEvents = {
                id: id + ide[x].value,
                title: titulo + nombre[x].value,
                start: dia + fecha[x].value + 'T' + inicio + horaInicio[x].value,
                end: dia + fecha[x].value + 'T' + fin + horaFin[x].value,
                eventColor: 'bg-primary'
            };
            citas.push(defaultEvents);
        }
        var $this = this;
        $this.$calendarObj = $this.$calendar.fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            events: citas,
            editable: true,
            selectable: true,
            select: function (start, end, allDay) {
                $this.onSelect(start, end, allDay);
            }

        });


    },
            //init CalendarApp
            $.CalendarApp = new CalendarApp, $.CalendarApp.Constructor = CalendarApp;


}(window.jQuery),
//initializing CalendarApp
        function ($) {
            "use strict";
            $.CalendarApp.init()
        }(window.jQuery);
var dia = document.getElementById("fecha").value;
var titulo = document.getElementById("nombre").value;

