
!function ($) {
    "use strict";

    var CalendarApp = function () {
        this.$body = $("body")
        this.$modal = $('#event-modal'),
                this.$event = ('#external-events div.external-event'),
                this.$calendar = $('#calendar'),
                this.$saveCategoryBtn = $('.save-category'),
                this.$categoryForm = $('#add-category form'),
                this.$extEvents = $('#external-events'),
                this.$calendarObj = null;
    };


    /* on drop */
    CalendarApp.prototype.onDrop = function (eventObj, date) {
        var $this = this;
        // retrieve the dropped element's stored Event Object
        var originalEventObject = eventObj.data('eventObject');
        var $categoryClass = eventObj.attr('data-class');
        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject);
        // assign it the date that was reported
        copiedEventObject.start = date;
        start = moment(start).format('YYYY/MM/DD hh:mm');

        if ($categoryClass)
            copiedEventObject['className'] = [$categoryClass];
        // render the event on the calendar
        $this.$calendar.fullCalendar('renderEvent', copiedEventObject, true);
        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
            // if so, remove the element from the "Draggable Events" list
            eventObj.remove();
        }
    },
            /* on click on event */
            CalendarApp.prototype.onEventClick = function (calEvent, jsEvent, view) {
                window.location.href="Cita?opcion=6&textId="+ calEvent.id +"&textEstado=1";
            },
                    
            CalendarApp.prototype.enableDrag = function () {
                //init events
                $(this.$event).each(function () {
                    // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                    // it doesn't need to have a start or end
                    var eventObject = {
                        title: $.trim($(this).text()) // use the element's text as the event title
                    };
                    // store the Event Object in the DOM element so we can get to it later
                    $(this).data('eventObject', eventObject);
                    // make the event draggable using jQuery UI
                    $(this).draggable({
                        zIndex: 999,
                        revert: true, // will cause the event to go back to its
                        revertDuration: 0  //  original position after the drag
                    });
                });
            };
    /* Initializing */
    CalendarApp.prototype.init = function () {
        this.enableDrag();
        /*  Initialize the calendar  */
        var ide = document.getElementsByName("id");
        var fecha = document.getElementsByName("fecha");
        var nombre = document.getElementsByName("nombre");
        var horaInicio = document.getElementsByName("horaInicio");
        var horaFin = document.getElementsByName("horaFin");
        var id="";
        var dia = "";
        var titulo = "";
        var inicio = "";
        var fin = "";

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var form = '';
        var today = new Date($.now());
        var citas=[];
       
       for (var x = 0; x < nombre.length; x++) {
            var defaultEvents ={
                    id: id + ide[x].value,
                    title: titulo + nombre[x].value,
                    start: dia + fecha[x].value + 'T' + inicio + horaInicio[x].value,
                    end: dia + fecha[x].value + 'T' + fin + horaFin[x].value,
                    className: 'bg-primary'
                };
                citas.push(defaultEvents);
            } 
            var $this = this; 
      
            $this.$calendarObj = $this.$calendar.fullCalendar({
                header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
                locale: 'es', 
                events: citas,
                editable: true,
                eventClick: function(calEvent, jsEvent, view) { $this.onEventClick(calEvent, jsEvent, view); }
            });
          
       
       
    },
            //init CalendarApp
            $.CalendarApp = new CalendarApp, $.CalendarApp.Constructor = CalendarApp;

}(window.jQuery),
//initializing CalendarApp
        function ($) {
            "use strict";
            $.CalendarApp.init()
        }(window.jQuery);
