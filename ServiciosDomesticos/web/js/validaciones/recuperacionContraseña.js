/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function validar() {

   var correo = document.getElementById('correo').value;
   var  expresion = /\w+@\w+\.+[a-z]/;

    if (correo === "") {
        swal("Espera!","Tienes que llenar el campo","warning");
        return false;
    }
    else if (correo.length > 50) {
        swal("Espera!","El correo es muy largo","warning");
        return false;
    }
    else if (!expresion.test(correo)) {
        swal("Espera!","El correo no es valido","warning");
        return false;
    }

}