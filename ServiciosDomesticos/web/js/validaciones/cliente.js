/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function validar() {
    var direccion, localidad, barrio;
    direccion = document.getElementById('direccion').value;
    localidad = document.getElementById('localidad').value;
    barrio = document.getElementById('barrio').value;


    if (direccion === "" || localidad === "" || barrio === "") {
        swal("Espera!","Todos los campos son obligatorios","warning");
        return false;
    }
    else if (direccion.length > 50) {
        swal("Espera!","La direccion no puede sobrepasar los 50 caracteres","warning");
        return false;
    }
    else if (localidad.length > 30) {
        swal("Espera!","La localidad no puede superar made los 30 caracteres","warning");
        return false;
    }
    else if (!/^([a-zA-ZñÑáéíóúÁÉÍÓÚ\s\ ])+$/i.test(localidad)) {
        swal("Espera!","La localidad solo puede contener letras","warning");
        return false;
    }
    else if (barrio.length > 30) {
        swal("Espera!","El barrio no puede superar los 30 caracteres","warning");
        return false;
    }
    else if (!/^([a-zA-ZñÑáéíóúÁÉÍÓÚ\s\ ])+$/i.test(barrio)) {
        swal("Espera!","el barrio solo puede contener letras","warning");
        return false;
    }

}