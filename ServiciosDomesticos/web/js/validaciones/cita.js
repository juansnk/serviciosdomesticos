/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function validarCita(){
    var direccion = document.getElementById("direccion").value;
    
    if (direccion === "") {
        swal("Espera!","Todos los campos son obligatorios","warning");
        return false;
    }else if (direccion.length > 50) {
        swal("Espera!","La direccion no puede sobrepasar los 50 caracteres","warning");
        return false;
    }
}
