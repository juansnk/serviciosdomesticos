/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function validar() {
    var experiencia, descripcion;
    experiencia = document.getElementById('experiencia').value;
    descripcion = document.getElementById('descripcion').value;


    if (experiencia === "" || descripcion === "") {
        swal("Espera!", "Todos los campos son obligatorios","warning");
        return false;
    }
    else if (experiencia.length > 10) {
        swal("Espera!", "Tu expereciencia no puede superar 10 caracteres","warning");
        return false;
    }

    else if (descripcion.length > 300) {
        swal("Espera!", "Tu descripcion no puede superar los 300 caracteres","warning");
        return false;
    }
    else if (!/^([a-zA-ZñÑáéíóúÁÉÍÓÚ\s\ ])+$/i.test(descripcion)) {
        swal("Espera!", "La descripcion solo puede contener letras","warning");
        return false;
    }

}