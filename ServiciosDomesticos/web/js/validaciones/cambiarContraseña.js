/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

window.onload = function() {
  var myInput = document.getElementById('contrasena1');
  var myInput1 = document.getElementById('contrasena2');
  myInput.onpaste = function(e) {
    e.preventDefault();
    swal("Esta acción está prohibida");
  };
  
  myInput.oncopy = function(e) {
    e.preventDefault();
    swal("Esta acción está prohibida");
  };
  myInput1.onpaste = function(e) {
    e.preventDefault();
    swal("Esta acción está prohibida");
  };
  
  myInput1.oncopy = function(e) {
    e.preventDefault();
    swal("Esta acción está prohibida");
  };
};
function validar(){
    var contrasena = document.getElementById("contrasena").value;
    var contrasena1 = document.getElementById("contrasena1").value;
    var contrasena2 = document.getElementById("contrasena2").value;
    var valContra =  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    
     if (contrasena === "" || contrasena1 === "" || contrasena2 === "" ) {
        swal("Espera!","Todos los campos son obligatorios","warning");
        return false;
    }else if(!contrasena1.match(valContra)){
        swal("Espera!","La contraseña debe tener mas de 6 caracteres y debe contener al menos un número y una mayúscula","warning");
        return false;
    }else if(contrasena1 !== contrasena2){
        swal("Espera!","Las contraseñas no coinciden","warning");
        return false;
    }
}
