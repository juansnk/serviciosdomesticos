/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function validar() {
    var nombre, apellido, identificacion, nacimiento, ciudad, correo, telefono, usuario,  valContra;
    nombre = document.getElementById('nombre').value;
    apellido = document.getElementById('apellido').value;
    identificacion = document.getElementById('identificacion').value;
    nacimiento = document.getElementById('nacimiento').value;
    ciudad = document.getElementById('ciudad').value;
    correo = document.getElementById('correo').value;
    telefono = document.getElementById('telefono').value;
    usuario = document.getElementById('usuario').value;

    var expresion = /\w+@\w+\.+[a-z]/;
    valContra =  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;


    if (nombre === "" || apellido === "" || identificacion === "" || nacimiento === "" || ciudad === "" || correo === "" || telefono === "" || usuario === "") {
        swal("Espera!","Todos los campos son obligatorios","warning");
        return false;
    }
    else if (nombre.length > 30) {
        swal("Espera!","El nombre es muy largo","warning");
        return false;
    }
    else if (!/^([a-zA-ZñÑáéíóúÁÉÍÓÚ\s\ ])+$/i.test(nombre)) {
        swal("Espera!","El nombre solo puede contener letras","warning");
        return false;
    }
    else if (apellido.length > 30) {
        swal("Espera!","El apellido es muy largo","warning");
        return false;
    }
    else if (!/^([a-zA-ZñÑáéíóúÁÉÍÓÚ\s\ ])+$/i.test(apellido)) {
        swal("Espera!","el apellido solo puede contener letras","warning");
        return false;
    }
    else if (identificacion.length > 12) {
        swal("Espera!","La identificacion es muy largo","warning");
        return false;
    }

    else if (correo.length > 50) {
        swal("Espera!","El correo es muy largo","warning");
        return false;
    }
    else if (telefono.length > 10) {
        swal("Espera!","El telefono contiene 10 digitos","warning");
        return false;
    }
    else if (usuario.length > 30) {
        swal("Espera!","El usuario superan los 30 caracteres","warning");
        return false;
    }
    else if (isNaN(telefono)) {
        swal("Espera!","El teléfono ingresado no es un número","warning");
        return false;
    }
    else if (!expresion.test(correo)) {
        swal("Espera!","El correo no es valido","warning");
        return false;
    }


}
