<%-- 
    Document   : contraseñaCorreo
    Created on : 10/11/2020, 07:16:21 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ServiHogar</title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/lib/unix.css" rel="stylesheet">
        <link href="CSS/lib/sweetalert/sweetalert.css" rel="stylesheet">
        <link href="CSS/styleee.css" rel="stylesheet">
        <script src="js/validaciones/recuperacionContraseña.js" type="text/javascript"></script>
    </head>

    <body class="bg-success">

        <div class="unix-login">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="login-content">
                            <div class="login-logo">
                                <a href="index.html"><span><img src="Imagenes/logoBlanco.png" alt="logo" height="50" ></span></a>
                            </div>
                            <div class="login-form">
                                <p class="nombrePa" style="text-align: center">Olvidaste tu contraseña</p>
                                <p class="introPa" style="text-align: center">Ingrese su correo electrónico para restablecer su contraseña y revisa tu correo</p>
                                <form method="psot" action="Usuario" onsubmit="return validar()">
                                    <input type="email" name="textCorreo" placeholder="Ejemplo1@gmail.com" class="correoRecuperar" id="correo">
                                    <input type="hidden" name="tipoCorreo" value="3">
                                    <button class="btn btn-warning btn-flat m-b-10 m-t-10">Enviar correo</button>
                                    <input name="opcion" type="hidden" value="11">
                                    <script>
                                        window.onload = function () {
                                        <% if (request.getAttribute("mensajeError") != null) {%>
                                            swal("Espera!", "<%=request.getAttribute("mensajeError")%>", "warning");
                                        <% } else if(request.getAttribute("mensajeExito") != null){%>
                                            swal("Excelente", "<%=request.getAttribute("mensajeExito")%>", "success");
                                        <% }%>
                                        };
                                    </script>
                                </form>
                                <a href="login.jsp"  class="btn btn-info btn-flat m-b-10 m-t-10" >Volver</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/lib/sweetalert/sweetalert.min.js"></script>
    </body>

</html>

