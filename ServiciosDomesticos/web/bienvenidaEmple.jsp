<%-- 
    Document   : bienvenidaEmple
    Created on : 17/09/2020, 04:58:33 PM
    Author     : Admin
--%>

<%@include file="sesiones.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ServiHogar</title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/lib/sweetalert/sweetalert.css" rel="stylesheet">
        <link href="CSS/lib/unix.css" rel="stylesheet">
        <link href="CSS/styleee.css" rel="stylesheet">
        <script src="js/validaciones/empleado.js" type="text/javascript"></script>
    </head>

    <body class="bg-success">

        <div class="unix-login">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="login-content">
                            <div class="login-logo">
                                <a href="index.html"><span><img src="Imagenes/logoBlanco.png" alt="logo" height="50" ></span></a>
                            </div>
                            <div class="login-form">
                                <p class="bienPa">Bienvenido</p>
                                <p class="nombrePa"><%=nombre%> <%=apellido%></p>
                                <p class="introPa">Para poder ofrecer tu servicio llena el siguiente formulario</p>
                                <form action="Empleado" method="post" onsubmit="return validar()">
                                    <input type="hidden" name="textId" class="form-control" value="<%=idEmpleado%>">
                                    <div class="form-group">
                                        <label>Experiencia laboral</label>
                                        <input type="text" name="textExperiencia" class="form-control" placeholder="Experiencia en años" onfocus="(this.value = 'años')" id="experiencia" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Descripcion</label>
                                        <textarea class="form-control" rows="3" placeholder="Escribe una descripción tuya" name="textDescripcion" id="descripcion"></textarea>
                                    </div>                             
                                    <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Continuar</button>
                                    <input type="hidden" name="opcion" value="1">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/lib/sweetalert/sweetalert.min.js"></script>
    </body>

</html>