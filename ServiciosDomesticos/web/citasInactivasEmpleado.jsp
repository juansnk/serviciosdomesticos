<%-- 
    Document   : citasInactivasEmpleado
    Created on : 27/10/2020, 06:43:55 PM
    Author     : Admin
--%>

<%@page import="modeloVO.EmpleadoVO"%>
<%@page import="modeloDAO.EmpleadoDAO"%>
<%@page import="modeloVO.EmpleadoVO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="modeloDAO.CitaDAO"%>
<%@page import="modeloVO.CitaVO"%>
<%@page import="java.util.ArrayList"%>
<%@include file="sesiones.jsp" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Webstrot Admin : Table</title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link href="CSS/lib/menubar/sidebar.css" rel="stylesheet">
        <link href="CSS/lib/sweetalert/sweetalert.css" rel="stylesheet">
        <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/lib/unix.css" rel="stylesheet">
        <link href="CSS/styleee.css" rel="stylesheet">
    </head>

    <body>
        <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <ul>
                        <li class="label">Principal</li>
                        <li class="active"><a href="indexEmpleado.jsp"><i class="ti-home"></i> Inicio</a></li>
                        <li class="label">Agenda</li>
                         <li><a href="listaAgenda.jsp"><i class="ti-pencil-alt"></i>Mi agenda</a>
                        </li>
                        <li class="label">Citas</li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i>Listas <span class="badge badge-primary">3</span><span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="citasActivasEmpleado.jsp">Activas</a></li>
                                <li><a href="citasInactivasEmpleado.jsp">Terminadas</a></li>
                            </ul>
                        </li>
                        <li class="label">Informacion</li>
                        <li><a href="calendarioEmpleado.jsp"><i class="ti-calendar"></i> Calendario </a></li>                    
                        <li><a href="perfilEmpleado.jsp"><i class="ti-user"></i> Perfil</a></li>
                        <li><a href="Sesiones"><i class="ti-close"></i> Cerrar Sesión</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /# sidebar -->
        <div class="header">
            <div class="pull-left">
                <div class="logo"><a href="index.jsp"><span><img src="Imagenes/logoNegro.png" height="50" /></span></a></div>

                <div class="hamburger sidebar-toggle">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>
            </div>

            <div class="pull-right p-r-15">
                <ul>
                    <li class="header-icon dib"><i class="ti-bell"><span class="badge badge-danger">.</span></i>
                        <div class="drop-down">
                            <div class="dropdown-content-heading">
                                <span class="text-left">Notificaciones recientes</span>
                            </div>
                            <div class="dropdown-content-body">
                                <ul>
                                    <%                                        EmpleadoVO empVO = new EmpleadoVO();
                                        EmpleadoDAO empDAO = new EmpleadoDAO();
                                        ArrayList<EmpleadoVO> listaCitasNo = empDAO.listarEmpleadaNotificacion(idEmpleado);
                                        for (int i = 0; i < listaCitasNo.size(); i++) {
                                            empVO = listaCitasNo.get(i);
                                    %>
                                    <li>
                                        <a href="#">
                                            <h4>Tienes una cita nueva</h4>
                                            <div class="notification-content">
                                                <small class="notification-timestamp pull-right"></small>
                                                <div class="notification-heading"><%=empVO.getUsuNombre()%> <%=empVO.getUsuApellido()%></div>
                                                <div class="notification-text"><%=empVO.getCitFecha()%>---<%=empVO.getCitHoraInicio()%> </div>

                                                <a href="Cita?opcion=9&textId=<%=empVO.getIdCita()%>&tipoCorreo=4&correo=<%=empVO.getUsuCorreo()%>" class="btn btn-danger">Cancelar</a> <a href="Cita?textEstado=1&opcion=10&textId=<%=empVO.getIdCita()%>" class="btn btn-success">Aceptar</a> <a href="Cita?opcion=5&textId=<%=empVO.getIdCita()%>" class="btn btn-default">Ver</a>
                                            </div>
                                        </a>
                                    </li>
                                    <%}%>
                                    <li class="text-center">
                                        <a href="#" class="more-link">Cerrar</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li class="header-icon dib"><img class="avatar-img" src="Imagenes/usuario.png" alt="" /> <span class="user-avatar"> <%=usuario%> <i class="ti-angle-down f-s-10"></i></span>
                        <div class="drop-down dropdown-profile">
                            <div class="dropdown-content-heading">
                            </div>
                            <div class="dropdown-content-body">
                                <ul>
                                   <li><a href="perfilEmpleado.jsp"><i class="ti-user"></i> <span>Perfil</span></a></li>
                                    <li><a href="calendarioEmpleado.jsp"><i class="ti-calendar"></i> <span>Calendario</span></a></li>
                                    <li><a href="modificarContraseñaEmpleado.jsp"><i class="ti-lock"></i> <span>Cambiar contraseña</span></a></li>
                                    <li><a href="Sesiones"><i class="ti-power-off"></i> <span>Cerrar sesión</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="content-wrap">
            <div class="main">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-8 p-r-0 title-margin-right">
                            <div class="page-header">
                                <div class="page-title">
                                    <h1>Citas</h1>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                        <div class="col-lg-4 p-l-0 title-margin-left">
                            <div class="page-header">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="indexEmpleado.jsp">Inicio</a></li>
                                        <li class="active">Citas terminadas</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                    <div id="main-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header pr">
                                        <h4>Citas terminadas </h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table student-data-table m-t-20">
                                                <thead>
                                                    <tr>
                                                        <th>Foto</th>
                                                        <th>Nombre empleada</th>    
                                                        <th>Fecha</th>
                                                        <th>Calificacion</th>
                                                        <th>Direccion</th>
                                                        <th>Hora de inicio</th>
                                                        <th>Hora de fin</th>
                                                        <th>Total Precio</th>
                                                        <th>Estado</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%
  
                                                        EmpleadoVO emppVO= new EmpleadoVO();
                                                        ArrayList<EmpleadoVO> listaCitasInactivas= empDAO.listarCitasInactivasEmpleada(idEmpleado);
                                                        for (int i = 0; i < listaCitasInactivas.size(); i++) {
                                                            emppVO = listaCitasInactivas.get(i);
                                                    %>
                                                    <tr>
                                                        <td>
                                                            <div class="round-img">
                                                                <a href=""><img src="ImagenControlador?opcionImg=3&idUsuario=<%=emppVO.getIdUsuario()%>" alt="" width="50px" height="50px" ></a>
                                                            </div>
                                                        </td>
                                                        <td><%=emppVO.getUsuNombre()%> <%=emppVO.getUsuApellido()%></td>
                                                        <td><%=emppVO.getCitFecha()%></td>
                                                        <td><%=emppVO.getCitCalificacion()%></td>
                                                        <td><%=emppVO.getCitDireccion()%></td>
                                                        <td><%=emppVO.getCitHoraInicio()%></td>
                                                        <td><%=emppVO.getCitHoraFin()%></td>
                                                        <td><%=emppVO.getCitPrecio()%></td>
                                                        <td <%=emppVO.getCitEstado()%>><span class="badge badge-warning">Inactivo</span></td>
                                                        <td style="text-align: center">
                                                            <span><a href="Cita?opcion=12&textId=<%=emppVO.getIdCita()%>&textEstado=<%=emppVO.getCitEstado()%>"><i class="ti-eye color-default" style="font-size: 18px"></i></a> </span>
                                                            <span><a href="Cita?opcion=13&textId=<%=emppVO.getIdCita()%>" class="btn sweet-confirm" onclick="return citasCanceladas(event)"><i class="ti-trash color-danger" style="font-size: 18px"></i> </a></span>

                                                        </td>
                                                    </tr>
                                                    <%}%>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->

                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="footer">
                                    <p>ServiHogar <span id="date-time"></span></p>
                                </div>
                            </div>
                        </div>                   
                        <!-- /# row -->


                    </div>
                </div>
            </div>
        </div>






        <div id="search">
            <button type="button" class="close">×</button>
            <form>
                <input type="search" value="" placeholder="type keyword(s) here" />
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
        <!-- jquery vendor -->
        <script src="js/lib/jquery.min.js"></script>
        <script src="js/lib/jquery.nanoscroller.min.js"></script>
        <!-- nano scroller -->
        <script src="js/lib/rating1/jRate.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/rating1/jRate.init.js"></script>
        <script src="js/lib/menubar/sidebar.js"></script>
        <script src="js/lib/preloader/pace.min.js"></script>
        <script src="js/lib/sweetalert/sweetalert.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/sweetalert/sweetalert.init.js"></script>
        <!-- sidebar -->
        <script src="js/lib/bootstrap.min.js"></script>
        <!-- bootstrap -->
        <script src="js/scripts.js"></script>
        <!-- scripit init-->





    </body>

</html>
