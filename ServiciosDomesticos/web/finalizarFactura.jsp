<%-- 
    Document   : finalizarFactura
    Created on : 23/10/2020, 11:23:03 PM
    Author     : Admin
--%>
<%@include file="sesiones.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Webstrot Admin : Invoice</title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
    <link href="CSS/lib/themify-icons.css" rel="stylesheet">
    <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
    <link href="CSS/lib/unix.css" rel="stylesheet">
    <link href="CSS/styleee.css" rel="stylesheet">
    </head>

    <body class="bg-success">

        <div class="unix-login">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="index.html"><span><img src="Imagenes/logoBlanco.png" alt="logo" height="50" ></span></a>
                        </div>
                        <div class="login-form">
                            <p class="nombrePa" style="text-align: center">Tu cita fue agendada con éxito, espera que tu empleado acepte la cita</p>
                            <p class="introPa" style="text-align: center">Gracias por preferirnos</p>
                             <a href="indexCliente.jsp"  class="btn btn-warning btn-flat m-b-10 m-t-10">Volver al inicio</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>

</html>
