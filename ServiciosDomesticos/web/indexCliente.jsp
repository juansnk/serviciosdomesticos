<%-- 
    Document   : indexCliente
    Created on : 24/06/2020, 05:54:53 PM
    Author     : Admin
--%>


<%@page import="java.util.ArrayList"%>
<%@page import="modeloDAO.CitaDAO"%>
<%@page import="modeloVO.CitaVO"%>
<%@include file="sesiones.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ServiHogar</title>
        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">
        <!-- Styles -->
        <link href="CSS/lib/calendar2/semantic.ui.min.css" rel="stylesheet">
        <link href="CSS/lib/calendar2/pignose.calendar.min.css" rel="stylesheet">
        <link href="CSS/lib/chartist/chartist.min.css" rel="stylesheet">
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link href="CSS/lib/owl.carousel.min.css" rel="stylesheet" />
        <link href="CSS/lib/owl.theme.default.min.css" rel="stylesheet" />
        <link href="CSS/lib/weather-icons.css" rel="stylesheet" />
        <link href="CSS/lib/menubar/sidebar.css" rel="stylesheet">
        <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/lib/unix.css" rel="stylesheet">
        <link href="CSS/lib/sweetalert/sweetalert.css" rel="stylesheet">
        <link href="CSS/styleee.css" rel="stylesheet">
        <link href="CSS/popupRegistrar.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <ul>
                        <li class="label">Principal</li>
                        <li class="active"><a href="indexCliente.jsp"><i class="ti-home"></i> Inicio  </a></li>
                        <li class="label">Citas</li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i>Listas <span class="badge badge-primary">3</span><span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="citasActivas.jsp">Activas</a></li>
                                <li><a href="citasInactivas.jsp">Terminadas</a></li>
                                <li><a href="citasCanceladas.jsp">Canceladas</a></li>   
                            </ul>
                        </li>

                        <li class="label">Empleados</li>
                        <li><a href="buscarEmpleada.jsp"><i class="ti-search"></i> Busca tu empleada </a></li>

                        <li class="label">Informacion</li>
                        <li><a href="calendarioCliente.jsp"><i class="ti-calendar"></i> Calendario </a></li>
                        <li><a href="perfilCliente.jsp"><i class="ti-user"></i> Perfil</a></li>

                        <li><a href="Sesiones"><i class="ti-close"></i> Cerrar sesion</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /# sidebar -->

        <div class="header">
            <div class="pull-left">
                <div class="logo"><a href="indexCliente.jsp"><span><img src="Imagenes/logoNegro.png" height="50" /></span></a></div><div class="hamburger sidebar-toggle">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>
            </div>
            <div class="pull-right p-r-15">
                <ul>
                    <li class="header-icon dib"><img class="avatar-img" src="Imagenes/usuario.png" alt="" /> <span class="user-avatar"> <%=usuario%> <i class="ti-angle-down f-s-10"></i></span>
                        <div class="drop-down dropdown-profile">
                            <div class="dropdown-content-body">
                                <ul>
                                    <li><a href="perfilCliente.jsp"><i class="ti-user"></i> <span>Perfil</span></a></li>
                                    <li><a href="calendarioCliente.jsp"><i class="ti-calendar"></i> <span>Calendario</span></a></li>
                                    <li><a href="modificarContraseñaCliente.jsp"><i class="ti-lock"></i> <span>Cambiar contraseña</span></a></li>
                                    <li><a href="Sesiones"><i class="ti-power-off"></i> <span>Cerrar sesión</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="content-wrap">
            <div class="main">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-8 p-r-0 title-margin-right">
                            <div class="page-header">
                                <div class="page-title">
                                    <h1>Hola, <span>bienvenido <%=nombre%></span></h1>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                        <div class="col-lg-4 p-l-0 title-margin-left">
                            <div class="page-header">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="indexCliente.jsp">Inicio</a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                    <div id="main-content">
                        <div class="row">
                            <a href="buscarEmpleada.jsp">
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="stat-widget-five">
                                            <div class="stat-icon">
                                                <i class="ti-search bg-primary"></i>
                                            </div>
                                            <div class="stat-content">
                                                <div class="stat-heading color-primary">Buscar empleada</div>
                                                <div class="stat-text">ServiHogar</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="perfilCliente.jsp">
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="stat-widget-five">
                                            <div class="stat-icon">
                                                <i class="ti-user bg-success"></i>
                                            </div>
                                            <div class="stat-content">
                                                <div class="stat-heading color-success">Perfil</div>
                                                <div class="stat-text">ServiHogar</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="citasActivas.jsp">
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="stat-widget-five">
                                            <div class="stat-icon">
                                                <i class="ti-pencil-alt bg-danger"></i>
                                            </div>
                                            <div class="stat-content">
                                                <div class="stat-heading color-primary">Citas</div>
                                                <div class="stat-text">ServiHogar</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="calendarioCliente.jsp">
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="stat-widget-five">
                                            <div class="stat-icon bg-warning">
                                                <i class="ti-calendar"></i>
                                            </div>
                                            <div class="stat-content">
                                                <div class="stat-heading color-primary">Calendario</div>
                                                <div class="stat-text">ServiHogar</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="row">
                            <!-- /# column -->
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header pr">
                                        <h4>Citas pendientes</h4>
                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-option drop-menu"><i class="ti-settings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="link"></i></li>   
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table student-data-table m-t-20">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre empleada</th>
                                                        <th>Ciudad</th>
                                                        <th>Fecha</th>
                                                        <th>Día</th>
                                                        <th>Dirección</th>
                                                        <th>Hora de inicio</th>
                                                        <th>Hora de fin</th>
                                                        <th>Estado</th>          
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%
                                                        CitaVO citVO = new CitaVO();
                                                        CitaDAO citDAO = new CitaDAO();
                                                        ArrayList<CitaVO> listaCitas = citDAO.listarCitas(idCliente);
                                                        for (int i = 0; i < listaCitas.size(); i++) {
                                                            citVO = listaCitas.get(i);

                                                    %>
                                                    <tr>
                                                        <td><%=citVO.getUsuNombre()%> <%=citVO.getUsuApellido()%></td>
                                                        <td><%=citVO.getUsuCiudad()%></td>
                                                        <td><%=citVO.getCitFecha()%></td>
                                                        <td><%=citVO.getProDia()%></td>
                                                        <td><%=citVO.getCitDireccion()%></td>
                                                        <td><%=citVO.getCitHoraInicio()%></td>
                                                        <td><%=citVO.getCitHoraFin()%></td>
                                                        <td style="text-align: center"><span class="badge badge-primary">Activa</span></td>
                                                    </tr>
                                                    <%}%>   
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->
                        </div>
                        <!-- /# row -->
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="card alert">
                                    <div class="card-header">
                                        <h4>Calendario</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="year-calendar"></div>
                                    </div>
                                </div>
                                <!-- /# card -->
                            </div>
                            <div class="col-lg-8">
                                <div class="card alert">
                                    <div class="card-header pr">
                                        <h4>Citas en proceso</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table student-data-table m-t-20">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre empleada</th>
                                                        <th>Ciudad</th>
                                                        <th>Fecha</th>
                                                        <th>Día</th>
                                                        <th>Hora de inicio</th>
                                                        <th>Hora de fin</th>
                                                        <th>Estado</th>
                                                        <th>Acción</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%
                                                        ArrayList<CitaVO> listaCitasP = citDAO.listarCitasEnProceso(idCliente);
                                                        for (int i = 0; i < listaCitasP.size(); i++) {
                                                            citVO = listaCitasP.get(i);

                                                    %>
                                                    <tr>
                                                        <td><%=citVO.getUsuNombre()%> <%=citVO.getUsuApellido()%></td>
                                                        <td><%=citVO.getUsuCiudad()%></td>
                                                        <td><%=citVO.getCitFecha()%></td>
                                                        <td><%=citVO.getProDia()%></td>
                                                        <td><%=citVO.getCitHoraInicio()%></td>
                                                        <td><%=citVO.getCitHoraFin()%></td>
                                                        <td <%=citVO.getCitEstado()%> style="text-align: center"><span class="badge badge-primary">Activa</span></td>
                                                        <td><button id="btn-abrir-popup" class="btn btn-danger">Finalizar cita</button></td>   

                                                    </tr>
                                                    <div class="overlayy" id="overlayy">
                                                    <div class="popup" id="popup">
                                                        <h4 class="modal-title"><strong>Califica tu cita</strong></h4>
                                                        <form method='post' action='Cita'>
                                                            <div class="modal-header">
                                                                <div class='row'>
                                                                    <div class='col-md-6'>
                                                                        <div class='form-group'>
                                                                            <label class='control-label'>Califica el servicio</label>     
                                                                            <div class="card alert">
                                                                                <div class="card-header">
                                                                                    <h4>Califica</h4>
                                                                                </div>
                                                                                <div class="rating1">
                                                                                    <div id="no-checked-stars"></div>
                                                                                    <input id="ratingNum" type="hidden" name="textCalificacion">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class='col-md-6'>
                                                                        <div class='form-group'>
                                                                            <label class='control-label'>Opinion</label>
                                                                            <textarea class='form-control'name='textOpinion' rows="6" cols="10" placeholder="¿Que opinas del servicio?"></textarea>                                           
                                                                        </div>
                                                                    </div>
                                                                    <input class='form-control'type='hidden' name='textId' value='<%=citVO.getIdCita()%>'/>
                                                                    <input class='form-control'type='hidden' name='textEstado' value='2'/>
                                                                    <input class='form-control'type='hidden' name='textIdProgramacion' value='<%=citVO.getAgenda_idProgramacion()%>'/>
                                                                    <a href="#" type="button" class="btn btn-default waves-effect"  id="btn-cerrar-popup" >Cerrar</a>
                                                                    <button class="btn btn-success save-event waves-effect waves-light">Finalizar</button>
                                                                    <input type="hidden" name="opcion" value="8">   
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                    <%}%>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# column -->

                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="footer">
                                    <p>ServiHogar <span id="date-time"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                                                    <script>
            window.onload = function () {
            <% if (request.getAttribute("mensajeError") != null) {%>
                swal("Tienes una cita en proceso", "<%=request.getAttribute("mensajeError")%>", "warning");
            <% }%>
            };
        </script>

        <div id="search">
            <button type="button" class="close">×</button>
            <form>
                <input type="search" value="" placeholder="type keyword(s) here" />
                <button type="submit" class="btn btn-primary">Buscar</button>
            </form>
        </div>
        <!-- jquery vendor -->
        <script src="js/popup.js"></script>
        <script src="js/lib/jquery.min.js"></script>
        <script src="js/lib/jquery.nanoscroller.min.js"></script>
        <!-- nano scroller -->
        <script src="js/lib/menubar/sidebar.js"></script>
        <script src="js/lib/preloader/pace.min.js"></script>
        <!-- sidebar -->
        <script src="js/lib/bootstrap.min.js"></script>
        <!-- bootstrap -->

        <script src="js/lib/calendar-2/moment.latest.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/semantic.ui.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/prism.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/pignose.calendar.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/pignose.init.js"></script>
     

   
        <!-- scripit init-->
        <script src="js/lib/sweetalert/sweetalert.min.js"></script>

        <script src="js/lib/rating1/jRate.min.js"></script>
    <!-- scripit init-->
        <script src="js/lib/rating1/jRate.init.js"></script> 
        <script src="js/lib/weather/jquery.simpleWeather.min.js"></script>
        <script src="s/lib/weather/weather-init.js"></script>
        <script src="js/lib/circle-progress/circle-progress.min.js"></script>
        <script src="js/lib/circle-progress/circle-progress-init.js"></script>
        <script src="js/lib/chartist/chartist.min.js"></script>
        <script src="js/lib/chartist/chartist-init.js"></script>
        <script src="js/lib/sparklinechart/jquery.sparkline.min.js"></script>
        <script src="js/lib/sparklinechart/sparkline.init.js"></script>
        <script src="js/lib/owl-carousel/owl.carousel.min.js"></script>
        <script src="js/lib/owl-carousel/owl.carousel-init.js"></script>
        <script src="js/scripts.js"></script>
        
        <!-- scripit init-->
    </body>

</html>