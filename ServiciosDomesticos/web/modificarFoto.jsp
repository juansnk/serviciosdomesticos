<%-- 
    Document   : modificarFoto
    Created on : 1/12/2020, 12:00:48 PM
    Author     : Admin
--%>

<%@include file="sesiones.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ServiHogar</title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <!-- Styles -->
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/lib/unix.css" rel="stylesheet">
        <link href="CSS/styleee.css" rel="stylesheet">
        <link href="CSS/pefil.css" rel="stylesheet" type="text/css"/>
        <script src="js/validaciones/cliente.js" type="text/javascript"></script>
    </head>

    <body class="bg-success">

        <div class="unix-login">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="login-content">
                            <div class="login-logo">
                                <a href="index.html"><span><img src="Imagenes/logoBlanco.png" alt="logo" height="50" ></span></a>
                            </div>
                            <div class="login-form">
                                <p class="bienPa" style="text-align: center">Cambiar foto de perfil</p>
                                <p class="introPa">Carga una foto nueva</p>
                                <form action="Usuario" method="post" onsubmit="return validar()" enctype="multipart/form-data">
                                         <input type="hidden" name="textId" class="form-control" value="<%=idUsuario%>">
                                            <div class="col-xs-12">
                                                <div class="center-block">
                                                <div class="profile-img">
                                                    <img src="ImagenControlador?opcionImg=4&idUsuario=<%=idUsuario%>" alt="" id="img"/>
                                                    <div class="file btn btn-lg btn-primary">
                                                        Cargar foto 
                                                        <input type="file" name="textFoto" onchange="mostrarImagen();"/>
                                                    </div>
                                                </div>
                                                    <input type="hidden" name="textRol" value="<%=rol%>">
                                                    
                                                </div>
                                            </div>
                                        <input type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30" value="Continuar">
                                       <a href="javascript: history.go(-1)" class="btn btn-success btn-flat">Volver</a>
                                        <input type="hidden" name="opcion" value="15">
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            function mostrarImagen(){
                var preview = document.getElementById('img');
                var file = document.querySelector('input[type=file]').files[0];
                
                var leer = new FileReader();
                if (file){
                    leer.readAsDataURL(file);
                    leer.onloadend=function(){
                        preview.src=leer.result;
                    };
                }else{
                    preview.src=""; 
                }
            }
        </script>
    </body>

</html>

