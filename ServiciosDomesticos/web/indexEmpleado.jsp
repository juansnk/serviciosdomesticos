<%-- 
    Document   : indexEmpleado
    Created on : 15/06/2020, 07:37:02 PM
    Author     : Admin
--%>

<%@page import="modeloDAO.CitaDAO"%>
<%@page import="modeloVO.CitaVO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modeloDAO.EmpleadoDAO"%>
<%@page import="modeloVO.EmpleadoVO"%>
<%@include file="sesiones.jsp" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Webstrot Admin : Dashboard</title>

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon--> 
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon--> 
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

        <link href="CSS/lib/chartist/chartist.min.css" rel="stylesheet">
        <!-- Styles -->
        <link href="CSS/lib/calendar2/semantic.ui.min.css" rel="stylesheet">
        <link href="CSS/lib/calendar2/pignose.calendar.min.css" rel="stylesheet">
        <link href="CSS/lib/owl.carousel.min.css" rel="stylesheet" />
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link href="CSS/lib/menubar/sidebar.css" rel="stylesheet">
        <link href="CSS/lib/sweetalert/sweetalert.css" rel="stylesheet">
        <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/lib/unix.css" rel="stylesheet">
        <link href="CSS/styleee.css" rel="stylesheet">
    </head>

    <body>
        <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <ul>
                        <li class="label">Principal</li>
                        <li class="active"><a href="indexEmpleado.jsp"><i class="ti-home"></i> Inicio</a></li>
                        <li class="label">Agenda</li>
                        <li><a href="listaAgenda.jsp"><i class="ti-pencil-alt"></i>Mi agenda</a>
                        </li>
                        <li class="label">Citas</li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i>Listas <span class="badge badge-primary">3</span><span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="citasActivasEmpleado.jsp">Activas</a></li>
                                <li><a href="citasInactivasEmpleado.jsp">Terminadas</a></li>
                            </ul>
                        </li>
                        <li class="label">Informacion</li>
                        <li><a href="calendarioEmpleado.jsp"><i class="ti-calendar"></i> Calendario </a></li>                    
                        <li><a href="perfilEmpleado.jsp"><i class="ti-user"></i> Perfil</a></li>
                        <li><a href="Sesiones"><i class="ti-close"></i> Cerrar Sesión</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /# sidebar -->
        <div class="header">
            <div class="pull-left">
                <div class="logo"><a href="index.jsp"><span><img src="Imagenes/logoNegro.png" height="50" /></span></a></div>

                <div class="hamburger sidebar-toggle">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>
            </div>

            <div class="pull-right p-r-15">
                <ul>
                    <li class="header-icon dib"><i class="ti-bell"><span class="badge badge-danger">.</span></i>
                        <div class="drop-down">
                            <div class="dropdown-content-heading">
                                <span class="text-left">Notificaciones recientes</span>
                            </div>
                            <div class="dropdown-content-body">
                                <ul>
                                    <%                                        EmpleadoVO empVO = new EmpleadoVO();
                                        EmpleadoDAO empDAO = new EmpleadoDAO();
                                        ArrayList<EmpleadoVO> listaCitasNo = empDAO.listarEmpleadaNotificacion(idEmpleado);
                                        for (int i = 0; i < listaCitasNo.size(); i++) {
                                            empVO = listaCitasNo.get(i);
                                    %>
                                    <li>
                                        <a href="#">
                                            <h4>Tienes una cita nueva</h4>
                                            <div class="notification-content">
                                                <small class="notification-timestamp pull-right"></small>
                                                <div class="notification-heading"><%=empVO.getUsuNombre()%> <%=empVO.getUsuApellido()%></div>
                                                <div class="notification-text"><%=empVO.getCitFecha()%>---<%=empVO.getCitHoraInicio()%> </div>

                                                <a href="Cita?opcion=9&textId=<%=empVO.getIdCita()%>&tipoCorreo=4&correo=<%=empVO.getUsuCorreo()%>" class="btn btn-danger">Cancelar</a> <a href="Cita?textEstado=1&opcion=10&textId=<%=empVO.getIdCita()%>" class="btn btn-success">Aceptar</a> <a href="Cita?opcion=5&textId=<%=empVO.getIdCita()%>" class="btn btn-default">Ver</a>
                                            </div>
                                        </a>
                                    </li>
                                    <%}%>
                                    <li class="text-center">
                                        <a href="#" class="more-link">Cerrar</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li class="header-icon dib"><span class="user-avatar"> <%=usuario%> <i class="ti-angle-down f-s-10"></i></span>
                        <div class="drop-down dropdown-profile">
                            <div class="dropdown-content-heading">
                            </div>
                            <div class="dropdown-content-body">
                                <ul>
                                    <li><a href="perfilEmpleado.jsp"><i class="ti-user"></i> <span>Perfil</span></a></li>
                                    <li><a href="calendarioEmpleado.jsp"><i class="ti-calendar"></i> <span>Calendario</span></a></li>
                                    <li><a href="modificarContraseñaEmpleado.jsp"><i class="ti-lock"></i> <span>Cambiar contraseña</span></a></li>
                                    <li><a href="Sesiones"><i class="ti-power-off"></i> <span>Cerrar sesión</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="content-wrap">
            <div class="main">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-8 p-r-0 title-margin-right">
                            <div class="page-header">
                                <div class="page-title">
                                    <h1>Hola, <span>bienvenido <%=nombre%></span></h1>
                                </div>
                            </div>
                        </div><!-- /# column -->
                        <div class="col-lg-4 p-l-0 title-margin-left">
                            <div class="page-header">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="#">Inicio</a></li>
                                        <li class="active">Inicio</li>
                                    </ol>
                                </div>
                            </div>
                        </div><!-- /# column -->
                    </div><!-- /# row -->
                    <div id="main-content">
                        <div class="row">
                            <a href="listaAgenda.jsp">
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="stat-widget-five">
                                            <div class="stat-icon">
                                                <i class="ti-agenda bg-primary"></i>
                                            </div>
                                            <div class="stat-content">
                                                <div class="stat-heading color-primary">Agenda</div>
                                                <div class="stat-text">ServiHogar</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="perfilEmpleado.jsp">
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="stat-widget-five">
                                            <div class="stat-icon">
                                                <i class="ti-user bg-success"></i>
                                            </div>
                                            <div class="stat-content">
                                                <div class="stat-heading color-success">Perfil</div>
                                                <div class="stat-text">ServiHogar</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="citasActivasEmpleado.jsp">
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="stat-widget-five">
                                            <div class="stat-icon">
                                                <i class="ti-pencil-alt bg-danger"></i>
                                            </div>
                                            <div class="stat-content">
                                                <div class="stat-heading color-primary">Citas</div>
                                                <div class="stat-text">ServiHogar</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="calendarioEmpleado.jsp">
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="stat-widget-five">
                                            <div class="stat-icon bg-warning">
                                                <i class="ti-calendar"></i>
                                            </div>
                                            <div class="stat-content">
                                                <div class="stat-heading color-primary">Calendario</div>
                                                <div class="stat-text">ServiHogar</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header">
                                        <h4>Citas pendientes </h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre de cliente</th>
                                                        <th>Fecha</th>
                                                        <th>Direccion</th>
                                                        <th>Hora de inicio</th>
                                                        <th>Hora de fin</th>
                                                        <th>Total precio</th>
                                                        <th>Estado</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%
                                                        ArrayList<EmpleadoVO> listaCitas = empDAO.listarCitasEmpleada(idEmpleado);
                                                        for (int i = 0; i < listaCitas.size(); i++) {
                                                            empVO = listaCitas.get(i);
                                                    %>
                                                    <tr>
                                                        <td><%=empVO.getUsuNombre()%> <%=empVO.getUsuApellido()%></td>
                                                        <td><span><%=empVO.getCitFecha()%></span></td>
                                                        <td><span><%=empVO.getCitDireccion()%></span></td>
                                                        <td><span><%=empVO.getCitHoraInicio()%></span></td>
                                                        <td><span><%=empVO.getCitHoraFin()%></span></td>
                                                        <td><span><%=empVO.getCitPrecio()%></span></td>
                                                        <td style="text-align: center"><span class="badge badge-primary">Activa</span></td>
                                                    </tr>
                                                    <%}%>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- /# column -->
                            <div class="col-lg-6">
                                <div class="card alert">
                                    <div class="card-header">
                                        <h4 class="m-b-30">Opiniones </h4>
                                        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
                                    </div>
                                    <div class="recent-comment">
                                        <%
                                            ArrayList<EmpleadoVO> listaOpi = empDAO.listarOpiniones(idEmpleado);
                                            for (int i = 0; i < listaOpi.size(); i++) {
                                                empVO = listaOpi.get(i);
                                        %>
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#"><img class="media-object" src="ImagenControlador?opcionImg=3&idUsuario=<%=empVO.getIdUsuario()%>" alt="..." width="70px" height="70px"></a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading"><%=empVO.getUsuNombre()%> <%=empVO.getUsuApellido()%></h4>
                                                <p><%=empVO.getCitOpinion()%></p>
                                                <div class="comment-action">
                                                    <div class="badge badge-warning">Terminado</div>
                                                    <span class="m-l-10">
                                                        <a href="#"><i class="fa fa-reply color-primary"></i></a>
                                                    </span>
                                                </div>
                                                <p class="comment-date"><%=empVO.getCitFecha()%></p>
                                            </div>                                                 
                                        </div>
                                        <%}%>
                                    </div>
                                </div>
                                <!-- /# card -->
                            </div>
                            <!-- /# column -->
                            <div class="col-lg-6">
                                <div class="card alert">
                                    <div class="card-header">
                                        <h4>Calendario</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="year-calendar"></div>
                                    </div>
                                </div>
                                <!-- /# card -->
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="footer">
                                    <p>ServiHogar <span id="date-time"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            window.onload = function () {
            <% if (request.getAttribute("mensajeError") != null) {%>
                swal("Hola!", "<%=request.getAttribute("mensajeError")%>", "warning");
            <% }%>
            };
        </script>

        <!-- jquery vendor -->
        <script src="js/lib/jquery.min.js"></script>
        <!-- nano scroller -->
        <script src="js/lib/jquery.nanoscroller.min.js"></script>
        <!-- sidebar -->
        <script src="js/lib/menubar/sidebar.js"></script>
        <!-- bootstrap -->

        <script src="js/lib/calendar-2/moment.latest.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/semantic.ui.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/prism.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/pignose.calendar.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/calendar-2/pignose.init.js"></script>

        <script src="js/lib/bootstrap.min.js"></script>
        <!-- Circle Progress Bar -->
        <script src="js/lib/circle-progress/circle-progress.min.js"></script>
        <script src="js/lib/circle-progress/circle-progress-init.js"></script>
        <script src="js/lib/chartist/chartist.min.js"></script>
        <script src="js/lib/chartist/chartist-init.js"></script>
        <script src="js/lib/sparklinechart/jquery.sparkline.min.js"></script>
        <script src="js/lib/sparklinechart/sparkline.init.js"></script>
        <!-- Bar Chat Js -->
        <script src="js/lib/datamap/d3.min.js"></script>
        <script src="js/lib/datamap/topojson.js"></script>
        <script src="js/lib/datamap/datamaps.world.min.js"></script>
        <script src="js/lib/datamap/datamap-init.js"></script>

        <script src="js/lib/morris-chart/raphael-min.js"></script>
        <script src="js/lib/morris-chart/morris.js"></script>
        <script src="js/lib/morris-chart/morris-init.js"></script>
        <script src="js/lib/sweetalert/sweetalert.min.js"></script>

        <script src="js/lib/peitychart/jquery.peity.min.js"></script>
        <!-- scripit init-->
        <script src="js/lib/peitychart/peitychart.init.js"></script>
        <script src="js/lib/owl-carousel/owl.carousel.min.js"></script>
        <script src="js/lib/owl-carousel/owl.carousel-init.js"></script>
        <script src="js/scripts.js"></script>
    </body>

</html>