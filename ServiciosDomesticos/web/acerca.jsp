<%-- 
    Document   : acerca
    Created on : 22/07/2020, 03:43:56 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>ServiHogar</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="CSS/popupRegistrar.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="CSS/open-iconic-bootstrap.min.css">
        <link rel="stylesheet" href="CSS/animate.css">

        <link rel="stylesheet" href="CSS/owl.carousel.min.css">
        <link rel="stylesheet" href="CSS/owl.theme.default.min.css">
        <link rel="stylesheet" href="CSS/magnific-popup.css">

        <link rel="stylesheet" href="CSS/aos.css">

        <link rel="stylesheet" href="CSS/ionicons.min.css">
        <link href="CSS/lib/sweetalert/sweetalert.css" rel="stylesheet">
        <link rel="stylesheet" href="CSS/flaticon.css">
        <link rel="stylesheet" href="CSS/icomoon.css">
        <link rel="stylesheet" href="CSS/style.css">
        <script src="js/validaciones/usuario.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="bg-top navbar-light d-flex flex-column-reverse">
            <div class="container py-3">
                <div class="row no-gutters d-flex align-items-center align-items-stretch">
                    <div class="col-md-4 d-flex align-items-center py-4">
                        <img src="Imagenes/Logo.jpg" alt=""/>
                    </div>
                    <div class="col-lg-8 d-block">
                        <div class="row d-flex">
                            <div class="col-md d-flex topper align-items-center align-items-stretch py-md-4">
                                <div class="icon d-flex justify-content-center align-items-center"><span class="ion-ios-paper-plane"></span></div>
                                <div class="text">
                                    <span>Servicios</span>
                                    <span>Hogar</span>
                                </div>
                            </div>
                            <div class="col-md d-flex topper align-items-center align-items-stretch py-md-4">
                                <div class="icon d-flex justify-content-center align-items-center"><span class="ion-ios-call"></span></div>
                                <div class="text">
                                    <span>Servicios</span>
                                    <span>Oficina</span>                                 
                                </div>
                            </div>
                            <div class="col-md d-flex topper align-items-center align-items-stretch py-md-4">
                                <div class="icon d-flex justify-content-center align-items-center"><span class="ion-ios-time"></span></div>
                                <div class="text">
                                    <span>Servicios</span>
                                    <span>Restaurante</span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
            <div class="container d-flex align-items-center">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="oi oi-menu"></span> Menú
                </button>
                <form action="#" class="searchform order-lg-last">
                    <div class="form-group d-flex">
                        <input type="text" class="form-control pl-3" placeholder="Search">
                        <button type="submit" placeholder="" class="form-control search"><span class="ion-ios-search"></span></button>
                    </div>
                </form>
                <div class="collapse navbar-collapse" id="ftco-nav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active"><a href="index.jsp" class="nav-link">Inicio</a></li>
                        <li class="nav-item"><a href="acerca.jsp" class="nav-link">Acerca de</a></li>
                        <li class="nav-item"><a href="hogar.jsp" class="nav-link">Hogar</a></li>
                        <li class="nav-item"><a href="oficina.jsp" class="nav-link">Oficina</a></li>
                        <li class="nav-item"><a href="restaurante.jsp" class="nav-link">Restaurante</a></li>
                        <li class="nav-item"><a href="login.jsp" class="nav-link" class="btn-abrir-popup" id="btn-abrir-popup">Iniciar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- END nav -->

        <section class="hero-wrap hero-wrap-2" style="background-image: url('Imagenes/portada1.jpg');">
            <div class="overlay"></div>
            <div class="container">
                <div class="row no-gutters slider-text align-items-center justify-content-center">
                    <div class="col-md-9 ftco-animate text-center">
                        <h1 class="mb-2 bread">Acerca de</h1>
                        <p class="breadcrumbs"><span class="mr-2"><a href="index.jsp">Inicio <i class="ion-ios-arrow-forward"></i></a></span> <span>Acerca de<i class="ion-ios-arrow-forward"></i></span></p>
                    </div>
                </div>
            </div>
        </section>

        <section class="ftco-section ftco-about ftco-no-pt ftco-no-pb ftco-counter" id="section-counter">
            <div class="container consult-wrap">
                <div class="row d-flex align-items-stretch">
                    <div class="col-md-6 wrap-about align-items-stretch d-flex">
                        <div class="img" style="background-image: url(Imagenes/imagen3.jpg);"></div>
                    </div>
                    <div class="col-md-6 wrap-about ftco-animate py-md-5 pl-md-5">
                        <div class="heading-section mb-4">
                            <span class="subheading">Bienvenido a servihogar</span>
                            <h2>La opción más intelegente para solicitar tu servicio para el lugar que necesitas</h2>
                        </div>
                        <p>Más linda que una azucena, más limpia que una patena. Un baño limpio es reflejo de quién lo usa, no de quién lo limpia. No es más limpio el que más limpió, sino el que menos ensucia.</p>
                        <div class="tabulation-2 mt-4">
                            <ul class="nav nav-pills nav-fill d-md-flex d-block">
                                <li class="nav-item">
                                    <a class="nav-link active py-2" data-toggle="tab" href="#home1"><span class="ion-ios-home mr-2"></span> Nuestra misión</a>
                                </li>
                                <li class="nav-item px-lg-2">
                                    <a class="nav-link py-2" data-toggle="tab" href="#home2"><span class="ion-ios-person mr-2"></span> Nuestra visión</a>
                                </li>
                            </ul>
                            <div class="tab-content bg-light rounded mt-2">
                                <div class="tab-pane container p-0 active" id="home1">
                                    <p>Proporcionar a todos los clientes la mejor calidad de limpieza para solicitudes residenciales y comerciales, ofreciendo el mejor servicio y cuidado. </p>
                                </div>
                                <div class="tab-pane container p-0 fade" id="home2">
                                    <p>Hacer que cada hogar, oficina y restaurante de Colombia tenga un servicio ofrecido por nosotros. </p>
                                </div>

                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                                <div class="block-18">
                                    <div class="icon"><span class="flaticon-doctor"></span></div>
                                    <div class="text">
                                        <strong class="number" data-number="99999">0</strong>
                                        <span>Clientes felices</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="ftco-section ftco-consult ftco-no-pt ftco-no-pb" style="background-image: url(Imagenes/portada22.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row justify-content-start">
                <div class="col-md-6 py-5 px-md-5">
                    <div class="py-md-5">
                        <div class="heading-section heading-section-white ftco-animate mb-5">
                            <h2 class="mb-4">Registrate</h2>
                            <p>Registrate para que tengas las posibilidad de ofrecer o solicitar tu servicio rápido, sencillo y eficaz</p>
                        </div>
                        <form action="Usuario" class="appointment-form ftco-animate" onsubmit="return validar()" method="post">
                            <div class="d-md-flex">
                                <div class="form-group">
                                    <label style="color: black">Nombre</label>
                                    <input type="text" class="form-control" name="textNombre" placeholder="Nombre" id="nombre" required>
                                </div>
                                <div class="form-group ml-md-4">
                                    <label style="color: black">Apellido</label>
                                    <input type="text" class="form-control" name="textApellido" placeholder="Apellido" id="apellido" required>
                                </div>
                            </div>
                            <div class="d-md-flex">
                                <div class="form-group">
                                    <label style="color: black">Identificacion</label>
                                    <input type="number" class="form-control" name="textIdentificacion" placeholder="Cedula" id="identificacion" required>
                                </div>
                                <div class="form-group ml-md-4">
                                    <label style="color: black">Fecha de nacimiento</label>
                                    <input type="text" class="form-control" name="textFecha" placeholder="Fecha Nacimiento" onfocus="(this.type = 'date')" id="nacimiento" required>
                                </div>
                            </div>
                            <div class="d-md-flex">
                                <div class="form-group">
                                    <label style="color: black">Ciudad</label>
                                    <select name="textCiudad" class="form-control" class="selectFormulario" id="ciudad" required>
                                        <option>Selecciona una ciudad</option>
                                        <option value="Barranquilla" style="color: black">Barranquilla</option>
                                        <option value="Bogotá" style="color: black">Bogotá</option>
                                        <option value="Cali" style="color: black">Cali</option>
                                        <option value="Cartagena" style="color: black">Cartagena</option>
                                        <option value="Cúcuta" style="color: black">Cúcuta</option>
                                        <option value="Medellín" style="color: black">Medellín</option>
                                    </select> 
                                </div>
                                <div class="form-group ml-md-4">
                                    <label style="color: black">Correo electronico</label>
                                    <input type="text" class="form-control" name="textCorreo" placeholder="Correo" id="correo" required>
                                </div>
                            </div>
                            <div class="d-md-flex">
                                <div class="form-group">
                                    <label style="color: black">Telefono</label>
                                    <input type="number" class="form-control" name="textTelefono" placeholder="Telefono" id="telefono" required>
                                </div>
                                <div class="form-group ml-md-4">
                                    <label style="color: black">Usuario</label>
                                    <input type="text" class="form-control" name="textUsuario" placeholder="Usuario" id="usuario" required>
                                </div>
                            </div>
                            <div class="d-md-flex">
                                <div class="form-group">
                                    <label style="color: black">Contraseña</label>
                                    <input type="password" class="form-control" name="textContrasena" placeholder="Contraseña" id="contrasena" required>
                                </div>
                                <div class="form-group ml-md-4">
                                    <div class="form-field">
                                        <label style="color: black">Rol</label>
                                        <select name="textRol" class="form-control" class="selectFormulario" id="rol" required>
                                            <option>Selecciona tú rol</option>
                                            <option value="1" style="color: black">Cliente</option>
                                            <option value="2" style="color: black">Empleado</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="d-md-flex">
                                <script>
                                    window.onload = function () {
                                    <% if (request.getAttribute("mensajeError") != null) {%>
                                        swal("Espera!", "<%=request.getAttribute("mensajeError")%>", "warning");
                                    <% }%>
                                    };
                                </script>
                                <div class="form-group">
                                    <button class="btn btn-white py-3 px-4  ">Registrar</button>
                                    <input type="hidden" value="1" name="tipoCorreo">
                                    <input type="hidden" value="7" name="opcion">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <footer class="ftco-footer ftco-bg-dark ftco-section">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-6 col-lg-3">
                    <div class="ftco-footer-widget mb-5">
                        <h2 class="ftco-heading-2">Tienes preguntas?</h2>
                        <div class="block-23 mb-3">
                            <ul>
                                <li><span class="icon icon-map-marker"></span><span class="text">Bogotá D.C, Colombia</span></li>
                                <li><a href="#"><span class="icon icon-phone"></span><span class="text">+57 300 615 9435</span></a></li>
                                <li><a href="#"><span class="icon icon-phone"></span><span class="text">+57 305 388 6400</span></a></li>
                                <li><a href="#"><span class="icon icon-phone"></span><span class="text">+57 321 903 8839</span></a></li>
                                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">lffranco27@misena.edu.co</span></a></li>
                                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">jdchaves@misena.edu.co</span></a></li>
                                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">jsgalvis@misena.edu.co</span></a></li>
                            </ul>
                        </div>
                        <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
                            <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                            <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                            <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-2">
                    <div class="ftco-footer-widget mb-5 ml-md-4">
                        <h2 class="ftco-heading-2">Principales ciudades</h2>
                        <ul class="list-unstyled">
                            <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Bogotá</a></li>
                            <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Medellín</a></li>
                            <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Cali</a></li>
                            <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Barranquilla</a></li>
                            <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Cartagena</a></li>
                            <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Cúcuta</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">

                    <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Esta obra de arte fue hecha por <i class="icon-heart" aria-hidden="true"></i><a href="https://colorlib.com" target="_blank"> Franchalvis </a><i class="icon-heart" aria-hidden="true"></i>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                </div>
            </div>
        </div>
    </footer>



    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-migrate-3.0.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/aos.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/scrollax.min.js"></script>
    <script src="js/lib/sweetalert/sweetalert.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    <script src="js/google-map.js"></script>
    <script src="js/main.js"></script>

</body>
</html>