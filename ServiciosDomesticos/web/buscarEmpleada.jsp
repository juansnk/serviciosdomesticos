<%-- 
    Document   : buscarEmpleada
    Created on : 26/08/2020, 12:51:30 PM
    Author     : Admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modeloDAO.UsuarioDAO"%>
<%@page import="modeloVO.UsuarioVO"%>
<%@include file="sesiones.jsp" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Webstrot Admin : Creative Admin Dashboard</title>
        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
        <!-- Standard iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
        <!-- Standard iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">
        <!-- Styles -->
        <link href="CSS/lib/chartist/chartist.min.css" rel="stylesheet">
        <link href="CSS/lib/font-awesome.min.css" rel="stylesheet">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link href="CSS/lib/owl.carousel.min.css" rel="stylesheet" />
        <link href="CSS/lib/owl.theme.default.min.css" rel="stylesheet" />
        <link href="CSS/lib/weather-icons.css" rel="stylesheet" />
        <link href="CSS/lib/menubar/sidebar.css" rel="stylesheet">
        <link href="CSS/lib/bootstrap.min.css" rel="stylesheet">
        <link href="CSS/lib/unix.css" rel="stylesheet">
        <link href="CSS/styleee.css" rel="stylesheet">
    </head>

    <body>

      <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <ul>
                        <li class="label">Principal</li>
                        <li class="active"><a href="indexCliente.jsp"><i class="ti-home"></i> Inicio  </a></li>
                        <li class="label">Citas</li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i>Listas <span class="badge badge-primary">3</span><span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="citasActivas.jsp">Activas</a></li>
                                <li><a href="citasInactivas.jsp">Terminadas</a></li>
                                <li><a href="citasCanceladas.jsp">Canceladas</a></li>   
                            </ul>
                        </li>

                        <li class="label">Empleados</li>
                        <li><a href="buscarEmpleada.jsp"><i class="ti-search"></i> Busca tu empleada </a></li>

                        <li class="label">Informacion</li>
                        <li><a href="calendarioCliente.jsp"><i class="ti-calendar"></i> Calendario </a></li>
                        <li><a href="perfilCliente.jsp"><i class="ti-user"></i> Perfil</a></li>

                        <li><a href="Sesiones"><i class="ti-close"></i> Cerrar sesion</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /# sidebar -->

        <div class="header">
            <div class="pull-left">
                <div class="logo"><a href="indexCliente.jsp"><span><img src="Imagenes/logoNegro.png" height="50" /></span></a></div><div class="hamburger sidebar-toggle">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>
            </div>
            <div class="pull-right p-r-15">
                <ul>           
                    <li class="header-icon dib"><img class="avatar-img" src="assets/images/avatar/1.jpg" alt="" /> <span class="user-avatar"> <%=usuario%> <i class="ti-angle-down f-s-10"></i></span>
                        <div class="drop-down dropdown-profile">
                            <div class="dropdown-content-body">
                                <ul>
                                    <li><a href="perfilCliente.jsp"><i class="ti-user"></i> <span>Perfil</span></a></li>
                                    <li><a href="calendarioCliente.jsp"><i class="ti-calendar"></i> <span>Calendario</span></a></li>
                                    <li><a href="modificarContraseñaCliente.jsp"><i class="ti-lock"></i> <span>Cambiar contraseña</span></a></li>
                                    <li><a href="Sesiones"><i class="ti-power-off"></i> <span>Cerrar sesión</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="content-wrap">
            <div class="main">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-8 p-r-0 title-margin-right">
                            <div class="page-header">
                                <div class="page-title">
                                    <h1>Hola, <span>bienvenido <%=nombre%></span></h1>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                        <div class="col-lg-4 p-l-0 title-margin-left">
                            <div class="page-header">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="indexCliente.jsp">Inicio</a></li>
                                        <li class="active">Buscar empleado</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-8 p-r-0 title-margin-right">
                            <div class="page-header">
                                <div class="page-title">
                                    <h1>Buscar</h1>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                        <div class="col-lg-4 p-l title-margin-left">
                            <div class="page-header">
                                <div class="page-title">
                                    <div class="breadcrumb text-right">
                                        <form method="post" action="Usuario">
                                            <select name="textCiudad">
                                                <option>Ciudad</option>
                                                <option value="Barranquilla">Barranquilla</option>
                                                <option value="Bogotá">Bogotá</option>
                                                <option value="Cali">Cali</option>
                                                <option value="Cartagena">Cartagena</option>
                                                <option value="Cúcuta">Cúcuta</option>
                                                <option value="Medellín">Medellín</option>
                                            </select>
                                            <select name="promedio">
                                                <option>Promedio</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                            <select name="precio">
                                                <option>Precio</option>
                                                <option>10000</option>
                                                <option>12000</option>
                                                <option>14000</option>
                                                <option>16000</option>
                                                <option>18000</option>
                                                <option>20000</option>
                                            </select>

                                            <button class="btn-success">Buscar</button>
                                            <input type="hidden" name="opcion" value="8">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>

                    <div class="main-content">
                        <div class="row">
                            <%
                                UsuarioVO usuVO = new UsuarioVO();
                                UsuarioDAO usuDAO = new UsuarioDAO();
                                ArrayList<UsuarioVO> listaEmp = usuDAO.listaEmpleados();
                                for (int i = 0; i < listaEmp.size(); i++) {
                                    usuVO = listaEmp.get(i);

                            %> 
                            <form method="post" action="Usuario" >
                                <div class="col-lg-3">
                                    <div class="card alert">
                                        <div class="products_1 text-center">
                                            <div class="pr_img_price">
                                                <div class="product_img">
                                                    <img src="ImagenControlador?opcionImg=3&idUsuario=<%=usuVO.getIdUsuario()%>" alt="" width="120" height="120" />
                                                    <input type="hidden" value="<%=usuVO.getIdUsuario()%>" name="textId">
                                                </div>
                                                <div class="product_price">
                                                    <div class="prd_prc_box">
                                                        <div class="product_price">
                                                            <p>$<%=usuVO.getEmpPrecio()%></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="product_details">
                                                <div class="product_name">
                                                    <h4><%=usuVO.getUsuNombre()%> <%=usuVO.getUsuApellido()%></h4>
                                                </div>
                                                <div class="product_des">
                                                    <p><%=usuVO.getUsuCiudad()%></p>
                                                </div>
                                                <div class="product_des">
                                                    <p><%=usuVO.getEmpPromedio()%></p>
                                                </div>
                                                <div class="prdt_add_to_cart">
                                                    <button class="btn btn-primary btn-rounded  m-l-5">Ver</button>
                                                    <input type="hidden" name="opcion" value="5">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- /# card -->
                                </div> 
                            </form>
                            <%}%>
                            <!-- /# column -->
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="footer">
                                    <p>ServiHogar <span id="date-time"></span></p>
                                </div>
                            </div>
                        </div>
                        <!-- /# row -->
                    </div>
                </div>
                <!-- /# container-fluid -->
            </div>
            <!-- /# main -->
        </div>
        <!-- /# content wrap -->

        <div id="search">
            <button type="button" class="close">×</button>
            <form>
                <input type="search" value="" placeholder="type keyword(s) here" />
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
        <!-- jquery vendor -->
        <script src="js/lib/jquery.min.js"></script>
        <script src="js/lib/jquery.nanoscroller.min.js"></script>
        <!-- nano scroller -->
        <script src="js/lib/menubar/sidebar.js"></script>
        <script src="js/lib/preloader/pace.min.js"></script>
        <!-- sidebar -->
        <script src="js/lib/bootstrap.min.js"></script>
        <!-- bootstrap -->
        <script src="js/lib/mmc-common.js"></script>
        <script src="js/lib/mmc-chat.js"></script>
        <script src="js/lib/nestable/jquery.nestable.js"></script>
        <!-- scripit init-->
        <script src="js/lib/nestable/nestable.init.js"></script>
        <!-- scripit init-->
        <script src="js/scripts.js"></script>
        <!-- scripit init-->

    </body>

</html>