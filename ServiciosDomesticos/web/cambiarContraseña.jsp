<%-- 
    Document   : cambiarContraseña
    Created on : 11/11/2020, 12:46:03 PM
    Author     : Admin
--%>

<%@page import="modeloVO.UsuarioVO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>ServiHogar</title>
        <link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <link href="CSS/lib/themify-icons.css" rel="stylesheet">
        <link rel="stylesheet" href="CSS/login.css">
        <link href="CSS/lib/sweetalert/sweetalert.css" rel="stylesheet">

    </head>
    <body>

        <main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
            <div class="container">
                <div class="card login-card" style="text-align: center">
                    <div class="col-xs-12">
                        <div class="card-body" >
                            <div class="brand-wrapper">
                                <img src="Imagenes/servihogar.png" alt="logo" class="logo" style="margin-top: -80px">
                            </div>
                            <p class="login-card-description">RESTABLECER TU CONTRASEÑA</p>
                            <form action="Usuario" method="post" style="margin-left: auto;margin-right: auto" onsubmit="return validar();" >                 
                                <div class="form-group">
                                    <% UsuarioVO usuVO = (UsuarioVO) request.getAttribute("Contraseña");
                                        if (usuVO != null) {
                                    %>
                                    <input type="hidden" name="textId" value="<%=usuVO.getIdUsuario()%>">
                                    <%}%>
                                    <label for="email" >Nueva contraseña</label>
                                    <input type="password" name="textContrasena" id="contrasena1" class="form-control" placeholder="***********">
                                </div>
                                <div class="form-group mb-4">
                                    <label for="password">Confirmar contraseña</label>
                                    <input type="password" id="contrasena2" class="form-control" placeholder="***********">
                                </div>
                                <button name="login" id="login" class="btn btn-block login-btn mb-4">Restablecer</button>
                                <input type="hidden" value="12" name="opcion">
                            </form>
                        </div>
                    </div>
                </div>
 <script src="js/validaciones/contraseña.js" type="text/javascript"></script>
 <script>
     window.onload = function() {
  var myInput = document.getElementById('contrasena1');
  var myInput1 = document.getElementById('contrasena2');
  myInput.onpaste = function(e) {
    e.preventDefault();
    swal("Esta acción está prohibida");
  };
  
  myInput.oncopy = function(e) {
    e.preventDefault();
    swal("Esta acción está prohibida");
  };
  myInput1.onpaste = function(e) {
    e.preventDefault();
    swal("Esta acción está prohibida");
  };
  
  myInput1.oncopy = function(e) {
    e.preventDefault();
    swal("Esta acción está prohibida");
  };
};
 </script>
            </div>
        </main>
       
        <script src="js/lib/sweetalert/sweetalert.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    </body>
</html>
