/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modeloDAO.EmpleadoDAO;
import modeloVO.EmpleadoVO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "EmpleadoControlador", urlPatterns = {"/Empleado"})
public class EmpleadoControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        
        int opcion = Integer.parseInt(request.getParameter("opcion"));
        String idEmpleado = request.getParameter("textId");
        String empPromedio = request.getParameter("textPromedio");
        String empExperiencia = request.getParameter("textExperiencia");
        String empDescripcion = request.getParameter("textDescripcion");
        String usuario_idUsuario = request.getParameter("textUsuario_idUsuario");
        
        EmpleadoVO empVO = new EmpleadoVO(idEmpleado, empPromedio, empExperiencia, empDescripcion, usuario_idUsuario);
        EmpleadoDAO empDAO = new EmpleadoDAO(empVO);
        
        switch(opcion){
              
            case 1://agregar registro
                if (empDAO.agregarRegistro()){                   
                    request.getRequestDispatcher("registrarAgenda.jsp").forward(request, response);
                }else{
                    request.setAttribute("mensajeEror", "El usuario NO se Registro correctamente");
                }            
             break;
             
             case 2://consultar registro
                if (empDAO.consultarRegistro(idEmpleado)){
                    request.setAttribute("mensajeExitoso", "El usuario se actulizo correctamente");
                }else{
                    request.setAttribute("mensajeEror", "El usuario NO se actualizo correctamente");
                }
             request.getRequestDispatcher("consultarAgenda.jsp").forward(request, response);
             break;
             
             
             
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
