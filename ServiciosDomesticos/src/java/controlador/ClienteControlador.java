/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modeloDAO.AgendaDAO;
import modeloDAO.ClienteDAO;
import modeloVO.AgendaVO;
import modeloVO.ClienteVO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "ClienteControlador", urlPatterns = {"/Cliente"})
public class ClienteControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        int opcion = Integer.parseInt(request.getParameter("opcion"));
        String idCliente = request.getParameter("textId");
        String cliDireccion = request.getParameter("textDireccion");
        String cliLocalidad = request.getParameter("textLocalidad");
        String cliBarrio = request.getParameter("textBarrio");
        String usuario_idUsuario = request.getParameter("textUsuario_idUsuario");
        
        ClienteVO cliVO = new ClienteVO(idCliente, cliDireccion, cliLocalidad, cliBarrio, usuario_idUsuario);
        ClienteDAO cliDAO = new ClienteDAO(cliVO);
        
        switch(opcion){
              
            case 1://agregar registro
                if (cliDAO.agregarRegistro()){
                    request.setAttribute("mensajeExitoso", "El usuario se Registro correctamente");
                     request.getRequestDispatcher("index.jsp").forward(request, response);
                }else{
                    request.setAttribute("mensajeEror", "El usuario NO se Registro correctamente");
                    request.getRequestDispatcher("bienvenidaClien.jsp").forward(request, response);
                }
             break;
             
             case 2://consultar registro
                if (ageDAO.consultarRegistro(idAgenda)){
                    request.setAttribute("mensajeExitoso", "El usuario se actulizo correctamente");
                }else{
                    request.setAttribute("mensajeEror", "El usuario NO se actualizo correctamente");
                }
             request.getRequestDispatcher("consultarAgenda.jsp").forward(request, response);
             break;
             
             
             
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
