/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Correos.ConfirmacionCuenta;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modeloDAO.CitaDAO;
import modeloDAO.CitasCanceladasDAO;
import modeloDAO.UsuarioDAO;
import modeloVO.CitaVO;
import modeloVO.CitasCanceladasVO;
import modeloVO.UsuarioVO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "CitaControlador", urlPatterns = {"/Cita"})
public class CitaControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        int opcion = Integer.parseInt(request.getParameter("opcion"));
        String idCita = request.getParameter("textId");
        String citFecha = request.getParameter("textFecha");
        String citDireccion = request.getParameter("textDireccion");
        String citHoraInicio = request.getParameter("textHoraInicio");
        String citHoraFin = request.getParameter("textHoraFin");
        String citCalificacion = request.getParameter("textCalificacion");
        String citOpinion = request.getParameter("textOpinion");
        String citEstado = request.getParameter("textEstado");
        String cliente_idCliente = request.getParameter("textIdCliente");
        String agenda_idProgramacion = request.getParameter("textIdProgramacion");
        String agenda_idAgenda = request.getParameter("textIdAgenda");
        String descripcion = request.getParameter("textDescripcion");

        CitaVO citVO = new CitaVO(idCita, citFecha, citDireccion, citHoraInicio, citHoraFin, citCalificacion, citOpinion, citEstado, cliente_idCliente, agenda_idProgramacion, agenda_idAgenda);
        CitaDAO citDAO = new CitaDAO(citVO);

        CitasCanceladasVO cicVO = new CitasCanceladasVO(idCita, descripcion);
        CitasCanceladasDAO cicDAO = new CitasCanceladasDAO(cicVO);

        switch (opcion) {
            case 1://agregar registro
                if (citDAO.agregarRegistro()) {
                    CitaVO CitaVO;
                    CitaVO = CitaDAO.consultarIdCita(citFecha, citDireccion, citHoraInicio, cliente_idCliente);
                    request.setAttribute("idCita", CitaVO);
                    request.getRequestDispatcher("solicitarServicio.jsp").forward(request, response);
                } else {
                    String idUsuario = request.getParameter("textIdUsuario");
                    UsuarioVO UsuarioVO;
                    UsuarioVO = UsuarioDAO.consultarPorId(idUsuario);
                    if (UsuarioVO != null) {          
                        request.setAttribute("Empleados", UsuarioVO);
                    request.setAttribute("mensajeError", "Tu cita no se registro exitosamente, revisa la disponibilidad de tu empleada");
                    request.getRequestDispatcher("infoEmpleados.jsp").forward(request, response);
                    }
                }              
                break;
            case 2://Actulizar registro
                if (citDAO.actualizarRegistro()) {
                    request.getRequestDispatcher("citasActivas.jsp").forward(request, response);
                }else{
                    CitaVO CitaVO;
                    CitaVO = CitaDAO.consultarCitaId(idCita);
                     request.setAttribute("idCita", CitaVO);        
                    request.setAttribute("mensajeError", "Tu cita no se modifico exitosamente, revisa la disponibilidad de tu empleada");
                     request.getRequestDispatcher("modificarCita.jsp").forward(request, response);
                }
                break;

            case 3://Cancelar cita desde el rol cliente
                switch (citEstado) {
                    case "1":
                        if (citDAO.borrarRegistro()) {
                            CitasCanceladasVO CitasCanceladasVO;
                            CitasCanceladasVO = CitasCanceladasDAO.consultarIdCitaC(idCita);
                            request.setAttribute("idCitaC", CitasCanceladasVO);
                            request.getRequestDispatcher("registrarDescripcion.jsp").forward(request, response);
                        }
                        break;
                    case "2":
                        if (citDAO.borrarRegistro()) {
                            request.getRequestDispatcher("citasInactivas.jsp").forward(request, response);
                        }
                        break;
                }

                break;
            case 4://Consultar por placa
                if (cicDAO.registroDescripcion()) {
                    request.setAttribute("mensajeExito", "El vehiculo se borro correctamente");
                } else {
                    request.setAttribute("mensajeError", "El vehiculo no se borro correctamente");
                }
                request.getRequestDispatcher("citasCanceladas.jsp").forward(request, response);
                break;
            case 5://Consultar cita del cliente por index empleada notificaciones
                CitaVO CitaVO;
                CitaVO = CitaDAO.consultarCitaEmpleada(idCita);
                request.setAttribute("informacionCita", CitaVO);
                request.getRequestDispatcher("infoCitaEmpleada.jsp").forward(request, response);
                break;
            case 6://Consultar cita por id cliente
                switch (citEstado) {
                    case "1":
                        CitaVO = CitaDAO.consultarCitaId(idCita);
                        request.setAttribute("idCita", CitaVO);
                        request.getRequestDispatcher("infoCita.jsp").forward(request, response);
                        break;
                    case "2":
                        CitaVO = CitaDAO.consultarCitaId(idCita);
                        request.setAttribute("idCita", CitaVO);
                        CitaVO CitaaVO = new CitaVO();
                        CitaaVO = CitaDAO.consultarCalificacionCita(idCita);
                        request.setAttribute("idCitaCalificacion", CitaaVO);
                        request.getRequestDispatcher("infoCitaInactiva.jsp").forward(request, response);
                        break;
                    case "3":
                        CitaVO = CitaDAO.consultarCitaId(idCita);
                        request.setAttribute("idCita", CitaVO);
                        request.getRequestDispatcher("infoCitaCancelada.jsp").forward(request, response);
                        break;
                }
            case 7://Consultar cita por id para modificar
                CitaVO = CitaDAO.consultarCitaId(idCita);
                request.setAttribute("idCita", CitaVO);
                request.getRequestDispatcher("modificarCita.jsp").forward(request, response);
                break;
            case 8://Datos para calificacion del servicio
                if (citDAO.calificacionYopinion()) {
                    request.getRequestDispatcher("citasInactivas.jsp").forward(request, response);
                }
                break;
            case 9://Cancelar cita desde el rol empleado
                if (citDAO.cancelarCitaEmpleado()) {
                    int tipoCorreo = Integer.parseInt(request.getParameter("tipoCorreo"));
                     String usuCorreo = request.getParameter("correo");
                    ConfirmacionCuenta confi = new ConfirmacionCuenta();
                    confi.mandarCorreo(usuCorreo, tipoCorreo, null);
                    CitasCanceladasVO CitasCanceladasVO;
                    CitasCanceladasVO = CitasCanceladasDAO.consultarIdCitaC(idCita);
                    request.setAttribute("idCitaCancelar", CitasCanceladasVO);
                    request.getRequestDispatcher("registrarDescripcionE.jsp").forward(request, response);
                }
                break;
            case 10://Aceptar cita empleada
                if (citDAO.aceptarCitaEmpleado()) {
                    request.getRequestDispatcher("indexEmpleado.jsp").forward(request, response);
                }
                break;
            case 11://Aceptar o cancelar cita desde la factura
                if (citEstado != null) {
                    if (citDAO.aceptarFactura()) {
                        int tipoCorreo = Integer.parseInt(request.getParameter("tipoCorreo"));
                        String usuCorreo = request.getParameter("correo");
                        ConfirmacionCuenta confi= new ConfirmacionCuenta();
                        confi.mandarCorreo(usuCorreo, tipoCorreo, null);
                        request.getRequestDispatcher("finalizarFactura.jsp").forward(request, response);
                    }
                } else {
                    if (citDAO.cancelarFactura()) {
                        CitasCanceladasVO CitasCanceladasVO;
                        CitasCanceladasVO = CitasCanceladasDAO.consultarIdCitaC(idCita);
                        request.setAttribute("idCitaC", CitasCanceladasVO);
                        request.getRequestDispatcher("registrarDescripcion.jsp").forward(request, response);
                    }
                }
                break;
            case 12://Consultar cita por id Empleado
                switch (citEstado) {
                    case "1":
                        CitaVO = CitaDAO.consultarCitaEmpleado(idCita);
                        request.setAttribute("infoCitaEmpleado", CitaVO);
                        request.getRequestDispatcher("infoCitaActivaEmpleado.jsp").forward(request, response);
                        break;
                    case "2":
                        CitaVO = CitaDAO.consultarCitaEmpleado(idCita);
                        request.setAttribute("idCita", CitaVO);
                        CitaVO CitaaVO = new CitaVO();
                        CitaaVO = CitaDAO.consultarCalificacionCita(idCita);
                        request.setAttribute("idCitaCalificacion", CitaaVO);
                        request.getRequestDispatcher("infoCitaInactivaEmpleado.jsp").forward(request, response);
                        break;
                }
                break;
            case 13://borrar cita terminada empleado
                if (citDAO.borrarRegistro()) {
                    request.getRequestDispatcher("citasInactivasEmpleado.jsp").forward(request, response);
                }
                break;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
