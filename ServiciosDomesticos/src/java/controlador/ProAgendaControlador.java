/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modeloDAO.AgendaDAO;
import modeloDAO.ProAgendaDAO;
import modeloVO.AgendaVO;
import modeloVO.ProAgendaVO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "ProAgendaControlador", urlPatterns = {"/ProAgenda"})
public class ProAgendaControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        int opcion = Integer.parseInt(request.getParameter("opcion"));
        String idProgramacionAgenda = request.getParameter("textId");
        String proDia = request.getParameter("textDia");
        String proHoraInicio = request.getParameter("textHoraInicio");
        String proHoraFin = request.getParameter("textHoraFin");
        String agenda_idAgenda = request.getParameter("textIdAgenda");
        String empleado_idEmpleado = request.getParameter("textIdEmpleado");
        
        ProAgendaVO proVO = new ProAgendaVO(idProgramacionAgenda, proDia, proHoraInicio, proHoraFin, agenda_idAgenda, agenda_idAgenda, empleado_idEmpleado);
        ProAgendaDAO proDAO = new ProAgendaDAO(proVO);
        
        switch(opcion){
              
            case 1://agregar registro
                if (proDAO.agregarRegistro()){
                     AgendaVO AgendaVO;
                AgendaVO = AgendaDAO.consultarIdAgenda(empleado_idEmpleado);
                    request.setAttribute("proAgenda", AgendaVO);
                    request.setAttribute("mensajeExitoso", "Tu diosponibilidad se Registro correctamente");
                }else{
                    request.setAttribute("mensajeEror", "Tu disponibilidad  NO se Registro correctamente");
                }
             request.getRequestDispatcher("registrarProAgenda.jsp").forward(request, response);
             break;
                case 2://modificar registro
                if (proDAO.actualizarRegistro()){
                    request.setAttribute("mensajeExitoso", "Tu diosponibilidad se Registro correctamente");
                }else{
                    request.setAttribute("mensajeEror", "Tu disponibilidad  NO se Registro correctamente");
                }
             request.getRequestDispatcher("listaAgenda.jsp").forward(request, response);
             break;
                case 3:
                    AgendaVO AgendaVO;
                AgendaVO = AgendaDAO.consultarIdAgenda(empleado_idEmpleado);
                    request.setAttribute("proAgenda", AgendaVO);
                ProAgendaVO progVO =new ProAgendaVO(agenda_idAgenda);
              request.setAttribute("horasDisponibilidad",progVO);
             request.getRequestDispatcher("registrarProAgenda.jsp").forward(request, response);
             break;
                    case 4://Consultar por id
                   ProAgendaVO  ProAgendaVO;
                   ProAgendaVO = ProAgendaDAO.consultarPro(idProgramacionAgenda);
                if (ProAgendaVO != null) {
                    request.setAttribute("ProAgendaa", ProAgendaVO);
                    request.getRequestDispatcher("modificarAgenda.jsp").forward(request, response);
                    
                }
             
             
             
             
             
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
