/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modeloDAO.CitaDAO;
import modeloDAO.EmpleadoDAO;
import modeloDAO.UsuarioDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "ImagenControlador", urlPatterns = {"/ImagenControlador"})
public class ImagenControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    CitaDAO citDAO = new CitaDAO();
    UsuarioDAO usuDAO = new UsuarioDAO();
    EmpleadoDAO empDAO = new EmpleadoDAO();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ImagenControlador</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ImagenControlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int opcionIMG = Integer.parseInt(request.getParameter("opcionImg"));
        String cliente_idCliente = request.getParameter("idCliente");
        String idCita = request.getParameter("idCita");
        String idUsuario = request.getParameter("idUsuario");
        String idEmpleado = request.getParameter("idEmpleado");
        switch (opcionIMG) {
            case 1://listar imagenes de citas activas del cliente
                citDAO.listarImg(cliente_idCliente, response);
                break;
            case 2://imagenes de cada empleada al consultar la cita
                citDAO.consultarCitaImg(idCita, response);
                break;
            case 3://listar imagenes al buscar empleada
                usuDAO.listarEmpleadosImg(idUsuario,response);
                break;
            case 4://imagen de perfil
                usuDAO.imgUsuario(idUsuario, response);
                break;
            case 5://imagen de citas activas de empleada
                citDAO.consultarCitaEmpleadaImg(idCita, response);
                break;
            case 6://listar imagenes de opiniones de clientes en indexEmpleado
                empDAO.listarOpinionesImg(response);
                break;
            case 7://imagen de empleada informacion
                usuDAO.consultarPorIdImg(idUsuario, response);
                break;
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
