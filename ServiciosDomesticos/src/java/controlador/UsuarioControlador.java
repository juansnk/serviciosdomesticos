/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Correos.ConfirmacionCuenta;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import modeloDAO.AgendaDAO;
import modeloDAO.UsuarioDAO;
import modeloVO.AgendaVO;
import modeloVO.UsuarioVO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "UsuarioControlador", urlPatterns = {"/Usuario"})
@MultipartConfig
public class UsuarioControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        //01.Declarar todos lo elementos para todas las vistas 
        int opcion = Integer.parseInt(request.getParameter("opcion"));
        String idUsuario = request.getParameter("textId");
        String usuNombre = request.getParameter("textNombre");
        String usuApellido = request.getParameter("textApellido");
        String usuIdentificacion = request.getParameter("textIdentificacion");
        String usuFecha = request.getParameter("textFecha");
        String usuCiudad = request.getParameter("textCiudad");
        String usuCorreo = request.getParameter("textCorreo");
        String usuTelefono = request.getParameter("textTelefono");
        String usuEstado = request.getParameter("textEstado");
        String usuario = request.getParameter("textUsuario");
        String contrasena = request.getParameter("textContrasena");
        String rol_idRol = request.getParameter("textRol");
        //02.Mandar datos al VO

        UsuarioVO usuVO = new UsuarioVO(idUsuario, usuNombre, usuApellido, usuIdentificacion, usuFecha, usuCiudad, usuCorreo, usuTelefono, usuEstado, usuario, contrasena, rol_idRol);

        //03.Del VO los manda al DAO para que realize las instrucciones
        UsuarioDAO usuDAO = new UsuarioDAO(usuVO);
        ConfirmacionCuenta confi = new ConfirmacionCuenta();

        switch (opcion) {
            case 1://Agregar registro y redireccionar a confirmacion de registro
                if (usuDAO.agregarRegistro()) {
                    HttpSession miSesion = request.getSession(true);
                    int tipoCorreo = Integer.parseInt(request.getParameter("tipoCorreo"));
                    switch (rol_idRol) {
                        case "1":
                            usuVO = UsuarioDAO.consultarIdCliente(usuario);
                            miSesion.setAttribute("datosUsuario", usuVO);
                            confi.mandarCorreo(usuCorreo, tipoCorreo, idUsuario);
                            request.getRequestDispatcher("esperarConfirmacion.jsp").forward(request, response);
                            break;
                        case "2":
                            usuVO = UsuarioDAO.consultarIdEmpleado(usuario);
                            miSesion.setAttribute("datosUsuario", usuVO);
                            confi.mandarCorreo(usuCorreo, tipoCorreo, idUsuario);
                            request.getRequestDispatcher("esperarConfirmacion.jsp").forward(request, response);
                            break;
                    }
                } else {
                    request.setAttribute("mensajeError", "El usuario o correo ya estan en uso");
                    request.getRequestDispatcher("index.jsp").forward(request, response);
                }
                break;

            case 2://Actulizar registro
                switch (rol_idRol) {
                    case "1":
                        if (usuDAO.actualizarRegistro()) {
                            request.setAttribute("mensajeExito", "vuelve a iniciar sesión para ver reflejado los cambios");
                            request.getRequestDispatcher("perfilCliente.jsp").forward(request, response);
                        }
                        break;
                    case "2":
                        if (usuDAO.actualizarRegistro()) {
                            request.setAttribute("mensajeExito", "vuelve a iniciar sesión para ver reflejado los cambios");
                            request.getRequestDispatcher("perfilEmpleado.jsp").forward(request, response);
                        }
                        break;
                }

                break;

            case 3://borrar registro
                if (usuDAO.borrarRegistro()) {
                    request.setAttribute("mensajeExito", "El usuario se borro correctamente");
                } else {
                    request.setAttribute("mensajeError", "El usuario no se borro correctamente");
                }
                request.getRequestDispatcher("borrarUsuario.jsp").forward(request, response);
                break;

            case 4://inicio de sesion
                if (usuDAO.iniciarSesion(usuario, contrasena)) {
                    UsuarioVO UsuarioVO;
                    UsuarioVO = UsuarioDAO.consultarRol(usuario, contrasena);
                    String rol = UsuarioVO.getRol_idRol();
                    HttpSession miSesion = request.getSession(true);
                    switch (rol) {
                        case "1":
                            usuVO = UsuarioDAO.consultarIdCliente(usuario);
                            miSesion.setAttribute("datosUsuario", usuVO);
                            String idCliente = usuVO.getIdCliente();
                            if(usuDAO.citasEnProcesoAviso(idCliente)){
                                request.getRequestDispatcher("indexCliente.jsp").forward(request, response);
                            }else{
                                request.setAttribute("mensajeError", "Finaliza tu cita antes de acabar el dia");
                                request.getRequestDispatcher("indexCliente.jsp").forward(request, response);
                            }
                            break;
                        case "2":
                            usuVO = UsuarioDAO.consultarEmpleadoAge(usuario);
                            String idAgenda = usuVO.getIdAgenda();
                            AgendaVO ageVO = new AgendaVO(idAgenda);
                            AgendaDAO ageDAO = new AgendaDAO(ageVO);
                            if (idAgenda != null) {
                                if (ageDAO.estadoEmpAge()) {
                                    request.setAttribute("mensajeError", "Debes actulizar tu agenda o no podrás recibir citas");
                                }
                            }
                            miSesion.setAttribute("datosUsuario", usuVO);
                            request.getRequestDispatcher("indexEmpleado.jsp").forward(request, response);
                            break;
                    }
                } else {
                    request.setAttribute("mensajeError", "El usuario y/o la contraseña son incorrectos");
                    request.getRequestDispatcher("login.jsp").forward(request, response);
                }
                break;
            case 5://Consultar por datos empleado por id 

                usuVO = UsuarioDAO.consultarPorId(idUsuario);
                if (usuVO != null) {
                    request.setAttribute("Empleados", usuVO);
                    request.getRequestDispatcher("infoEmpleados.jsp").forward(request, response);
                }
                break;
            case 6://datos empleado para registrar cita

                usuVO = UsuarioDAO.consultarPorId(idUsuario);
                if (usuVO != null) {
                    request.setAttribute("Empleados", usuVO);
                    request.getRequestDispatcher("solicitarCita.jsp").forward(request, response);
                }
                break;
            case 7://Agregar registro acerca de
                if (usuDAO.agregarRegistro()) {
                    HttpSession miSesion = request.getSession(true);
                    int tipoCorreo = Integer.parseInt(request.getParameter("tipoCorreo"));
                    switch (rol_idRol) {
                        case "1":
                            usuVO = UsuarioDAO.consultarIdCliente(usuario);
                            miSesion.setAttribute("datosUsuario", usuVO);
                            confi.mandarCorreo(usuCorreo, tipoCorreo, idUsuario);
                            request.getRequestDispatcher("esperarConfirmacion.jsp").forward(request, response);
                            break;
                        case "2":
                            usuVO = UsuarioDAO.consultarIdEmpleado(usuario);
                            miSesion.setAttribute("datosUsuario", usuVO);
                            confi.mandarCorreo(usuCorreo, tipoCorreo, idUsuario);
                            request.getRequestDispatcher("esperandoConfirmacion.jsp").forward(request, response);
                            break;
                    }
                } else {
                    request.setAttribute("mensajeError", "El usuario o correo ya estan en uso");
                }
                request.getRequestDispatcher("acerca.jsp").forward(request, response);
                break;
            case 8://listar empleados por busqueda avanzada
                String empPrecio = request.getParameter("precio");
                String empPromedio = request.getParameter("promedio");
                ArrayList<UsuarioVO> listaEmp = usuDAO.listaEmpleadosBuscar(empPrecio, usuCiudad, empPromedio);
                request.setAttribute("listaBuscar", listaEmp);
                request.getRequestDispatcher("buscarEmpleadaDatos.jsp").forward(request, response);
                break;
            case 9://registrar foto de perfil
                if (idUsuario != null) {
                    Part part = request.getPart("textFoto");
                    InputStream usuFoto = part.getInputStream();
                    if (usuDAO.fotoPerfil(usuFoto)) {
                        switch (rol_idRol) {
                            case "1":
                                request.getRequestDispatcher("bienvenidaClien.jsp").forward(request, response);
                                break;
                            case "2":
                                request.getRequestDispatcher("bienvenidaEmple.jsp").forward(request, response);
                                break;
                        }
                    }
                }
                break;
            case 10://Redireccionar boton de confirmar registro desde el correo                
                request.getRequestDispatcher("fotoPerfil.jsp").forward(request, response);
                break;
            case 11://correo para recuperar contraseña y mandar id
                if (usuDAO.correoContraseña(usuCorreo)) {
                    usuVO = UsuarioDAO.ConsultarIdUsuario(usuCorreo);
                    String idUsuarioo = usuVO.getIdUsuario();
                    int tipoCorreo = Integer.parseInt(request.getParameter("tipoCorreo"));
                    confi.mandarCorreo(usuCorreo, tipoCorreo, idUsuarioo);
                    request.setAttribute("mensajeExito", "Revisa tu correo, te llegara un mensaje");
                } else {
                    request.setAttribute("mensajeError", "El correo no esta registrado en el sistema, vuelve a intentarlo");
                }
                request.getRequestDispatcher("contraseñaCorreo.jsp").forward(request, response);
                break;
            case 12://cambiar contraseña olvidada
                if (usuDAO.cambairContraseña()) {
                    request.getRequestDispatcher("login.jsp").forward(request, response);
                }
                break;
            case 13://redirecionar a cambiar contraseña desde el correo
                UsuarioVO usuVO1 = new UsuarioVO(idUsuario, usuCorreo);
                request.setAttribute("Contraseña", usuVO1);
                request.getRequestDispatcher("cambiarContraseña.jsp").forward(request, response);
                break;
            case 14://cambiar contraseña de usuario en el sistema
                String contrasenaNueva = request.getParameter("contrasenaNueva");
                switch (rol_idRol) {
                    case "1":
                        if (usuDAO.cambiarContraseñaSistema(contrasenaNueva)) {
                            request.setAttribute("mensajeExito", "Tu contraseña fue modificada exitosamente");
                            request.getRequestDispatcher("perfilCliente.jsp").forward(request, response);
                        } else {
                            request.setAttribute("mensajeError", "Tu contraseña actual no concuerda");
                            request.getRequestDispatcher("modificarContraseñaCliente.jsp").forward(request, response);
                        }
                        break;
                    case "2":
                        if (usuDAO.cambiarContraseñaSistema(contrasenaNueva)) {
                            request.setAttribute("mensajeExito", "Tu contraseña fue modificada exitosamente");
                            request.getRequestDispatcher("perfilEmpleado.jsp").forward(request, response);
                        } else {
                            request.setAttribute("mensajeError", "Tu contraseña actual no concuerda");
                            request.getRequestDispatcher("modificarContraseñaEmpleado.jsp").forward(request, response);
                        }
                        break;
                }
                break;
            case 15://registrar foto de perfil
                if (idUsuario != null) {
                    Part part = request.getPart("textFoto");
                    InputStream usuFoto = part.getInputStream();
                    if (usuDAO.fotoPerfil(usuFoto)) {
                        switch (rol_idRol) {
                            case "1":
                                request.getRequestDispatcher("perfilCliente.jsp").forward(request, response);
                                break;
                            case "2":
                                request.getRequestDispatcher("perfilEmpleado.jsp").forward(request, response);
                                break;
                        }
                    }
                }
                break;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
