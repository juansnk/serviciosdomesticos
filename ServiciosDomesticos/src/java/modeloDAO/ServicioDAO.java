/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modeloVO.ServicioVO;
import util.Conexion;

/**
 *
 * @author Admin
 */
public class ServicioDAO extends Conexion{
    //1.Declarar objetos y variables

    private Connection conexion = null;
    private PreparedStatement puente = null;
    private ResultSet mensajero = null;

    //1.1.Declarar variable para cualquier metodo
    public boolean operaciones = false;
    public String sql;

    //1.2.Declarar las variables del VO
    public String idServicio = "", serDescripcion="",categoria_idCategoria="";

    public ServicioDAO() {
    }
    
    //2.
    public ServicioDAO(ServicioVO serVO) {
        super();

        try {
            conexion = this.obtenerConexion();
            idServicio= serVO.getIdServicio();
            serDescripcion = serVO.getSerDescripcion();
            categoria_idCategoria = serVO.getCategoria_idCategoria();

        } catch (Exception e) {
            Logger.getLogger(ServicioDAO.class.getName()).log(Level.SEVERE, null, e);
        }
    
    }
     public ArrayList<ServicioVO> listarServicios(String categoria_idCategoria) {

        ServicioVO serVO = null;
        conexion = this.obtenerConexion();
        ArrayList<ServicioVO> listar = new ArrayList<>();
        try {
            sql = "SELECT * FROM servicios WHERE categoria_idCategoria=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, categoria_idCategoria);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                serVO = new ServicioVO(mensajero.getString(1),
                        mensajero.getString(2),mensajero.getString(3),
                        categoria_idCategoria);
                listar.add(serVO);

            }
        } catch (Exception e) {
            Logger.getLogger(ServicioDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listar;
    }

}

