/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modeloVO.CategoriaVO;
import util.Conexion;


/**
 *
 * @author Admin
 */
public class CategoriaDAO extends Conexion {

  //1.Declarar objetos y variables

    private Connection conexion = null;
    private PreparedStatement puente = null;
    private ResultSet mensajero = null;

    //1.1.Declarar variable para cualquier metodo
    public boolean operaciones = false;
    public String sql;

    //1.2.Declarar las variables del VO
    public String idCategoria = "", catNombre = "";

    public CategoriaDAO() {
    }
    
    //2.
    public CategoriaDAO(CategoriaVO catVO) {
        super();

        try {
            conexion = this.obtenerConexion();
            idCategoria= catVO.getIdCategoria();
            catNombre = catVO.getCatNombre();

        } catch (Exception e) {
            Logger.getLogger(CategoriaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
    
    }
     public ArrayList<CategoriaVO> listarCategorias() {

        CategoriaVO catVO = null;
        conexion = this.obtenerConexion();
        ArrayList<CategoriaVO> lista = new ArrayList<>();
        try {
            sql = "SELECT * FROM categorias";
            puente = conexion.prepareStatement(sql);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                catVO = new CategoriaVO(mensajero.getString(1),
                        mensajero.getString(2));
                lista.add(catVO);

            }
        } catch (Exception e) {
            Logger.getLogger(CategoriaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return lista;
    }
}
