/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modeloVO.ClienteVO;
import modeloVO.EmpleadoVO;
import util.Conexion;
import util.Crud;

/**
 *
 * @author Admin
 */
public class ClienteDAO extends Conexion implements Crud {
       //1.Declarar objetos y variables

    private Connection conexion = null;
    private PreparedStatement puente = null;
    private ResultSet mensajero = null;

    //1.1.Declarar variable para cualquier metodo
    public boolean operaciones = false;
    public String sql;

    //1.2.Declarar las variables del VO
    public String idCliente="", cliDireccion= "", cliLocalidad="", cliBarrio="",
            usuario_idUsuario="";

    //2.
    public ClienteDAO(ClienteVO cliVO) {
        super();

        try {
            conexion = this.obtenerConexion();
            idCliente = cliVO.getIdCliente();
            cliDireccion = cliVO.getCliDireccion();
            cliLocalidad = cliVO.getCliLocalidad();
            cliBarrio = cliVO.getCliBarrio();
            usuario_idUsuario = cliVO.getUsuario_idUsuario();

        } catch (Exception e) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public boolean agregarRegistro() {
        try {
            sql="CALL PAinserDaCliente (?,?,?,?)";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idCliente);
            puente.setString(2, cliDireccion);
            puente.setString(3, cliLocalidad);
            puente.setString(4, cliBarrio);
            puente.executeUpdate();
            operaciones = true;
        } catch (SQLException e) { 
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, e);
        }finally{
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }                 
      return operaciones;
    }

    @Override
    public boolean actualizarRegistro() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean consultarRegistro() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean borrarRegistro() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
