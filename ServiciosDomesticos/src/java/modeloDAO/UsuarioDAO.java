/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.locks.StampedLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import modeloVO.UsuarioVO;
import util.Conexion;
import util.Crud;

/**
 *
 * @author Admin
 */
public class UsuarioDAO extends Conexion implements Crud {

    //1.Declarar objetos y variables
    private Connection conexion = null;
    private PreparedStatement puente = null;
    private ResultSet mensajero = null;

    //1.1.Declarar variable para cualquier metodo
    public boolean operaciones = false;
    public String sql;

    //1.2.Declarar las variables del VO
    public String idUsuario = "", usuNombre = "", usuApellido = "", usuIdentificacion = "",
            usuFecha = "", usuCiudad = "", usuCorreo = "", usuTelefono = "", usuario = "",
            contrasena = "", rol_idRol = "", empPrecio = "", empPromedio = "";


    public UsuarioDAO() {

    }

    //2.
    public UsuarioDAO(UsuarioVO usuVO) {
        super();

        try {
            conexion = this.obtenerConexion();
            idUsuario = usuVO.getIdUsuario();
            usuNombre = usuVO.getUsuNombre();
            usuApellido = usuVO.getUsuApellido();
            usuIdentificacion = usuVO.getUsuIdentificacion();
            usuFecha = usuVO.getUsuFecha();
            usuCiudad = usuVO.getUsuCiudad();
            usuCorreo = usuVO.getUsuCorreo();
            usuTelefono = usuVO.getUsuTelefono();
            usuario = usuVO.getUsuario();
            contrasena = usuVO.getContrasena();
            rol_idRol = usuVO.getRol_idRol();
            empPrecio = usuVO.getEmpPrecio();
            empPromedio = usuVO.getEmpPromedio();

        } catch (Exception e) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public boolean agregarRegistro() {
        try {
            String sql1 = "SELECT usuCorreo, usuario FROM usuarios WHERE usuCorreo=? OR  usuario=?";
            puente = conexion.prepareStatement(sql1);
            puente.setString(1, usuCorreo);
            puente.setString(2, usuario);
            mensajero = puente.executeQuery();
            mensajero.last();
            if (mensajero.getRow() != 0) {
                return operaciones;
            } else {
                sql = "CALL `PAinsertUsuario`(?,?,?,?,?,?,?,?,?,?)";
                puente = conexion.prepareStatement(sql);
                puente.setString(1, usuNombre);
                puente.setString(2, usuApellido);
                puente.setString(3, usuIdentificacion);
                puente.setString(4, usuFecha);
                puente.setString(5, usuCiudad);
                puente.setString(6, usuCorreo);
                puente.setString(7, usuTelefono);
                puente.setString(8, usuario);
                puente.setString(9, contrasena);
                puente.setString(10, rol_idRol);
                puente.executeUpdate();
                operaciones = true;
            }
        } catch (SQLException e) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;

    }

    @Override
    public boolean actualizarRegistro() {
        try {           
            sql = "UPDATE usuarios SET usuNombre= ?, usuApellido =?, usuIdentificacion=?, usuFecha=?, usuCiudad=?, usuCorreo=?, usuTelefono=?, usuario=? WHERE idUsuario=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, usuNombre);
            puente.setString(2, usuApellido);
            puente.setString(3, usuIdentificacion);
            puente.setString(4, usuFecha);
            puente.setString(5, usuCiudad);
            puente.setString(6, usuCorreo);
            puente.setString(7, usuTelefono);
            puente.setString(8, usuario);
            puente.setString(9, idUsuario);
            puente.executeUpdate();
            operaciones = true;
        } catch (SQLException e) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;

    }

    @Override
    public boolean borrarRegistro() {
    }

    public boolean fotoPerfil(InputStream usuFoto) {
        try {
            sql = "UPDATE usuarios SET usuFoto=? WHERE idUsuario=?";
            puente = conexion.prepareStatement(sql);
            puente.setBinaryStream(1, usuFoto);
            puente.setString(2, idUsuario);
            puente.executeUpdate();
            operaciones = true;
        } catch (SQLException e) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;

    }

    public boolean iniciarSesion(String usuario, String contrasena) {

        try {
            conexion = this.obtenerConexion();
            sql = "SELECT * FROM vwrolesusuarios WHERE usuario=? AND contrasena=SHA1(?)";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, usuario);
            puente.setString(2, contrasena);
            mensajero = puente.executeQuery();
            if (mensajero.next()) {
                operaciones = true;
            }
        } catch (SQLException e) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;

    }

    public boolean correoContraseña(String usuCorreo) {
        try {
            conexion = this.obtenerConexion();
            sql = "SELECT usuCorreo FROM usuarios WHERE usuCorreo=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, usuCorreo);
            mensajero = puente.executeQuery();
            mensajero.last();
            if (mensajero.getRow() == 0) {
                return operaciones;
            } else {
                operaciones = true;
            }

        } catch (SQLException e) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;

    }

    public boolean cambairContraseña() {
        try {
            sql = "CALL PArestablecerContraseña(?,?)";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, contrasena);
            puente.setString(2, idUsuario);
            puente.executeUpdate();
            operaciones = true;
        } catch (SQLException e) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;

    }

    public boolean cambiarContraseñaSistema(String contrasenaNueva) {
        try {
            String sql1 = "SELECT contrasena FROM usuarios WHERE idUsuario=? AND contrasena=SHA1(?)";
            puente = conexion.prepareStatement(sql1);
            puente.setString(1, idUsuario);
            puente.setString(2, contrasena);
            mensajero = puente.executeQuery();
            mensajero.last();
            if (mensajero.getRow() != 0) {
                sql = "CALL PArestablecerContraseña(?,?)";
                puente = conexion.prepareStatement(sql);
                puente.setString(1, contrasenaNueva);
                puente.setString(2, idUsuario);
                puente.executeUpdate();
                operaciones = true;
            } else {
                return operaciones;
            }
        } catch (SQLException e) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;

    }
    public boolean citasEnProcesoAviso(String cliente_idCliente) {
        try {
            Conexion conexBd = new Conexion();
            Connection conexionn = conexBd.obtenerConexion();
            sql = "SELECT * FROM vwcitasenproceso WHERE cliente_idCliente="+cliente_idCliente+"";
            puente = conexionn.prepareStatement(sql);
            mensajero = puente.executeQuery();
            mensajero.last();
            if (mensajero.getRow() != 0) {
               operaciones=false;
            } else {
                operaciones=true;
            }
            
        } catch (SQLException e) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;

    }

    public static UsuarioVO ConsultarIdUsuario(String usuCorreo) {
        UsuarioVO usuVO = null;

        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT idUsuario, usuCorreo FROM usuarios WHERE usuCorreo=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, usuCorreo);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                usuVO = new UsuarioVO(mensajero.getString(1), usuCorreo);

            }

        } catch (SQLException e) {
            Logger.getLogger(UsuarioDAO.class
                    .getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();

            } catch (SQLException e) {
                Logger.getLogger(UsuarioDAO.class
                        .getName()).log(Level.SEVERE, null, e);
            }
        }
        return usuVO;
    }

    public static UsuarioVO consultarIdCliente(String usuario) {
        UsuarioVO usuVO = null;

        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT * FROM vwlogincliente WHERE usuario=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, usuario);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                usuVO = new UsuarioVO(mensajero.getString(1), mensajero.getString(2),
                        mensajero.getString(3), mensajero.getString(4),
                        mensajero.getString(5), mensajero.getString(6),
                        mensajero.getString(7), mensajero.getString(8),
                        usuario, mensajero.getString(10),
                        mensajero.getString(11), mensajero.getString(12), mensajero.getString(13),
                        mensajero.getString(14), mensajero.getString(15), mensajero.getString(16));

            }

        } catch (SQLException e) {
            Logger.getLogger(UsuarioDAO.class
                    .getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();

            } catch (SQLException e) {
                Logger.getLogger(UsuarioDAO.class
                        .getName()).log(Level.SEVERE, null, e);
            }
        }
        return usuVO;
    }

    public static UsuarioVO consultarIdEmpleado(String usuario) {
        UsuarioVO usuVO = null;

        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT * FROM usuarios INNER JOIN empleados ON usuarios.idUsuario=empleados.usuario_idUsuario WHERE usuario=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, usuario);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                usuVO = new UsuarioVO(mensajero.getString(1), mensajero.getBinaryStream(2),
                        mensajero.getString(3), mensajero.getString(4),
                        mensajero.getString(5), mensajero.getString(6),
                        mensajero.getString(7), mensajero.getString(8),
                        mensajero.getString(9), mensajero.getString(10),
                        usuario, mensajero.getString(12), mensajero.getString(13),
                        mensajero.getString(14), mensajero.getString(15), mensajero.getString(16),
                        mensajero.getString(17), mensajero.getString(18), mensajero.getString(19));

            }

        } catch (SQLException e) {
            Logger.getLogger(UsuarioDAO.class
                    .getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();

            } catch (SQLException e) {
                Logger.getLogger(UsuarioDAO.class
                        .getName()).log(Level.SEVERE, null, e);
            }
        }
        return usuVO;
    }

    public static UsuarioVO consultarEmpleadoAge(String usuario) {
        UsuarioVO usuVO = null;

        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT * FROM usuarios INNER JOIN empleados ON usuarios.idUsuario=empleados.usuario_idUsuario INNER JOIN agenda ON empleados.idEmpleado=agenda.empleado_idEmpleado WHERE usuario=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, usuario);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                usuVO = new UsuarioVO(mensajero.getString(1), mensajero.getBinaryStream(2),
                        mensajero.getString(3), mensajero.getString(4),
                        mensajero.getString(5), mensajero.getString(6),
                        mensajero.getString(7), mensajero.getString(8),
                        mensajero.getString(9), mensajero.getString(10),
                        usuario, mensajero.getString(12), mensajero.getString(13),
                        mensajero.getString(14), mensajero.getString(15), mensajero.getString(16),
                        mensajero.getString(17), mensajero.getString(18), mensajero.getString(19),
                        mensajero.getString(20), mensajero.getString(21), mensajero.getString(22),
                        mensajero.getString(23));

            }

        } catch (SQLException e) {
            Logger.getLogger(UsuarioDAO.class
                    .getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();

            } catch (SQLException e) {
                Logger.getLogger(UsuarioDAO.class
                        .getName()).log(Level.SEVERE, null, e);
            }
        }
        return usuVO;
    }

    public static UsuarioVO consultarRol(String usuario, String contrasena) {
        UsuarioVO usuVO = null;
        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT idRol FROM  usuarios INNER JOIN rol ON usuarios.rol_idRol=rol.idRol  WHERE usuario=? AND contrasena=SHA1(?)";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, usuario);
            puente.setString(2, contrasena);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                usuVO = new UsuarioVO(mensajero.getString(1));
            }
        } catch (SQLException e) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return usuVO;

    }

    public ArrayList<UsuarioVO> listaEmpleados() {

        UsuarioVO usuVO = null;
        conexion = this.obtenerConexion();
        ArrayList<UsuarioVO> listaEmp = new ArrayList<>();
        try {
            sql = "SELECT * from vwinfoprincipal";
            puente = conexion.prepareStatement(sql);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                usuVO = new UsuarioVO(mensajero.getString(1),
                        mensajero.getBinaryStream(2), mensajero.getString(3),
                        mensajero.getString(4), mensajero.getString(5),
                        mensajero.getString(6), mensajero.getString(7),
                        mensajero.getString(8), mensajero.getString(9),
                        mensajero.getString(10));
                listaEmp.add(usuVO);

            }
        } catch (Exception e) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listaEmp;
    }
    public void listarEmpleadosImg(String idUsuario,HttpServletResponse response){
             InputStream inputStream= null;
             OutputStream outputStream=null;
             BufferedInputStream bufferedInputStream=null;
             BufferedOutputStream bufferedOutputStream=null;
             response.setContentType("image/png");
        try {
            conexion = this.obtenerConexion();
             sql = "SELECT * FROM usuarios WHERE idUsuario=?";
             outputStream=response.getOutputStream();
             puente = conexion.prepareStatement(sql);
             puente.setString(1, idUsuario);
             mensajero = puente.executeQuery();
            if (mensajero.next()) {
                inputStream=mensajero.getBinaryStream("usuFoto");
            }
            bufferedInputStream=new BufferedInputStream(inputStream);
            bufferedOutputStream=new BufferedOutputStream(outputStream);
            int i=0;
            while((i=bufferedInputStream.read())!=-1){
                bufferedOutputStream.write(i);
            }

        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException ex) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    public ArrayList<UsuarioVO> listaEmpleadosBuscar(String empPrecio, String usuCiudad, String empPromedio) {

        UsuarioVO usuVO = null;
        conexion = this.obtenerConexion();
        ArrayList<UsuarioVO> listaEmp = new ArrayList<>();
        try {
            sql = "SELECT * from vwinfoprincipal WHERE Precio LIKE '%" + empPrecio + "%' OR Ciudad LIKE '%" + usuCiudad + "%' OR Promedio LIKE '%" + empPromedio + "%'";
            puente = conexion.prepareStatement(sql);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                usuVO = new UsuarioVO(mensajero.getString(1),
                        mensajero.getBinaryStream(2), mensajero.getString(3),
                        mensajero.getString(4), mensajero.getString(5),
                        mensajero.getString(6), mensajero.getString(7),
                        mensajero.getString(8), mensajero.getString(9),
                        mensajero.getString(10));
                listaEmp.add(usuVO);

            }
        } catch (Exception e) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listaEmp;
    }

    public static UsuarioVO consultarPorId(String idUsuario) {
        UsuarioVO usuVO = null;

        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT * from vwingocompletaempleados WHERE idUsuario=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, idUsuario);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                usuVO = new UsuarioVO(idUsuario, mensajero.getString(2),
                        mensajero.getBinaryStream(3), mensajero.getString(4),
                        mensajero.getString(5), mensajero.getString(6),
                        mensajero.getString(7), mensajero.getString(8),
                        mensajero.getString(9), mensajero.getString(10),
                        mensajero.getString(11), mensajero.getString(12),
                        mensajero.getString(13), mensajero.getString(14),
                        mensajero.getString(15));

            }

        } catch (SQLException e) {
            Logger.getLogger(UsuarioDAO.class
                    .getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();

            } catch (SQLException e) {
                Logger.getLogger(UsuarioDAO.class
                        .getName()).log(Level.SEVERE, null, e);
            }
        }
        return usuVO;
    }
    public void consultarPorIdImg(String idUsuario,HttpServletResponse response){
             InputStream inputStream= null;
             OutputStream outputStream=null;
             BufferedInputStream bufferedInputStream=null;
             BufferedOutputStream bufferedOutputStream=null;
             response.setContentType("image/png");
        try {
            conexion = this.obtenerConexion();
             sql = "SELECT * from vwingocompletaempleados WHERE idUsuario=?";
             outputStream=response.getOutputStream();
             puente = conexion.prepareStatement(sql);
             puente.setString(1, idUsuario);
             mensajero = puente.executeQuery();
            if (mensajero.next()) {
                inputStream=mensajero.getBinaryStream("usuFoto");
            }
            bufferedInputStream=new BufferedInputStream(inputStream);
            bufferedOutputStream=new BufferedOutputStream(outputStream);
            int i=0;
            while((i=bufferedInputStream.read())!=-1){
                bufferedOutputStream.write(i);
            }

        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException ex) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    public ArrayList<UsuarioVO> listaDias(String idAgenda) {

        UsuarioVO usuVO = null;
        conexion = this.obtenerConexion();
        ArrayList<UsuarioVO> listaHoras = new ArrayList<>();
        try {
            sql = "SELECT proDia, proHoraInicio, proHoraFin FROM programacionagenda where agenda_idAgenda=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idAgenda);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                usuVO = new UsuarioVO(mensajero.getString(1),
                        mensajero.getString(2), mensajero.getString(3));
                listaHoras.add(usuVO);

            }
        } catch (Exception e) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listaHoras;
    }
    
     public void imgUsuario(String idUsuario,HttpServletResponse response){
             InputStream inputStream= null;
             OutputStream outputStream=null;
             BufferedInputStream bufferedInputStream=null;
             BufferedOutputStream bufferedOutputStream=null;
             response.setContentType("image/png");
        try {
            conexion = this.obtenerConexion();
             sql = "SELECT usuFoto FROM usuarios WHERE idUsuario=?";
             outputStream=response.getOutputStream();
             puente = conexion.prepareStatement(sql);
             puente.setString(1, idUsuario);
             mensajero = puente.executeQuery();
            if (mensajero.next()) {
                inputStream=mensajero.getBinaryStream("usuFoto");
            }
            bufferedInputStream=new BufferedInputStream(inputStream);
            bufferedOutputStream=new BufferedOutputStream(outputStream);
            int i=0;
            while((i=bufferedInputStream.read())!=-1){
                bufferedOutputStream.write(i);
            }

        } catch (SQLException e) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }
     
      

}
