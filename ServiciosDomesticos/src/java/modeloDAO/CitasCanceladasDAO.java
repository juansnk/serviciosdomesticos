/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modeloVO.CitasCanceladasVO;
import util.Conexion;

/**
 *
 * @author Admin
 */
public class CitasCanceladasDAO extends Conexion {
    //1.Declarar objetos y variables

    private Connection conexion = null;
    private PreparedStatement puente = null;
    private ResultSet mensajero = null;

    //1.1.Declarar variable para cualquier metodo
    public boolean operaciones = false;
    public String sql;

    //1.2.Declarar las variables del VO
    public String idCita= "", fecha="", direccion= "", horaInicio= "",
            horaFin = "", estado="", cliente="", fechaCancelada="", descripcion="";

    public CitasCanceladasDAO() {
    }
    
    public CitasCanceladasDAO(CitasCanceladasVO cicVO) {

        super();

        try {

            conexion = this.obtenerConexion();
            idCita= cicVO.getIdCita();
            fecha= cicVO.getFecha();
            direccion= cicVO.getDireccion();
            horaInicio= cicVO.getHoraInicio();
            horaFin= cicVO.getHoraFin();
            estado= cicVO.getEstado();
            cliente= cicVO.getCliente();
            fechaCancelada= cicVO.getFechaCancelada();
            descripcion= cicVO.getDescripcion();

        } catch (Exception e) {
            Logger.getLogger(CitasCanceladasDAO.class.getName()).log(Level.SEVERE, null, e);

        }
    }
    public boolean registroDescripcion() {
        try {
            sql = "CALL PAcitasCanceladas(?,?)";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, descripcion);
            puente.setString(2, idCita);
            puente.executeUpdate();
            operaciones = true;

        } catch (SQLException e) {
            Logger.getLogger(CitasCanceladasDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitasCanceladasDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }
     public boolean borrarRegistro() {
        try {
            sql = "DELETE FROM citas_canceladas WHERE idCita=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idCita);
            puente.executeUpdate();
            operaciones = true;

        } catch (SQLException e) {
            Logger.getLogger(CitasCanceladasDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitasCanceladasDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }
     public ArrayList<CitasCanceladasVO> listarCitasCanceladas(String cliente) {

        CitasCanceladasVO cicVO = null;
        conexion = this.obtenerConexion();
        ArrayList<CitasCanceladasVO> listarCanceladas = new ArrayList<>();
        try {
            sql = "SELECT * FROM citas_canceladas WHERE cliente=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, cliente);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                cicVO = new CitasCanceladasVO(mensajero.getString(1),
                        mensajero.getString(2), mensajero.getString(3),
                        mensajero.getString(4), mensajero.getString(5),
                        mensajero.getString(6),cliente,
                        mensajero.getString(8),mensajero.getString(9));
                listarCanceladas.add(cicVO);

            }
        } catch (Exception e) {
            Logger.getLogger(CitasCanceladasDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listarCanceladas;
    }
     public static CitasCanceladasVO consultarIdCitaC(String idCita) {
        CitasCanceladasVO cicVO = null;

        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT idCita from citas_canceladas  WHERE idCita=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, idCita);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                cicVO = new CitasCanceladasVO(mensajero.getString(1));

            }

        } catch (SQLException e) {
            Logger.getLogger(CitasCanceladasDAO.class
                    .getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();

            } catch (SQLException e) {
                Logger.getLogger(CitasCanceladasDAO.class
                        .getName()).log(Level.SEVERE, null, e);
            }
        }
        return cicVO;
     }
}
