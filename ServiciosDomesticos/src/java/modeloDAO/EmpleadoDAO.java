/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import modeloVO.EmpleadoVO;
import util.Conexion;
import util.Crud;

/**
 *
 * @author Admin
 */
public class EmpleadoDAO extends Conexion implements Crud {

    //1.Declarar objetos y variables
    private Connection conexion = null;
    private PreparedStatement puente = null;
    private ResultSet mensajero = null;

    //1.1.Declarar variable para cualquier metodo
    public boolean operaciones = false;
    public String sql;

    //1.2.Declarar las variables del VO
    public String idEmpleado = "", empPromedio = "", empExperiencia = "", empDescripcion = "",
            usuario_idUsuario = "";

    public EmpleadoDAO() {
    }

    //2.
    public EmpleadoDAO(EmpleadoVO empVO) {
        super();

        try {
            conexion = this.obtenerConexion();
            idEmpleado = empVO.getIdEmpleado();
            empPromedio = empVO.getEmpPromedio();
            empExperiencia = empVO.getEmpExperiencia();
            empDescripcion = empVO.getEmpDescripcion();
            usuario_idUsuario = empVO.getUsuario_idUsuario();

        } catch (Exception e) {
            Logger.getLogger(EmpleadoDAO.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public boolean agregarRegistro() {
        try {
            sql = "CALL PAinsertDaEmpleado (?,?,?)";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idEmpleado);
            puente.setString(2, empExperiencia);
            puente.setString(3, empDescripcion);
            puente.executeUpdate();
            operaciones = true;
        } catch (SQLException e) {
            Logger.getLogger(EmpleadoDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(EmpleadoDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }

    @Override
    public boolean actualizarRegistro() {
        try {
            sql = "CALL `PAinsertDaEmpleado`(?,?,?)";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idEmpleado);
            puente.setString(1, empExperiencia);
            puente.setString(2, empDescripcion);
            puente.executeUpdate();
            operaciones = true;
        } catch (SQLException e) {
            Logger.getLogger(EmpleadoDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(EmpleadoDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }

    @Override
    public boolean borrarRegistro() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public ArrayList<EmpleadoVO> listarEmpleadaNotificacion(String idEmpleado) {

        EmpleadoVO empVO = null;
        conexion = this.obtenerConexion();
        ArrayList<EmpleadoVO> listaCitasNo = new ArrayList<>();
        try {
            sql = "SELECT * FROM vwcitanotificacion WHERE idEmpleado=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idEmpleado);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                empVO = new EmpleadoVO(idEmpleado,
                        mensajero.getString(2), mensajero.getString(3),
                        mensajero.getString(4), mensajero.getString(5),
                        mensajero.getString(6),mensajero.getString(7));
                listaCitasNo.add(empVO);

            }
        } catch (Exception e) {
            Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listaCitasNo;
    }

    public ArrayList<EmpleadoVO> listarCitasEmpleada(String idEmpleado) {

        EmpleadoVO empVO = null;
        conexion = this.obtenerConexion();
        ArrayList<EmpleadoVO> listaCitas = new ArrayList<>();
        try {
            sql = "SELECT * FROM vwcitasempleada WHERE idEmpleado=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idEmpleado);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                empVO = new EmpleadoVO(idEmpleado,
                        mensajero.getString(2), mensajero.getString(3),
                        mensajero.getString(4), mensajero.getString(5),
                        mensajero.getString(6), mensajero.getString(7),
                        mensajero.getString(8), mensajero.getString(9),
                        mensajero.getString(10));
                listaCitas.add(empVO);

            }
        } catch (Exception e) {
            Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listaCitas;
    }

    public ArrayList<EmpleadoVO> listarOpiniones(String idEmpleado) {

        EmpleadoVO empVO = null;
        conexion = this.obtenerConexion();
        ArrayList<EmpleadoVO> listaOpi = new ArrayList<>();
        try {
            sql = "SELECT * FROM vwopinion_cita WHERE idEmpleado=? LIMIT 3";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idEmpleado);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                empVO = new EmpleadoVO(idEmpleado,
                        mensajero.getString(2), mensajero.getString(3),
                        mensajero.getBinaryStream(4), mensajero.getString(5),
                        mensajero.getString(6), mensajero.getString(7),
                        mensajero.getString(8),mensajero.getString(9));
                listaOpi.add(empVO);

            }
        } catch (Exception e) {
            Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listaOpi;
    }
    public void listarOpinionesImg(HttpServletResponse response){
             InputStream inputStream= null;
             OutputStream outputStream=null;
             BufferedInputStream bufferedInputStream=null;
             BufferedOutputStream bufferedOutputStream=null;
             response.setContentType("image/*");
        try {
            conexion = this.obtenerConexion();
             sql = "SELECT * FROM vwopinion_cita LIMIT 3";
              outputStream=response.getOutputStream();
            puente = conexion.prepareStatement(sql);
             mensajero = puente.executeQuery();
            if (mensajero.next()) {
                inputStream=(mensajero.getBinaryStream("usuFoto"));
            }
            bufferedInputStream=new BufferedInputStream(inputStream);
            bufferedOutputStream=new BufferedOutputStream(outputStream);
            int i=0;
            while((i=bufferedInputStream.read())!=-1){
                bufferedOutputStream.write(i);
            }
        } catch (SQLException | IOException e) {
            Logger.getLogger(EmpleadoDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(EmpleadoDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }
    
    public ArrayList<EmpleadoVO> listarCitasActivasEmpleada(String idEmpleado) {

        EmpleadoVO empVO = null;
        conexion = this.obtenerConexion();
        ArrayList<EmpleadoVO> listaCitasActivas = new ArrayList<>();
        try {
            sql = "SELECT * FROM vwcitasactivasempleado WHERE idEmpleado=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idEmpleado);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                empVO = new EmpleadoVO(idEmpleado,
                        mensajero.getString(2), mensajero.getString(3),
                        mensajero.getString(4), mensajero.getString(5),
                        mensajero.getString(6), mensajero.getString(7),
                        mensajero.getString(8), mensajero.getBinaryStream(9),
                        mensajero.getString(10),mensajero.getString(11),
                        mensajero.getString(12),mensajero.getString(13));
                listaCitasActivas.add(empVO);

            }
        } catch (Exception e) {
            Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listaCitasActivas;
    }
    
     public ArrayList<EmpleadoVO> listarCitasInactivasEmpleada(String idEmpleado) {

        EmpleadoVO empVO = null;
        conexion = this.obtenerConexion();
        ArrayList<EmpleadoVO> listaCitasInactivas = new ArrayList<>();
        try {
            sql = "SELECT * FROM vwcitasinactivasempleado WHERE idEmpleado=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idEmpleado);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                empVO = new EmpleadoVO(idEmpleado,
                        mensajero.getString(2), mensajero.getString(3),
                        mensajero.getString(4), mensajero.getString(5),
                        mensajero.getString(6), mensajero.getString(7),
                        mensajero.getString(8), mensajero.getBinaryStream(9),
                        mensajero.getString(10),mensajero.getString(11),
                        mensajero.getString(12),mensajero.getString(13),
                        mensajero.getString(14));
                listaCitasInactivas.add(empVO);

            }
        } catch (Exception e) {
            Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listaCitasInactivas;
    }
}
