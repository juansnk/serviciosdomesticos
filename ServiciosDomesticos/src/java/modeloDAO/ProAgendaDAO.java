/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modeloVO.AgendaVO;
import modeloVO.ProAgendaVO;
import util.Conexion;
import util.Crud;

/**
 *
 * @author Admin
 */
public class ProAgendaDAO extends Conexion implements Crud{

      //1.Declarar objetos y variables

    private Connection conexion = null;
    private PreparedStatement puente = null;
    private ResultSet mensajero = null;

    //1.1.Declarar variable para cualquier metodo
    public boolean operaciones = false;
    public String sql;

    //1.2.Declarar las variables del VO
    public String idProgramacionAgenda= "", proDia="", proHoraInicio= "", proHoraFin= "",
            agenda_idAgenda = "";

    public ProAgendaDAO() {
    }

    public ProAgendaDAO(ProAgendaVO proVO) {

        super();

        try {

            conexion = this.obtenerConexion();
            idProgramacionAgenda= proVO.getIdProgramacionAgenda();
            proDia = proVO.getProDia();
            proHoraInicio = proVO.getProHoraInicio();
            proHoraFin = proVO.getProHoraFin();
            agenda_idAgenda = proVO.getAgenda_idAgenda();

        } catch (Exception e) {
            Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);

        }

    }

    @Override
    public boolean agregarRegistro() {
        try {
            sql = "UPDATE programacionagenda SET proHoraInicio=?, proHoraFin=? WHERE idProgramacionAgenda=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, proHoraInicio);
            puente.setString(2, proHoraFin);
            puente.setString(3, idProgramacionAgenda);
            puente.executeUpdate();
            operaciones = true;

        } catch (SQLException e) {
            Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }

    @Override
    public boolean actualizarRegistro() {
         try {
            sql = "UPDATE programacionagenda SET proHoraInicio=?, proHoraFin=? WHERE idProgramacionAgenda=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, proHoraInicio);
            puente.setString(2, proHoraFin);
            puente.setString(3, idProgramacionAgenda);
            puente.executeUpdate();
            operaciones = true;

        } catch (SQLException e) {
            Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }


    @Override
    public boolean borrarRegistro() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     public ArrayList<ProAgendaVO> listarFK(String idAgenda) {

        ProAgendaVO proVO = null;
        conexion = this.obtenerConexion();
        ArrayList<ProAgendaVO> listaProAgenda = new ArrayList<>();
        try {
            sql = "SELECT * FROM programacionagenda WHERE agenda_idAgenda=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idAgenda);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                proVO = new ProAgendaVO(mensajero.getString(1),
                        mensajero.getString(2), mensajero.getString(3),
                        mensajero.getString(4), agenda_idAgenda);
                listaProAgenda.add(proVO);

            }
        } catch (Exception e) {
            Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listaProAgenda;
    }
     public ArrayList<ProAgendaVO> listarHoras(String idProgramacionAgenda) {

        ProAgendaVO proVO = null;
        conexion = this.obtenerConexion();
        ArrayList<ProAgendaVO> listarHoras = new ArrayList<>();
        try {
            sql = "SELECT * FROM programacionagenda WHERE idProgramacionAgenda=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idProgramacionAgenda);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                proVO = new ProAgendaVO(mensajero.getString(1),
                        mensajero.getString(2), mensajero.getString(3),
                        mensajero.getString(4), agenda_idAgenda);
                listarHoras.add(proVO);

            }
        } catch (Exception e) {
            Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listarHoras;
    }
     
     public static ProAgendaVO consultarFK(String agenda_idAgenda) {
        ProAgendaVO proVO = null;
        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT * FROM programacionagenda WHERE agenda_idAgenda=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, agenda_idAgenda);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                proVO = new ProAgendaVO(mensajero.getString(1), mensajero.getString(2),
                        mensajero.getString(3), mensajero.getString(4),
                        agenda_idAgenda);                            
            }

        } catch (SQLException e) {
            Logger.getLogger(ProAgendaDAO.class
                    .getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();

            } catch (SQLException e) {
                Logger.getLogger(ProAgendaDAO.class
                        .getName()).log(Level.SEVERE, null, e);
            }
        }
        return proVO;
    } 
      public static ProAgendaVO consultarPro(String idProgramacionAgenda) {
        ProAgendaVO proVO = null;
        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT * FROM programacionagenda WHERE idProgramacionAgenda=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, idProgramacionAgenda);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                proVO = new ProAgendaVO(idProgramacionAgenda, mensajero.getString(2),
                        mensajero.getString(3), mensajero.getString(4),
                        mensajero.getString(5));                            
            }

        } catch (SQLException e) {
            Logger.getLogger(ProAgendaDAO.class
                    .getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();

            } catch (SQLException e) {
                Logger.getLogger(ProAgendaDAO.class
                        .getName()).log(Level.SEVERE, null, e);
            }
        }
        return proVO;
    } 
     
}
