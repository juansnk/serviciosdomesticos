/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import modeloVO.CitaVO;
import modeloVO.ProAgendaVO;
import util.Conexion;
import util.Crud;

/**
 *
 * @author Admin
 */
public class CitaDAO extends Conexion implements Crud {

    //1.Declarar objetos y variables

    private Connection conexion = null;
    private PreparedStatement puente = null;
    private ResultSet mensajero = null;

    //1.1.Declarar variable para cualquier metodo
    public boolean operaciones = false;
    public String sql;

    //1.2.Declarar las variables del VO
    public String idCita = "", citFecha = "", citDireccion = "", citHoraInicio = "",
            citHoraFin = "", citEstado="",citCalificacion="", citOpinion="", cliente_idCliente = "", agenda_idProgramacion = "", agenda_idAgenda;

    public CitaDAO() {
    }
    
    //2.
    public CitaDAO(CitaVO citVO) {
        super();

        try {
            conexion = this.obtenerConexion();
            idCita = citVO.getIdCita();
            citFecha = citVO.getCitFecha();
            citDireccion = citVO.getCitDireccion();
            citHoraInicio = citVO.getCitHoraInicio();
            citHoraFin = citVO.getCitHoraFin();            
            citEstado= citVO.getCitEstado();
            citCalificacion= citVO.getCitCalificacion();
            citOpinion = citVO.getCitOpinion();
            cliente_idCliente = citVO.getCliente_idCliente();
            agenda_idProgramacion = citVO.getAgenda_idProgramacion();
            agenda_idAgenda = citVO.getAgenda_idAgenda();

        } catch (Exception e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
    } 

    @Override
    public boolean agregarRegistro() {
        try {
            String sql1="CALL PAvalidaCitaEmpleada(?,?,?,?);";
            puente = conexion.prepareStatement(sql1);
            puente.setString(1, citFecha);
            puente.setString(2, agenda_idAgenda);
            puente.setString(3, citHoraInicio);
            puente.setString(4, citHoraFin);
            mensajero = puente.executeQuery();
            mensajero.last();
            if (mensajero.getRow() == 0) {
                return operaciones;
            } else{
            sql="CALL PAregistrarCitas(?,?,?,?,?,?,?)";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, citFecha);
            puente.setString(2, citDireccion);
            puente.setString(3, citHoraInicio);
            puente.setString(4, citHoraFin);
            puente.setString(5, citEstado);
            puente.setString(6, cliente_idCliente);
            puente.setString(7, agenda_idAgenda);
            puente.executeUpdate();
            operaciones = true;
            }
        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }

    @Override
    public boolean actualizarRegistro() {
        try {
            String sql1="CALL PAvalidaCitaEmpleada(?,?,?,?);";
            puente = conexion.prepareStatement(sql1);
            puente.setString(1, citFecha);
            puente.setString(2, agenda_idAgenda);
            puente.setString(3, citHoraInicio);
            puente.setString(4, citHoraFin);
            mensajero = puente.executeQuery();
            mensajero.last();
            if (mensajero.getRow() == 0) {
                return operaciones;
            } else{
            sql = "UPDATE citas SET citDireccion= ?, citHoraInicio= ?, citHoraFin =? WHERE idCita=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, citDireccion);
            puente.setString(2, citHoraInicio);
            puente.setString(3, citHoraFin);
            puente.setString(4, idCita);
            puente.executeUpdate();
            operaciones = true;
            }
        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }

    @Override
    public boolean borrarRegistro() {
        try {
            sql = "CALL PAcancelarCita (?)";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idCita);
            puente.executeUpdate();
            operaciones = true;
        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }
    public boolean cancelarCitaEmpleado() {
        try {
            sql = "CALL PAcancelarCita (?)";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idCita);
            puente.executeUpdate();
            operaciones = true;
        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }
    
    public boolean aceptarCitaEmpleado() {
        try {
            sql = "UPDATE citas SET citEstado=? WHERE idCita=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, citEstado);
            puente.setString(2, idCita);
            puente.executeUpdate();
            operaciones = true;
        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }
    public boolean calificacionYopinion() {
        try {
            sql = "CALL PAupdateCalificacion(?,?,?,?,?)";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, citEstado);
            puente.setString(2, citCalificacion);
            puente.setString(3, citOpinion);
            puente.setString(4, idCita);
            puente.setString(5, agenda_idProgramacion);
            puente.executeUpdate();
            operaciones = true;
        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }
     public boolean aceptarFactura() {
        try {
            sql = "UPDATE citas SET citEstado=? WHERE idCita=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, citEstado);
            puente.setString(2, idCita);
            puente.executeUpdate();
            operaciones = true;
        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }
     public boolean cancelarFactura() {
        try {
            sql = "CALL PAcancelarCita (?)";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idCita);
            puente.executeUpdate();
            operaciones = true;
        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }
     public static CitaVO consultarIdCita(String citFecha, String citDireccion, String citHoraInicio, String cliente_idCliente) {
        CitaVO citVO = null;

        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT idCita from citas  WHERE citFecha=? AND citDireccion=? AND citHoraInicio=? AND cliente_idCliente=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, citFecha);
            puente.setString(2, citDireccion);
            puente.setString(3, citHoraInicio);
            puente.setString(4, cliente_idCliente);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                citVO = new CitaVO(mensajero.getString(1));

            }

        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class
                    .getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();

            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class
                        .getName()).log(Level.SEVERE, null, e);
            }
        }
        return citVO;
     }
     
     public static CitaVO consultarIdCita(String idCita) {
        CitaVO citVO = null;

        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT * FROM citas WHERE idCita=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, idCita);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                citVO = new CitaVO(idCita, mensajero.getString(2),
                        mensajero.getString(3));
            }

        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return citVO;
    }
     
    public ArrayList<CitaVO> listarCitas(String cliente_idCliente) {

        CitaVO citVO = null;
        conexion = this.obtenerConexion();
        ArrayList<CitaVO> listaCitas = new ArrayList<>();
        try {
            sql = "SELECT * FROM vwcitasactivas WHERE cliente_idCliente=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, cliente_idCliente);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                citVO = new CitaVO(mensajero.getString(1),
                        mensajero.getString(2), mensajero.getBinaryStream(3),
                        mensajero.getString(4), mensajero.getString(5),
                        mensajero.getString(6), mensajero.getString(7),
                        mensajero.getString(8), mensajero.getString(9),
                        mensajero.getString(10), mensajero.getString(11),
                        mensajero.getString(12),cliente_idCliente);
                listaCitas.add(citVO);

            }
        } catch (Exception e) {
            Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listaCitas;
    }
    public void listarImg(String cliente_idCliente,HttpServletResponse response){
             InputStream inputStream= null;
             OutputStream outputStream=null;
             BufferedInputStream bufferedInputStream=null;
             BufferedOutputStream bufferedOutputStream=null;
             response.setContentType("image/*");
        try {
            conexion = this.obtenerConexion();
             sql = "SELECT * FROM vwcitasactivas WHERE cliente_idCliente=?";
              outputStream=response.getOutputStream();
            puente = conexion.prepareStatement(sql);
            puente.setString(1, cliente_idCliente);
             mensajero = puente.executeQuery();
            if (mensajero.next()) {
                inputStream=(mensajero.getBinaryStream("usuFoto"));
            }
            bufferedInputStream=new BufferedInputStream(inputStream);
            bufferedOutputStream=new BufferedOutputStream(outputStream);
            int i=0;
            while((i=bufferedInputStream.read())!=-1){
                bufferedOutputStream.write(i);
            }
        } catch (SQLException | IOException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }
    
    public ArrayList<CitaVO> listarCitasI(String cliente_idCliente) {

        CitaVO citVO = null;
        conexion = this.obtenerConexion();
        ArrayList<CitaVO> listaCitasI = new ArrayList<>();
        try {
            sql = "SELECT * FROM vwcitasinactivas WHERE cliente_idCliente=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, cliente_idCliente);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                citVO = new CitaVO(mensajero.getString(1),
                        mensajero.getString(2), mensajero.getBinaryStream(3),
                        mensajero.getString(4), mensajero.getString(5),
                        mensajero.getString(6), mensajero.getString(7),
                        mensajero.getString(8), mensajero.getString(9),
                        mensajero.getString(10),mensajero.getString(11),
                        mensajero.getString(12), mensajero.getString(13), cliente_idCliente);
                listaCitasI.add(citVO);

            }
        } catch (Exception e) {
            Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listaCitasI;
    }   
    public ArrayList<CitaVO> listarCitasEnProceso(String cliente_idCliente) {

        CitaVO citVO = null;
        conexion = this.obtenerConexion();
        ArrayList<CitaVO> listaCitasP = new ArrayList<>();
        try {
            sql = "SELECT * FROM vwcitasenproceso WHERE cliente_idCliente=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, cliente_idCliente);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                citVO = new CitaVO(mensajero.getString(1),
                        mensajero.getString(2), mensajero.getBinaryStream(3),
                        mensajero.getString(4), mensajero.getString(5),
                        mensajero.getString(6), mensajero.getString(7),
                        mensajero.getString(8), mensajero.getString(9),
                        mensajero.getString(10),mensajero.getString(11),
                        mensajero.getString(12),mensajero.getString(13),
                        mensajero.getString(14),mensajero.getString(15),
                        mensajero.getString(16),cliente_idCliente);
                listaCitasP.add(citVO);

            }
        } catch (Exception e) {
            Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listaCitasP;
    }   
    
     public static CitaVO consultarCitaEmpleada(String idCita) {
        CitaVO citVO = null;

        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT * FROM vwinformacioncita WHERE idCita=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, idCita);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                citVO = new CitaVO(idCita, mensajero.getString(2),
                        mensajero.getString(3),mensajero.getString(4),
                        mensajero.getString(5),mensajero.getString(6),
                        mensajero.getBinaryStream(7),mensajero.getString(8),
                        mensajero.getString(9),mensajero.getString(10),
                        mensajero.getString(11),mensajero.getString(12));
            }

        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return citVO;
    }
     
     public void consultarCitaEmpleadaImg(String idCita,HttpServletResponse response){
             InputStream inputStream= null;
             OutputStream outputStream=null;
             BufferedInputStream bufferedInputStream=null;
             BufferedOutputStream bufferedOutputStream=null;
             response.setContentType("image/*");
        try {
            conexion = this.obtenerConexion();
             sql = "SELECT * FROM vwinformacioncita WHERE idCita=?";
             outputStream=response.getOutputStream();
             puente = conexion.prepareStatement(sql);
            puente.setString(1, idCita);
             mensajero = puente.executeQuery();
            if (mensajero.next()) {
                inputStream=mensajero.getBinaryStream("usuFoto");
            }
            bufferedInputStream=new BufferedInputStream(inputStream);
            bufferedOutputStream=new BufferedOutputStream(outputStream);
            int i=0;
            while((i=bufferedInputStream.read())!=-1){
                bufferedOutputStream.write(i);
            }

        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException ex) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        
    }
    
 
     public static CitaVO consultarCitaId(String idCita) {
        CitaVO citVO = null;

        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT * FROM vwinfo_citas WHERE idCita=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, idCita);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                citVO = new CitaVO(idCita, mensajero.getString(2),
                        mensajero.getBinaryStream(3),mensajero.getString(4),
                        mensajero.getString(5),mensajero.getString(6),
                        mensajero.getString(7),mensajero.getString(8),
                        mensajero.getString(9),mensajero.getString(10),
                        mensajero.getString(11),mensajero.getString(12),
                        mensajero.getString(13),mensajero.getString(14),
                        mensajero.getString(15),mensajero.getString(16));
            }

        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return citVO;
    }
      public void consultarCitaImg(String idCita,HttpServletResponse response){
             InputStream inputStream= null;
             OutputStream outputStream=null;
             BufferedInputStream bufferedInputStream=null;
             BufferedOutputStream bufferedOutputStream=null;
             response.setContentType("image/*");
        try {
            conexion = this.obtenerConexion();
             sql = "SELECT * FROM vwinfo_citas WHERE idCita=?";
             outputStream=response.getOutputStream();
             puente = conexion.prepareStatement(sql);
            puente.setString(1, idCita);
             mensajero = puente.executeQuery();
            if (mensajero.next()) {
                inputStream=mensajero.getBinaryStream("usuFoto");
            }
            bufferedInputStream=new BufferedInputStream(inputStream);
            bufferedOutputStream=new BufferedOutputStream(outputStream);
            int i=0;
            while((i=bufferedInputStream.read())!=-1){
                bufferedOutputStream.write(i);
            }

        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException ex) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        
    }
     public static CitaVO consultarCalificacionCita(String idCita) {
        CitaVO citVO = null;

        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT idCita, citCalificacion, citOpinion, cliente_idCliente FROM citas WHERE idCita=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, idCita);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                citVO = new CitaVO(idCita, mensajero.getString(2),
                        mensajero.getString(3),mensajero.getString(4));
            }

        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return citVO;
    }
      public static CitaVO citaFactura(String idCita) {
        CitaVO citVO = null;

        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT * FROM vwcitafactura WHERE idCita=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, idCita);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                citVO = new CitaVO(mensajero.getString(1),mensajero.getBinaryStream(2),
                                    mensajero.getString(3),mensajero.getString(4),
                                    mensajero.getString(5),mensajero.getString(6),
                                    mensajero.getString(7),mensajero.getString(8), 
                                    idCita,mensajero.getString(10));

            }

        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class
                    .getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();

            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class
                        .getName()).log(Level.SEVERE, null, e);
            }
        }
        return citVO;
     }
      public static CitaVO consultarCitaEmpleado(String idCita) {
        CitaVO citVO = null;

        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT * FROM vwinfocitaempleado WHERE idCita=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, idCita);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                citVO = new CitaVO(mensajero.getString(2), idCita,
                        mensajero.getString(3),mensajero.getString(4),
                        mensajero.getString(5),mensajero.getString(6),
                        mensajero.getString(7),mensajero.getBinaryStream(8),
                        mensajero.getString(9),mensajero.getString(10),
                        mensajero.getString(11),mensajero.getString(12),
                        mensajero.getString(13),mensajero.getString(14),
                mensajero.getString(15));
            }

        } catch (SQLException e) {
            Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return citVO;
    }
      
      
}
