/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modeloVO.AgendaVO;
import util.Conexion;
import util.Crud;

/**
 *
 * @author usuario
 */
public class AgendaDAO extends Conexion implements Crud {
    //1.Declarar objetos y variables

    private Connection conexion = null;
    private PreparedStatement puente = null;
    private ResultSet mensajero = null;

    //1.1.Declarar variable para cualquier metodo
    public boolean operaciones = false;
    public String sql;

    public String idAgenda = "", ageFechaInicio = "", ageFechaFinal = "",
            empleado_idEmpleado = "";

    public AgendaDAO() {
    }

    public AgendaDAO(AgendaVO ageVO) {

        super();

        try {

            conexion = this.obtenerConexion();
            idAgenda = ageVO.getIdAgenda();
            ageFechaInicio = ageVO.getAgeFechaInicio();
            ageFechaFinal = ageVO.getAgeFechaFinal();
            empleado_idEmpleado = ageVO.getEmpleado_idEmpleado();

        } catch (Exception e) {
            Logger.getLogger(AgendaDAO.class.getName()).log(Level.SEVERE, null, e);

        }

    }

    @Override
    public boolean agregarRegistro() {
        try {
            sql = "CALL PAagendaEmpleado(?,?,?)";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, ageFechaInicio);
            puente.setString(2, ageFechaFinal);
            puente.setString(3, empleado_idEmpleado);
            puente.executeUpdate();
            operaciones = true;

        } catch (SQLException e) {
            Logger.getLogger(AgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(AgendaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }

    @Override
    public boolean actualizarRegistro() {
        try {
            sql = "CALL PAupdateAgenda(?,?)";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, ageFechaFinal);
            puente.setString(2, idAgenda);
            puente.executeUpdate();
            operaciones = true;

        } catch (SQLException e) {
            Logger.getLogger(AgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(AgendaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }

    @Override
    public boolean borrarRegistro() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static AgendaVO consultarIdAgenda(String empleado_idEmpleado) {
        AgendaVO ageVO = null;

        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT idAgenda FROM agenda WHERE empleado_idEmpleado=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, empleado_idEmpleado);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                ageVO = new AgendaVO(mensajero.getString(1));
            }

        } catch (SQLException e) {
            Logger.getLogger(AgendaDAO.class
                    .getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();

            } catch (SQLException e) {
                Logger.getLogger(AgendaDAO.class
                        .getName()).log(Level.SEVERE, null, e);
            }
        }
        return ageVO;
    }

    public boolean estadoEmpAge() {
        try {
            sql = "CALL PAEmpleadaEstado(?)";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idAgenda);
            puente.executeUpdate();
            mensajero = puente.executeQuery();
            mensajero.last();
            if (mensajero.getRow() != 0) {
                operaciones = true;
                puente.executeUpdate();
            } else {
                return operaciones;
            }

        } catch (SQLException e) {
            Logger.getLogger(AgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(AgendaDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }

    public ArrayList<AgendaVO> listaAgenda(String empleado_idEmpleado) {

        AgendaVO ageVO = null;
        conexion = this.obtenerConexion();
        ArrayList<AgendaVO> listarAgenda = new ArrayList<>();
        try {
            sql = "SELECT * FROM agenda WHERE empleado_idEmpleado=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, empleado_idEmpleado);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                ageVO = new AgendaVO(mensajero.getString(1),
                        mensajero.getString(2), mensajero.getString(3),
                        empleado_idEmpleado);
                listarAgenda.add(ageVO);

            }
        } catch (Exception e) {
            Logger.getLogger(ProAgendaDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listarAgenda;
    }

    public static AgendaVO agendaEmpleado(String idAgenda) {
        AgendaVO ageVO = null;

        try {
            Conexion conexBd = new Conexion();
            Connection conexion = conexBd.obtenerConexion();
            String sql = "SELECT * FROM agenda WHERE idAgenda=?";
            PreparedStatement puente = conexion.prepareStatement(sql);
            puente.setString(1, idAgenda);
            ResultSet mensajero = puente.executeQuery();
            if (mensajero.next()) {
                ageVO = new AgendaVO(idAgenda, mensajero.getString(2),
                        mensajero.getString(3), mensajero.getString(4));
            }

        } catch (SQLException e) {
            Logger.getLogger(AgendaDAO.class
                    .getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                Conexion conexBd = new Conexion();
                conexBd.cerrarConexion();

            } catch (SQLException e) {
                Logger.getLogger(AgendaDAO.class
                        .getName()).log(Level.SEVERE, null, e);
            }
        }
        return ageVO;
    }
}
