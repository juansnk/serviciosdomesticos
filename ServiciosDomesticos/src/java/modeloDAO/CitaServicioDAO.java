/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modeloVO.CitaServicioVO;
import util.Conexion;
import util.Crud;

/**
 *
 * @author Admin
 */
public class CitaServicioDAO extends Conexion implements Crud {
    //1.Declarar objetos y variables

    private Connection conexion = null;
    private PreparedStatement puente = null;
    private ResultSet mensajero = null;

    //1.1.Declarar variable para cualquier metodo
    public boolean operaciones = false;
    public String sql;

    //1.2.Declarar las variables del VO
    public String idCita="", idServicio[];
    public String cisCosto = "";

    public CitaServicioDAO() {
    }

    //2.
    public CitaServicioDAO(CitaServicioVO cisVO) {
        super();

        try {
            conexion = this.obtenerConexion();
            idCita = cisVO.getIdCita();
            idServicio = cisVO.getIdServicio();
            cisCosto = cisVO.getCisCosto();
        } catch (Exception e) {
            Logger.getLogger(CitaServicioDAO.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    @Override
    public boolean agregarRegistro() {
        try {
            sql = "CALL PAcalcularPrecioCita (?,?)";
                puente = conexion.prepareStatement(sql);            
            for (int i = 0; i < idServicio.length; i++) {
                puente.setString(1, idCita);
                puente.setString(2, idServicio[i]);
                puente.addBatch();               
            }
                puente.executeBatch();
                operaciones = true;
        } catch (SQLException e) {
            Logger.getLogger(CitaServicioDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                this.cerrarConexion();
            } catch (SQLException e) {
                Logger.getLogger(CitaServicioDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return operaciones;
    }

    @Override
    public boolean actualizarRegistro() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean borrarRegistro() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     public ArrayList<CitaServicioVO> listarServiciosCitas(String idCita) {

        CitaServicioVO cisVO = null;
        conexion = this.obtenerConexion();
        ArrayList<CitaServicioVO> listaServicios = new ArrayList<>();
        try {
            sql = "SELECT * FROM vwservicios_cita WHERE idCita=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idCita);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                cisVO = new CitaServicioVO(idCita,
                        mensajero.getString(2), mensajero.getString(3));
                listaServicios.add(cisVO);

            }
        } catch (Exception e) {
            Logger.getLogger(CitaServicioDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listaServicios;
    }   
     public ArrayList<CitaServicioVO> listarServiciosFactura(String idCita) {

        CitaServicioVO cisVO = null;
        conexion = this.obtenerConexion();
        ArrayList<CitaServicioVO> listaServiciosFac = new ArrayList<>();
        try {
            sql = "SELECT * FROM vwservicioscitafactura WHERE idCita=?";
            puente = conexion.prepareStatement(sql);
            puente.setString(1, idCita);
            mensajero = puente.executeQuery();

            while (mensajero.next()) {
                cisVO = new CitaServicioVO(idCita,
                        mensajero.getString(2), mensajero.getString(3),
                        mensajero.getString(4));
                listaServiciosFac.add(cisVO);

            }
        } catch (Exception e) {
            Logger.getLogger(CitaServicioDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return listaServiciosFac;
    }  
}
