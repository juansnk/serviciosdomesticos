/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Correos;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import util.Conexion;

/**
 *
 * @author Admin
 */
public class ConfirmacionCuenta extends Conexion {

    public void mandarCorreo(String usuCorreo, int tipoCorreo, String idUsuario) {
        // El correo gmail de envío
        String correoEnvia = "limpiezaservihogar1@gmail.com";
        String claveCorreo = "ussggmuubejfgkda";

        // La configuración para enviar correo
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.user", correoEnvia);
        properties.put("mail.password", claveCorreo);

        // Obtener la sesion
        Session session = Session.getInstance(properties, null);

        try {
            // Crear el cuerpo del mensaje
            MimeMessage mimeMessage = new MimeMessage(session);

            // Agregar quien envía el correo
            mimeMessage.setFrom(new InternetAddress(correoEnvia, "ServiHogar"));

            // Los destinatarios
            InternetAddress[] internetAddresses = {
                new InternetAddress(usuCorreo)};

            // Agregar los destinatarios al mensaje
            mimeMessage.setRecipients(Message.RecipientType.TO,
                    internetAddresses);

            if (tipoCorreo == 1) {
                //Confirmac registro de cuenta
                // Agregar el asunto al correo
                mimeMessage.setSubject("ServiHogar - confirmación de cuenta");

                // Crear el multipart para agregar la parte del mensaje anterior
                Multipart multipart = new MimeMultipart();
                // Leer la plantilla
                InputStream inputStream = getClass().getResourceAsStream(
                        "/../../../web/correos/confirmacionCuenta.jsp");
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(inputStream));

                // Almacenar el contenido de la plantilla en un StringBuffer
                String strLine;
                StringBuffer msjHTML = new StringBuffer();
                while ((strLine = bufferedReader.readLine()) != null) {
                    msjHTML.append(strLine);
                }
                // Url del directorio donde estan las imagenes
                String urlImagenes = "url del directorio donde estan las imagenes";
                File directorio = new File(urlImagenes);

                // Obtener los nombres de las imagenes en el directorio
                String[] imagenesDirectorio = directorio.list();

                // Creo la parte del mensaje HTML
                MimeBodyPart mimeBodyPart = new MimeBodyPart();
                mimeBodyPart.setContent(msjHTML.toString(), "text/html;charset=UTF-8");
                // Validar que el directorio si tenga las imagenes
                if (imagenesDirectorio != null) {
                    for (int count = 0; count < imagenesDirectorio.length; count++) {

                        MimeBodyPart imagePart = new MimeBodyPart();
                        imagePart.setHeader("Content-ID", "<"
                                + imagenesDirectorio[count] + ">");
                        imagePart.setDisposition(MimeBodyPart.INLINE);
                        imagePart.attachFile(urlImagenes
                                + imagenesDirectorio[count]);
                        multipart.addBodyPart(imagePart);
                    }
                } else {
                    System.out.println("No hay imagenes en el directorio");
                }

                // Agregar la parte del mensaje HTML al multiPart
                multipart.addBodyPart(mimeBodyPart);
                // Agregar el multipart al cuerpo del mensaje
                mimeMessage.setContent(multipart);

            } else if (tipoCorreo == 2) {
                //Enviar aviso de nueva cita a empleada
                // Agregar el asunto al correo
                mimeMessage.setSubject("ServiHogar - tienes una nueva cita");

                // Crear el multipart para agregar la parte del mensaje anterior
                Multipart multipart = new MimeMultipart();
                // Leer la plantilla
                InputStream inputStream = getClass().getResourceAsStream(
                        "/../../../web/correos/anuncioCita.jsp");
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(inputStream));

                // Almacenar el contenido de la plantilla en un StringBuffer
                String strLine;
                StringBuffer msjHTML = new StringBuffer();
                while ((strLine = bufferedReader.readLine()) != null) {
                    msjHTML.append(strLine);
                }
                // Url del directorio donde estan las imagenes
                String urlImagenes = "url del directorio donde estan las imagenes";
                File directorio = new File(urlImagenes);

                // Obtener los nombres de las imagenes en el directorio
                String[] imagenesDirectorio = directorio.list();

                // Creo la parte del mensaje HTML
                MimeBodyPart mimeBodyPart = new MimeBodyPart();
                mimeBodyPart.setContent(msjHTML.toString(), "text/html;charset=UTF-8");
                // Validar que el directorio si tenga las imagenes
                if (imagenesDirectorio != null) {
                    for (int count = 0; count < imagenesDirectorio.length; count++) {

                        MimeBodyPart imagePart = new MimeBodyPart();
                        imagePart.setHeader("Content-ID", "<"
                                + imagenesDirectorio[count] + ">");
                        imagePart.setDisposition(MimeBodyPart.INLINE);
                        imagePart.attachFile(urlImagenes
                                + imagenesDirectorio[count]);
                        multipart.addBodyPart(imagePart);
                    }
                } else {
                    System.out.println("No hay imagenes en el directorio");
                }

                // Agregar la parte del mensaje HTML al multiPart
                multipart.addBodyPart(mimeBodyPart);
                // Agregar el multipart al cuerpo del mensaje
                mimeMessage.setContent(multipart);

            } else if (tipoCorreo == 3) {
                //Enviar aviso de cambio de contraseña
                // Agregar el asunto al correo
                mimeMessage.setSubject("ServiHogar - solicitud de cambio de contraseña");

                // Crear el multipart para agregar la parte del mensaje anterior
                Multipart multipart = new MimeMultipart();
                // Leer la plantilla
                InputStream inputStream = getClass().getResourceAsStream(
                        "/../../../web/correos/cambioContraseña.jsp");
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(inputStream));

                // Almacenar el contenido de la plantilla en un StringBuffer
                String strLine;
                StringBuffer msjHTML = new StringBuffer();
                while ((strLine = bufferedReader.readLine()) != null) {
                    msjHTML.append(strLine.replace("{{id}}", idUsuario));
                }
                // Url del directorio donde estan las imagenes
                String urlImagenes = "url del directorio donde estan las imagenes";
                File directorio = new File(urlImagenes);

                // Obtener los nombres de las imagenes en el directorio
                String[] imagenesDirectorio = directorio.list();

                // Creo la parte del mensaje HTML
                MimeBodyPart mimeBodyPart = new MimeBodyPart();
                mimeBodyPart.setContent(msjHTML.toString(), "text/html;charset=UTF-8");
                // Validar que el directorio si tenga las imagenes
                if (imagenesDirectorio != null) {
                    for (int count = 0; count < imagenesDirectorio.length; count++) {

                        MimeBodyPart imagePart = new MimeBodyPart();
                        imagePart.setHeader("Content-ID", "<"
                                + imagenesDirectorio[count] + ">");
                        imagePart.setDisposition(MimeBodyPart.INLINE);
                        imagePart.attachFile(urlImagenes
                                + imagenesDirectorio[count]);
                        multipart.addBodyPart(imagePart);
                    }
                } else {
                    System.out.println("No hay imagenes en el directorio");
                }

                // Agregar la parte del mensaje HTML al multiPart
                multipart.addBodyPart(mimeBodyPart);
                // Agregar el multipart al cuerpo del mensaje
                mimeMessage.setContent(multipart);

            } else if (tipoCorreo == 4) {
                //Enviar aviso de cancelacion de cita
                // Agregar el asunto al correo
                mimeMessage.setSubject("ServiHogar - Cancelacion de cita");

                // Crear el multipart para agregar la parte del mensaje anterior
                Multipart multipart = new MimeMultipart();
                // Leer la plantilla
                InputStream inputStream = getClass().getResourceAsStream(
                        "/../../../web/correos/cancelacionCitaEmpleada.jsp");
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(inputStream));

                // Almacenar el contenido de la plantilla en un StringBuffer
                String strLine;
                StringBuffer msjHTML = new StringBuffer();
                while ((strLine = bufferedReader.readLine()) != null) {
                    msjHTML.append(strLine);
                }
                // Url del directorio donde estan las imagenes
                String urlImagenes = "url del directorio donde estan las imagenes";
                File directorio = new File(urlImagenes);

                // Obtener los nombres de las imagenes en el directorio
                String[] imagenesDirectorio = directorio.list();

                // Creo la parte del mensaje HTML
                MimeBodyPart mimeBodyPart = new MimeBodyPart();
                mimeBodyPart.setContent(msjHTML.toString(), "text/html;charset=UTF-8");
                // Validar que el directorio si tenga las imagenes
                if (imagenesDirectorio != null) {
                    for (int count = 0; count < imagenesDirectorio.length; count++) {

                        MimeBodyPart imagePart = new MimeBodyPart();
                        imagePart.setHeader("Content-ID", "<"
                                + imagenesDirectorio[count] + ">");
                        imagePart.setDisposition(MimeBodyPart.INLINE);
                        imagePart.attachFile(urlImagenes
                                + imagenesDirectorio[count]);
                        multipart.addBodyPart(imagePart);
                    }
                } else {
                    System.out.println("No hay imagenes en el directorio");
                }

                // Agregar la parte del mensaje HTML al multiPart
                multipart.addBodyPart(mimeBodyPart);
                // Agregar el multipart al cuerpo del mensaje
                mimeMessage.setContent(multipart);

            }

            // Enviar el mensaje
            Transport transport = session.getTransport("smtp");
            transport.connect(correoEnvia, claveCorreo);
            transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
            transport.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println("Correo enviado");
    }

    public static void main(String[] args) {
        ConfirmacionCuenta correoHTML = new ConfirmacionCuenta();
        correoHTML.mandarCorreo(null, null, null);

    }
}
