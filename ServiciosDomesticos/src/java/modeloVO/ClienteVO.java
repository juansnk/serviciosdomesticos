/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloVO;

/**
 *
 * @author Admin
 */
public class ClienteVO {
    private String idCliente, cliDireccion, cliLocalidad, cliBarrio, usuario_idUsuario;

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliDireccion() {
        return cliDireccion;
    }

    public void setCliDireccion(String cliDireccion) {
        this.cliDireccion = cliDireccion;
    }

    public String getCliLocalidad() {
        return cliLocalidad;
    }

    public void setCliLocalidad(String cliLocalidad) {
        this.cliLocalidad = cliLocalidad;
    }

    public String getCliBarrio() {
        return cliBarrio;
    }

    public void setCliBarrio(String cliBarrio) {
        this.cliBarrio = cliBarrio;
    }

    public String getUsuario_idUsuario() {
        return usuario_idUsuario;
    }

    public void setUsuario_idUsuario(String usuario_idUsuario) {
        this.usuario_idUsuario = usuario_idUsuario;
    }

    public ClienteVO(String idCliente, String cliDireccion, String cliLocalidad, String cliBarrio, String usuario_idUsuario) {
        this.idCliente = idCliente;
        this.cliDireccion = cliDireccion;
        this.cliLocalidad = cliLocalidad;
        this.cliBarrio = cliBarrio;
        this.usuario_idUsuario = usuario_idUsuario;
    }
    
}
