/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloVO;

/**
 *
 * @author Admin
 */
public class ProAgendaVO {
    private String idProgramacionAgenda, proDia, proHoraInicio, proHoraFin, agenda_idAgenda, idAgenda, empleado_idEmpleado;

    public String getIdProgramacionAgenda() {
        return idProgramacionAgenda;
    }

    public void setIdProgramacionAgenda(String idProgramacionAgenda) {
        this.idProgramacionAgenda = idProgramacionAgenda;
    }

    public String getProDia() {
        return proDia;
    }

    public void setProDia(String proDia) {
        this.proDia = proDia;
    }

    public String getProHoraInicio() {
        return proHoraInicio;
    }

    public void setProHoraInicio(String proHoraInicio) {
        this.proHoraInicio = proHoraInicio;
    }

    public String getProHoraFin() {
        return proHoraFin;
    }

    public void setProHoraFin(String proHoraFin) {
        this.proHoraFin = proHoraFin;
    }

    public String getAgenda_idAgenda() {
        return agenda_idAgenda;
    }

    public void setAgenda_idAgenda(String agenda_idAgenda) {
        this.agenda_idAgenda = agenda_idAgenda;
    }

    public String getIdAgenda() {
        return idAgenda;
    }

    public void setIdAgenda(String idAgenda) {
        this.idAgenda = idAgenda;
    }

    public String getEmpleado_idEmpleado() {
        return empleado_idEmpleado;
    }

    public void setEmpleado_idEmpleado(String empleado_idEmpleado) {
        this.empleado_idEmpleado = empleado_idEmpleado;
    }
    
    

    public ProAgendaVO(String idProgramacionAgenda, String proDia, String proHoraInicio, String proHoraFin, String agenda_idAgenda) {
        this.idProgramacionAgenda = idProgramacionAgenda;
        this.proDia = proDia;
        this.proHoraInicio = proHoraInicio;
        this.proHoraFin = proHoraFin;
        this.idAgenda = agenda_idAgenda;
    }

    public ProAgendaVO(String idProgramacionAgenda, String proDia, String proHoraInicio, String proHoraFin, String agenda_idAgenda, String idAgenda, String empleado_idEmpleado) {
        this.idProgramacionAgenda = idProgramacionAgenda;
        this.proDia = proDia;
        this.proHoraInicio = proHoraInicio;
        this.proHoraFin = proHoraFin;
        this.agenda_idAgenda = agenda_idAgenda;
        this.idAgenda = idAgenda;
        this.empleado_idEmpleado = empleado_idEmpleado;
    }
    

    public ProAgendaVO(String agenda_idAgenda) {
        this.agenda_idAgenda = agenda_idAgenda;
    }
    

    public ProAgendaVO() {
    }
    
            
}
