/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloVO;

import java.io.InputStream;

/**
 *
 * @author Admin
 */
public class CitaVO{
    private String idCita, citFecha, citDireccion, citHoraInicio, citHoraFin, citEstado, citPrecio, citCalificacion, citOpinion,
            cliente_idCliente, agenda_idProgramacion, agenda_idAgenda, idEmpleado, empPrecio,idUsuario,
            usuNombre, usuApellido, usuCiudad, usuCorreo, proDia, idCliente, usuTelefono;
    private InputStream usuFoto;

    public String getIdCita() {
        return idCita;
    }

    public void setIdCita(String idCita) {
        this.idCita = idCita;
    }

    public String getCitFecha() {
        return citFecha;
    }

    public void setCitFecha(String citFecha) {
        this.citFecha = citFecha;
    }

    public String getCitDireccion() {
        return citDireccion;
    }

    public void setCitDireccion(String citDireccion) {
        this.citDireccion = citDireccion;
    }

    public String getCitHoraInicio() {
        return citHoraInicio;
    }

    public void setCitHoraInicio(String citHoraInicio) {
        this.citHoraInicio = citHoraInicio;
    }

    public String getCitHoraFin() {
        return citHoraFin;
    }

    public void setCitHoraFin(String citHoraFin) {
        this.citHoraFin = citHoraFin;
    }

    public String getCitEstado() {
        return citEstado;
    }

    public String getCitPrecio() {
        return citPrecio;
    }

    public void setCitPrecio(String citPrecio) {
        this.citPrecio = citPrecio;
    }
    

    public void setCitEstado(String citEstado) {
        this.citEstado = citEstado;
    }
    public String getCitCalificacion() {
        return citCalificacion;
    }

    public void setCitCalificacion(String citCalificacion) {
        this.citCalificacion = citCalificacion;
    }

    public String getCitOpinion() {
        return citOpinion;
    }

    public void setCitOpinion(String citOpinion) {
        this.citOpinion = citOpinion;
    }
    

    public String getCliente_idCliente() {
        return cliente_idCliente;
    }

    public void setCliente_idCliente(String cliente_idCliente) {
        this.cliente_idCliente = cliente_idCliente;
    }

    public String getAgenda_idProgramacion() {
        return agenda_idProgramacion;
    }

    public void setAgenda_idProgramacion(String agenda_idProgramacion) {
        this.agenda_idProgramacion = agenda_idProgramacion;
    }

    public String getAgenda_idAgenda() {
        return agenda_idAgenda;
    }

    public void setAgenda_idAgenda(String agenda_idAgenda) {
        this.agenda_idAgenda = agenda_idAgenda;
    }

    public String getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(String idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public InputStream getUsuFoto() {
        return usuFoto;
    }

    public void setUsuFoto(InputStream usuFoto) {
        this.usuFoto = usuFoto;
    }


    public String getEmpPrecio() {
        return empPrecio;
    }

    public void setEmpPrecio(String empPrecio) {
        this.empPrecio = empPrecio;
    }
    
    
    public String getUsuNombre() {
        return usuNombre;
    }

    public void setUsuNombre(String usuNombre) {
        this.usuNombre = usuNombre;
    }

    public String getUsuApellido() {
        return usuApellido;
    }

    public void setUsuApellido(String usuApellido) {
        this.usuApellido = usuApellido;
    }

    public String getUsuCiudad() {
        return usuCiudad;
    }

    public void setUsuCiudad(String usuCiudad) {
        this.usuCiudad = usuCiudad;
    }

    public String getUsuCorreo() {
        return usuCorreo;
    }

    public void setUsuCorreo(String usuCorreo) {
        this.usuCorreo = usuCorreo;
    }

    
    public String getProDia() {
        return proDia;
    }

   

    public void setProDia(String proDia) {
        this.proDia = proDia;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getUsuTelefono() {
        return usuTelefono;
    }

    public void setUsuTelefono(String usuTelefono) {
        this.usuTelefono = usuTelefono;
    }

  
    
    //11
    public CitaVO(String idCita, String citFecha, String citDireccion, String citHoraInicio, String citHoraFin, String citCalificacion, String citOpinion, String citEstado, String cliente_idCliente, String agenda_idProgramacion,String agenda_idAgenda) {
        this.idCita = idCita;
        this.citFecha = citFecha;
        this.citDireccion = citDireccion;
        this.citHoraInicio = citHoraInicio;
        this.citHoraFin = citHoraFin;
        this.citCalificacion = citCalificacion;
        this.citOpinion = citOpinion;
        this.citEstado = citEstado;
        this.cliente_idCliente = cliente_idCliente;
        this.agenda_idProgramacion = agenda_idProgramacion;
        this.agenda_idAgenda = agenda_idAgenda;
    }
    //13
    public CitaVO(String idCita, String idUsuario, InputStream usuFoto, String usuNombre, String usuApellido, String usuCiudad, String citFecha, String proDia, String citDireccion, String citHoraInicio, String citHoraFin, String citEstado, String cliente_idCliente) {
        this.idCita = idCita;
        this.idUsuario = idUsuario;
        this.usuFoto = usuFoto;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.usuCiudad = usuCiudad;
        this.citFecha = citFecha;
        this.proDia = proDia;
        this.citDireccion = citDireccion;
        this.citHoraInicio = citHoraInicio;
        this.citHoraFin = citHoraFin;
        this.citEstado = citEstado;
        this.cliente_idCliente = cliente_idCliente;
    }
    //14
    public CitaVO(String idUsuario, String idCita, InputStream usuFoto, String usuNombre, String usuApellido, String usuCiudad, String citFecha, String proDia, String citDireccion, String citHoraInicio, String citHoraFin, String citEstado, String citCalificacion, String cliente_idCliente) {
        this.idUsuario = idUsuario;
        this.idCita = idCita;
        this.usuFoto = usuFoto;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.usuCiudad = usuCiudad;
        this.citFecha = citFecha;
        this.proDia = proDia;
        this.citDireccion = citDireccion;
        this.citHoraInicio = citHoraInicio;
        this.citHoraFin = citHoraFin;
        this.citEstado = citEstado;
        this.citCalificacion = citCalificacion;
        this.cliente_idCliente = cliente_idCliente;
    }
    //12
    public CitaVO(String idCita, String idUsuario, String citFecha, String citDireccion, String citHoraInicio, String citHoraFin,InputStream usuFoto, String usuNombre, String usuApellido, String usuTelefono, String usuCorreo, String citPrecio ) {
        this.idCita = idCita;
        this.idUsuario = idUsuario;
        this.citFecha = citFecha;
        this.citDireccion = citDireccion;
        this.citHoraInicio = citHoraInicio;
        this.citHoraFin = citHoraFin;
        this.usuFoto = usuFoto;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.usuTelefono = usuTelefono;
        this.usuCorreo = usuCorreo;
        this.citPrecio = citPrecio;
    }
    
    
    public CitaVO(String idCita, String citHoraInicio, String citHoraFin) {
        this.idCita = idCita;
        this.citHoraInicio = citHoraInicio;
        this.citHoraFin = citHoraFin;
    }
    //9
    public CitaVO(String idCita, String citFecha, String citDireccion, String citHoraInicio, String citHoraFin, String idEmpleado, String usuNombre, String usuApellido, String idCliente) {
        this.idCita = idCita;
        this.citFecha = citFecha;
        this.citDireccion = citDireccion;
        this.citHoraInicio = citHoraInicio;
        this.citHoraFin = citHoraFin;
        this.idEmpleado = idEmpleado;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.idCliente = idCliente;
    }
    //16
    public CitaVO(String idCita, String idEmpleado,InputStream usuFoto, String usuNombre, String usuApellido, String usuCiudad,String usuTelefono, String citFecha, String proDia, String citDireccion, String citHoraInicio, String citHoraFin, String citPrecio, String citEstado, String cliente_idCliente, String agenda_idProgramacion) {
        this.idCita = idCita;
        this.idEmpleado = idEmpleado;
        this.usuFoto = usuFoto;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.usuCiudad = usuCiudad;
        this.usuTelefono = usuTelefono;
        this.citFecha = citFecha;
        this.proDia = proDia;
        this.citDireccion = citDireccion;
        this.citHoraInicio = citHoraInicio;
        this.citHoraFin = citHoraFin;
        this.citPrecio = citPrecio;
        this.citEstado = citEstado;
        this.cliente_idCliente = cliente_idCliente;
        this.agenda_idProgramacion = agenda_idProgramacion;
    }
    //17
    public CitaVO(String idEmpleado, String idCita, InputStream usuFoto, String usuNombre, String usuApellido, String usuCiudad, String usuTelefono, String usuCorreo, String citFecha, String proDia, String citDireccion, String citHoraInicio, String citHoraFin, String citEstado, String agenda_idProgramacion, String citPrecio,String cliente_idCliente) { 
        this.idEmpleado = idEmpleado; 
        this.idCita = idCita; 
        this.usuFoto = usuFoto;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.usuCiudad = usuCiudad;
        this.usuTelefono = usuTelefono;
        this.usuCorreo = usuCorreo;
        this.citFecha = citFecha;
        this.proDia = proDia;
        this.citDireccion = citDireccion;
        this.citHoraInicio = citHoraInicio;
        this.citHoraFin = citHoraFin;
        this.citEstado = citEstado;
        this.agenda_idProgramacion = agenda_idProgramacion;
        this.citPrecio = citPrecio;
        this.cliente_idCliente = cliente_idCliente;    
    }
    //10
    public CitaVO(String idUsuario, InputStream usuFoto, String usuNombre, String usuApellido, String usuCorreo, String usuTelefono, String citEstado,String empPrecio, String idCita, String citPrecio) {
        this.idUsuario = idUsuario;
        this.usuFoto = usuFoto;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.usuCorreo = usuCorreo;
        this.usuTelefono = usuTelefono;
        this.citEstado = citEstado;
        this.empPrecio = empPrecio;
        this.idCita = idCita;
        this.citPrecio = citPrecio;
  
    }
    //15

    public CitaVO(String idEmpleado, String idCita, String citFecha, String citDireccion, String citHoraInicio, String citHoraFin, String idCliente, InputStream usuFoto, String usuNombre, String usuApellido, String citPrecio, String citEstado, String usuCiudad, String usuTelefono, String usuCorreo) {
        this.idEmpleado = idEmpleado;
        this.idCita = idCita;      
        this.citFecha = citFecha;
        this.citDireccion = citDireccion;
        this.citHoraInicio = citHoraInicio;
        this.citHoraFin = citHoraFin;
        this.idCliente = idCliente;
        this.usuFoto = usuFoto;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.citPrecio = citPrecio;
        this.citEstado = citEstado;
        this.usuCiudad = usuCiudad;
        this.usuTelefono = usuTelefono;
        this.usuCorreo = usuCorreo;
    }

    public CitaVO(String idCita, String citCalificacion, String citOpinion, String cliente_idCliente) {
        this.idCita = idCita;
        this.citCalificacion = citCalificacion;
        this.citOpinion = citOpinion;
        this.cliente_idCliente =  cliente_idCliente;
    }

    public CitaVO(String idUsuario, InputStream usuFoto) {
        this.idUsuario = idUsuario;
        this.usuFoto = usuFoto;
    }
    
    
    
    public CitaVO(String idCita) {
        this.idCita = idCita;
    }
        

    public CitaVO() {
    }
    
    
}
