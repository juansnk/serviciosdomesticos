/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloVO;

/**
 *
 * @author Admin
 */
public class CategoriaVO {
    private String idCategoria, catNombre;

    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCatNombre() {
        return catNombre;
    }

    public void setCatNombre(String catNombre) {
        this.catNombre = catNombre;
    }

    public CategoriaVO(String idCategoria, String catNombre) {
        this.idCategoria = idCategoria;
        this.catNombre = catNombre;
    }

    public CategoriaVO() {
    }
