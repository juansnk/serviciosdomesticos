/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloVO;

import java.io.InputStream;

/**
 *
 * @author Admin
 */
public class EmpleadoVO {
    private String idEmpleado, empPromedio, empExperiencia, empDescripcion,
            usuario_idUsuario, idCita, citFecha, citDireccion, citHoraInicio,
            citHoraFin, idCliente,idUsuario, usuNombre, usuApellido, citEstado, usuCorreo, citOpinion, citPrecio, citCalificacion;
    private InputStream usuFoto;

    public String getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(String idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getEmpPromedio() {
        return empPromedio;
    }

    public void setEmpPromedio(String empPromedio) {
        this.empPromedio = empPromedio;
    }

    public String getEmpExperiencia() {
        return empExperiencia;
    }

    public void setEmpExperiencia(String empExperiencia) {
        this.empExperiencia = empExperiencia;
    }

    public String getEmpDescripcion() {
        return empDescripcion;
    }

    public void setEmpDescripcion(String empDescripcion) {
        this.empDescripcion = empDescripcion;
    }

    public String getUsuario_idUsuario() {
        return usuario_idUsuario;
    }

    public void setUsuario_idUsuario(String usuario_idUsuario) {
        this.usuario_idUsuario = usuario_idUsuario;
    }

    public String getIdCita() {
        return idCita;
    }

    public void setIdCita(String idCita) {
        this.idCita = idCita;
    }

    public String getCitFecha() {
        return citFecha;
    }

    public void setCitFecha(String citFecha) {
        this.citFecha = citFecha;
    }

    public String getCitDireccion() {
        return citDireccion;
    }

    public void setCitDireccion(String citDireccion) {
        this.citDireccion = citDireccion;
    }

    public String getCitHoraInicio() {
        return citHoraInicio;
    }

    public void setCitHoraInicio(String citHoraInicio) {
        this.citHoraInicio = citHoraInicio;
    }

    public String getCitHoraFin() {
        return citHoraFin;
    }

    public void setCitHoraFin(String citHoraFin) {
        this.citHoraFin = citHoraFin;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuNombre() {
        return usuNombre;
    }

    public void setUsuNombre(String usuNombre) {
        this.usuNombre = usuNombre;
    }

    public String getUsuApellido() {
        return usuApellido;
    }

    public void setUsuApellido(String usuApellido) {
        this.usuApellido = usuApellido;
    }

    public String getCitEstado() {
        return citEstado;
    }

    public void setCitEstado(String citEstado) {
        this.citEstado = citEstado;
    }

    public String getUsuCorreo() {
        return usuCorreo;
    }

    public void setUsuCorreo(String usuCorreo) {
        this.usuCorreo = usuCorreo;
    }
    

    public String getCitOpinion() {
        return citOpinion;
    }

    public void setCitOpinion(String citOpinion) {
        this.citOpinion = citOpinion;
    }

    public String getCitPrecio() {
        return citPrecio;
    }

    public void setCitPrecio(String citPrecio) {
        this.citPrecio = citPrecio;
    }

    public String getCitCalificacion() {
        return citCalificacion;
    }

    public void setCitCalificacion(String citCalificacion) {
        this.citCalificacion = citCalificacion;
    }

    public InputStream getUsuFoto() {
        return usuFoto;
    }

    public void setUsuFoto(InputStream usuFoto) {
        this.usuFoto = usuFoto;
    }
    
    //5
    public EmpleadoVO(String idEmpleado, String empPromedio, String empExperiencia, String empDescripcion, String usuario_idUsuario) {
        this.idEmpleado = idEmpleado;
        this.empPromedio = empPromedio;
        this.empExperiencia = empExperiencia;
        this.empDescripcion = empDescripcion;
        this.usuario_idUsuario = usuario_idUsuario;
    }
    //10
    public EmpleadoVO(String idEmpleado, String idCita, String citFecha, String citDireccion, String citHoraInicio, String citHoraFin, String idCliente, String usuNombre, String usuApellido, String citPrecio) {
        this.idEmpleado = idEmpleado;
        this.idCita = idCita;
        this.citFecha = citFecha;
        this.citDireccion = citDireccion;
        this.citHoraInicio = citHoraInicio;
        this.citHoraFin = citHoraFin;
        this.idCliente = idCliente;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.citPrecio = citPrecio;
    }
    //9
    public EmpleadoVO(String idEmpleado, String idUsuario, String idCita,InputStream usuFoto,String usuNombre, String usuApellido, String citEstado, String citOpinion, String citFecha) {
        this.idEmpleado = idEmpleado;
        this.idUsuario = idUsuario;
        this.idCita = idCita;
        this.usuFoto = usuFoto;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.citEstado = citEstado;
        this.citOpinion = citOpinion;
        this.citFecha = citFecha;
    }
    //7
    public EmpleadoVO(String idEmpleado, String idCita, String usuNombre, String usuApellido, String citFecha, String citHoraInicio, String usuCorreo) {
        this.idEmpleado = idEmpleado;
        this.idCita = idCita;
         this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.citFecha = citFecha;
        this.citHoraInicio = citHoraInicio;
        this.usuCorreo = usuCorreo;
    }
    //13
    public EmpleadoVO(String idEmpleado, String idCita, String citFecha, String citDireccion, String citHoraInicio, String citHoraFin, String idCliente, String idUsuario, InputStream usuFoto,String usuNombre, String usuApellido, String usuCorreo, String citPrecio) {
        this.idEmpleado = idEmpleado;
        this.idCita = idCita;
        this.citFecha = citFecha;
        this.citDireccion = citDireccion;
        this.citHoraInicio = citHoraInicio;
        this.citHoraFin = citHoraFin;
        this.idCliente = idCliente;
        this.idUsuario = idUsuario;
        this.usuFoto = usuFoto;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.usuCorreo = usuCorreo;
        this.citPrecio = citPrecio;
    }
    //14

    public EmpleadoVO(String idEmpleado, String idCita, String citFecha, String citDireccion, String citHoraInicio, String citHoraFin, String idCliente, String idUsuario, InputStream usuFoto,String usuNombre, String usuApellido, String citEstado, String citPrecio, String citCalificacion) {
        this.idEmpleado = idEmpleado;
        this.idCita = idCita;
        this.citFecha = citFecha;
        this.citDireccion = citDireccion;
        this.citHoraInicio = citHoraInicio;
        this.citHoraFin = citHoraFin;
        this.idCliente = idCliente;
        this.idUsuario = idUsuario;
        this.usuFoto = usuFoto;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.citEstado = citEstado;
        this.citPrecio = citPrecio;
        this.citCalificacion = citCalificacion;
    }
    
    
    

    public EmpleadoVO() {
    }
    
    
}
