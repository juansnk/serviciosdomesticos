/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloVO;

/**
 *
 * @author usuario
 */
public class AgendaVO {
    
    private String idAgenda, ageFechaInicio, ageFechaFinal, empleado_idEmpleado;

    public String getIdAgenda() {
        return idAgenda;
    }

    public void setIdAgenda(String idAgenda) {
        this.idAgenda = idAgenda;
    }

    public String getAgeFechaInicio() {
        return ageFechaInicio;
    }

    public void setAgeFechaInicio(String ageFechaInicio) {
        this.ageFechaInicio = ageFechaInicio;
    }

    public String getAgeFechaFinal() {
        return ageFechaFinal;
    }

    public void setAgeFechaFinal(String ageFechaFinal) {
        this.ageFechaFinal = ageFechaFinal;
    }

    public String getEmpleado_idEmpleado() {
        return empleado_idEmpleado;
    }

    public void setEmpleado_idEmpleado(String empleado_idEmpleado) {
        this.empleado_idEmpleado = empleado_idEmpleado;
    }

    public AgendaVO(String idAgenda, String ageFechaInicio, String ageFechaFinal, String empleado_idEmpleado) {
        this.idAgenda = idAgenda;
        this.ageFechaInicio = ageFechaInicio;
        this.ageFechaFinal = ageFechaFinal;
        this.empleado_idEmpleado = empleado_idEmpleado;
    }

    public AgendaVO(String idAgenda) {
        this.idAgenda = idAgenda;
    }

    public AgendaVO() {
    }
    

   

    
    

   
    
    
}
