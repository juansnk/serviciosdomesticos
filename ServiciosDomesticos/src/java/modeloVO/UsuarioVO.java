/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloVO;

import java.io.InputStream;

/**
 *
 * @author Admin
 */
public class UsuarioVO {
    private String idUsuario, usuNombre, usuApellido, usuIdentificacion,usuFecha,
            usuCiudad, usuCorreo, usuTelefono, usuEstado, usuario, contrasena, rol_idRol, idEmpleado,
            empPromedio, empExperiencia, empDescripcion, empPrecio, usuario_idUsuario, idAgenda, ageFechaInicio,
            ageFechaFinal, empleado_idEmpleado, proDia, proHoraInicio, proHoraFin, idCliente, cliDireccion,
            cliLocalidad, cliBarrio;
    private InputStream usuFoto;

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public InputStream getUsuFoto() {
        return usuFoto;
    }

    public void setUsuFoto(InputStream usuFoto) {
        this.usuFoto = usuFoto;
    }
    

    public String getUsuNombre() {
        return usuNombre;
    }

    public void setUsuNombre(String usuNombre) {
        this.usuNombre = usuNombre;
    }

    public String getUsuApellido() {
        return usuApellido;
    }

    public void setUsuApellido(String usuApellido) {
        this.usuApellido = usuApellido;
    }

    public String getUsuIdentificacion() {
        return usuIdentificacion;
    }

    public void setUsuIdentificacion(String usuIdentificacion) {
        this.usuIdentificacion = usuIdentificacion;
    }

    public String getUsuFecha() {
        return usuFecha;
    }

    public void setUsuFecha(String usuFecha) {
        this.usuFecha = usuFecha;
    }

    public String getUsuCiudad() {
        return usuCiudad;
    }

    public void setUsuCiudad(String usuCiudad) {
        this.usuCiudad = usuCiudad;
    }

    public String getUsuCorreo() {
        return usuCorreo;
    }

    public void setUsuCorreo(String usuCorreo) {
        this.usuCorreo = usuCorreo;
    }

    public String getUsuTelefono() {
        return usuTelefono;
    }

    public void setUsuTelefono(String usuTelefono) {
        this.usuTelefono = usuTelefono;
    }

    public String getUsuEstado() {
        return usuEstado;
    }

    public void setUsuEstado(String usuEstado) {
        this.usuEstado = usuEstado;
    }
    

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getRol_idRol() {
        return rol_idRol;
    }

    public void setRol_idRol(String rol_idRol) {
        this.rol_idRol = rol_idRol;
    }

    public String getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(String idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getEmpPromedio() {
        return empPromedio;
    }

    public void setEmpPromedio(String empPromedio) {
        this.empPromedio = empPromedio;
    }

    public String getEmpExperiencia() {
        return empExperiencia;
    }

    public void setEmpExperiencia(String empExperiencia) {
        this.empExperiencia = empExperiencia;
    }

    public String getEmpDescripcion() {
        return empDescripcion;
    }

    public void setEmpDescripcion(String empDescripcion) {
        this.empDescripcion = empDescripcion;
    }

    public String getEmpPrecio() {
        return empPrecio;
    }

    public void setEmpPrecio(String empPrecio) {
        this.empPrecio = empPrecio;
    }
    
    public String getUsuario_idUsuario() {
        return usuario_idUsuario;
    }

    public void setUsuario_idUsuario(String usuario_idUsuario) {
        this.usuario_idUsuario = usuario_idUsuario;
    }

    public String getIdAgenda() {
        return idAgenda;
    }

    public void setIdAgenda(String idAgenda) {
        this.idAgenda = idAgenda;
    }
    
    public String getAgeFechaInicio() {
        return ageFechaInicio;
    }

    public void setAgeFechaInicio(String ageFechaInicio) {
        this.ageFechaInicio = ageFechaInicio;
    }

    public String getAgeFechaFinal() {
        return ageFechaFinal;
    }

    public void setAgeFechaFinal(String ageFechaFinal) {
        this.ageFechaFinal = ageFechaFinal;
    }

    public String getEmpleado_idEmpleado() {
        return empleado_idEmpleado;
    }

    public void setEmpleado_idEmpleado(String empleado_idEmpleado) {
        this.empleado_idEmpleado = empleado_idEmpleado;
    }
    

    public String getProDia() {
        return proDia;
    }

    public void setProDia(String proDia) {
        this.proDia = proDia;
    }

    public String getProHoraInicio() {
        return proHoraInicio;
    }

    public void setProHoraInicio(String proHoraInicio) {
        this.proHoraInicio = proHoraInicio;
    }

    public String getProHoraFin() {
        return proHoraFin;
    }

    public void setProHoraFin(String proHoraFin) {
        this.proHoraFin = proHoraFin;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliDireccion() {
        return cliDireccion;
    }

    public void setCliDireccion(String cliDireccion) {
        this.cliDireccion = cliDireccion;
    }

    public String getCliLocalidad() {
        return cliLocalidad;
    }

    public void setCliLocalidad(String cliLocalidad) {
        this.cliLocalidad = cliLocalidad;
    }

    public String getCliBarrio() {
        return cliBarrio;
    }

    public void setCliBarrio(String cliBarrio) {
        this.cliBarrio = cliBarrio;
    }
//13
    public UsuarioVO(String idUsuario,  String usuNombre, String usuApellido, String usuIdentificacion, String usuFecha, String usuCiudad, String usuCorreo, String usuTelefono, String usuEstado, String usuario, String contrasena, String rol_idRol) {
        this.idUsuario = idUsuario;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.usuIdentificacion = usuIdentificacion;
        this.usuFecha = usuFecha;
        this.usuCiudad = usuCiudad;
        this.usuCorreo = usuCorreo;
        this.usuTelefono = usuTelefono;
        this.usuEstado = usuEstado;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.rol_idRol = rol_idRol;
    }
    
//16
    public UsuarioVO(String idUsuario, String usuNombre, String usuApellido, String usuIdentificacion, String usuFecha, String usuCiudad, String usuCorreo, String usuTelefono, String usuario, String contrasena, String rol_idRol, String idCliente, String cliDireccion, String cliLocalidad, String cliBarrio, String Usuario_idUsuario) {
        this.idUsuario = idUsuario;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.usuIdentificacion = usuIdentificacion;
        this.usuFecha = usuFecha;
        this.usuCiudad = usuCiudad;
        this.usuCorreo = usuCorreo;
        this.usuTelefono = usuTelefono;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.rol_idRol = rol_idRol;
        this.idCliente = idCliente;
        this.cliDireccion = cliDireccion;
        this.cliLocalidad = cliLocalidad;
        this.cliBarrio = cliBarrio;
        this.usuario_idUsuario = usuario_idUsuario;
    }
  //19
    public UsuarioVO(String idUsuario, InputStream usuFoto, String usuNombre, String usuApellido, String usuIdentificacion, String usuFecha, String usuCiudad, String usuCorreo, String usuTelefono, String usuEstado, String usuario, String contrasena, String rol_idRol, String idEmpleado, String empPromedio, String empExperiencia, String empDescripcion,String empPrecio, String usuario_idUsuario) {
        this.idUsuario = idUsuario;
        this.usuFoto = usuFoto;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.usuIdentificacion = usuIdentificacion;
        this.usuFecha = usuFecha;
        this.usuCiudad = usuCiudad;
        this.usuCorreo = usuCorreo;
        this.usuTelefono = usuTelefono;
        this.usuEstado = usuEstado;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.rol_idRol = rol_idRol;
        this.idEmpleado = idEmpleado;
        this.empPromedio = empPromedio;
        this.empExperiencia = empExperiencia;
        this.empDescripcion = empDescripcion;
        this.empPrecio = empPrecio;
        this.usuario_idUsuario = usuario_idUsuario;
    }
    //23
    public UsuarioVO(String idUsuario,InputStream usuFoto, String usuNombre, String usuApellido, String usuIdentificacion, String usuFecha, String usuCiudad, String usuCorreo, String usuTelefono,String usuEstado, String usuario, String contrasena, String rol_idRol, String idEmpleado, String empPromedio, String empExperiencia, String empDescripcion,String empPrecio, String usuario_idUsuario, String idAgenda, String ageFechaInicio, String ageFechaFinal, String empleado_idEmpleado) {
        this.idUsuario = idUsuario;
        this.usuFoto = usuFoto;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.usuIdentificacion = usuIdentificacion;
        this.usuFecha = usuFecha;
        this.usuCiudad = usuCiudad;
        this.usuCorreo = usuCorreo;
        this.usuTelefono = usuTelefono;
        this.usuEstado = usuEstado;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.rol_idRol = rol_idRol;
        this.idEmpleado = idEmpleado;
        this.empPromedio = empPromedio;
        this.empExperiencia = empExperiencia;
        this.empDescripcion = empDescripcion;
        this.empPrecio = empPrecio;
        this.usuario_idUsuario = usuario_idUsuario;
        this.idAgenda = idAgenda;
        this.ageFechaInicio = ageFechaInicio;
        this.ageFechaFinal = ageFechaFinal;
        this.empleado_idEmpleado = empleado_idEmpleado;
    }
    
//11
    public UsuarioVO(String idUsuario, String usuNombre, String usuApellido, String usuIdentificacion, String usuFecha, String usuCiudad, String usuCorreo, String usuTelefono, String usuario, String contrasena, String rol_idRol) {
        this.idUsuario = idUsuario;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.usuIdentificacion = usuIdentificacion;
        this.usuFecha = usuFecha;
        this.usuCiudad = usuCiudad;
        this.usuCorreo = usuCorreo;
        this.usuTelefono = usuTelefono;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.rol_idRol = rol_idRol;
    }
//10
    public UsuarioVO(String idUsuario, InputStream usuFoto, String usuNombre, String usuApellido, String usuFecha, String usuCiudad, String empPromedio, String empDescripcion, String empPrecio, String idEmpleado) {
        this.idUsuario = idUsuario;
        this.usuFoto = usuFoto;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.usuFecha = usuFecha;
        this.usuCiudad = usuCiudad;   
        this.empPromedio = empPromedio;
        this.empDescripcion = empDescripcion;
        this.empPrecio = empPrecio;
        this.idEmpleado = idEmpleado;
    }
//15
    public UsuarioVO(String idUsuario,String idEmpleado, InputStream usuFoto,String usuNombre, String usuApellido, String usuFecha, String usuCiudad, String usuCorreo, String usuTelefono, String empPromedio, String empExperiencia, String empDescripcion, String idAgenda, String ageFechaInicio, String ageFechaFinal) {
        this.idUsuario = idUsuario;
        this.idEmpleado = idEmpleado;
        this.usuFoto = usuFoto;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.usuFecha = usuFecha;
        this.usuCiudad = usuCiudad;
        this.usuCorreo = usuCorreo;
        this.usuTelefono = usuTelefono;
        this.empPromedio = empPromedio;
        this.empExperiencia = empExperiencia;
        this.empDescripcion = empDescripcion;
        this.idAgenda = idAgenda;
        this.ageFechaInicio = ageFechaInicio;
        this.ageFechaFinal = ageFechaFinal;
    }

    public UsuarioVO(String proDia, String proHoraInicio, String proHoraFin) {
        this.proDia = proDia;
        this.proHoraInicio = proHoraInicio;
        this.proHoraFin = proHoraFin;
    }

    public UsuarioVO(String idUsuario, String usuNombre, String usuApellido, InputStream usuFoto) {
        this.idUsuario = idUsuario;
        this.usuNombre = usuNombre;
        this.usuApellido = usuApellido;
        this.usuFoto = usuFoto;
    }

    public UsuarioVO(String idUsuario, String usuCorreo) {
        this.idUsuario = idUsuario;
        this.usuCorreo = usuCorreo;
    }

    public UsuarioVO(String rol_idRol) {
        this.rol_idRol = rol_idRol;
    }
    

    public UsuarioVO() {
    }
    
    
}
