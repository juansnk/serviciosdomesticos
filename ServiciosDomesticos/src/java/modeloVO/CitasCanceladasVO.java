/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloVO;

/**
 *
 * @author Admin
 */
public class CitasCanceladasVO {
    private String idCita, fecha, direccion, horaInicio, horaFin, estado, cliente, fechaCancelada, descripcion;

    public String getIdCita() {
        return idCita;
    }

    public void setIdCita(String idCita) {
        this.idCita = idCita;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getFechaCancelada() {
        return fechaCancelada;
    }

    public void setFechaCancelada(String fechaCancelada) {
        this.fechaCancelada = fechaCancelada;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public CitasCanceladasVO(String idCita, String fecha, String direccion, String horaInicio, String horaFin, String estado, String cliente, String fechaCancelada, String descripcion) {
        this.idCita = idCita;
        this.fecha = fecha;
        this.direccion = direccion;
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
        this.estado = estado;
        this.cliente = cliente;
        this.fechaCancelada = fechaCancelada;
        this.descripcion = descripcion;
    }

    public CitasCanceladasVO(String idCita, String descripcion) {
        this.idCita = idCita;
        this.descripcion = descripcion;
    }

    public CitasCanceladasVO(String idCita) {
        this.idCita = idCita;
    }
    
    public CitasCanceladasVO() {
    }
    
    
    
}
