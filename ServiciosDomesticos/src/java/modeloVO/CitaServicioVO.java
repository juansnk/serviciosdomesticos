/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloVO;

/**
 *
 * @author Admin
 */
public class CitaServicioVO {
    private String idServicio [];
    private String idCita, idServicioo, cisCosto, serDescripcion, idCategoria, catNombre;

    public String getIdCita() {
        return idCita;
    }

    public void setIdCita(String idCita) {
        this.idCita = idCita;
    }

    
    public String[] getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(String[] idServicio) {
        this.idServicio = idServicio;
    }
    public String getIdServicioo() {
        return idServicioo;
    }

    public void setIdServicio(String idServicioo) {
        this.idServicioo = idServicioo;
    }

    public String getCisCosto() {
        return cisCosto;
    }

    public void setCisCosto(String cisCosto) {
        this.cisCosto = cisCosto;
    }

    public String getSerDescripcion() {
        return serDescripcion;
    }

    public void setSerDescripcion(String serDescripcion) {
        this.serDescripcion = serDescripcion;
    }

    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCatNombre() {
        return catNombre;
    }

    public void setCatNombre(String catNombre) {
        this.catNombre = catNombre;
    }
    

    public CitaServicioVO(String idCita , String idServicio [], String cisCosto) {
        this.idCita = idCita;
        this.idServicio = idServicio;
        this.cisCosto = cisCosto;
    }

    public CitaServicioVO(String idCita, String idServicioo, String serDescripcion) {
        this.idCita = idCita;      
        this.idServicioo = idServicioo;
        this.serDescripcion = serDescripcion;
    }

    public CitaServicioVO(String idCita, String idServicioo, String serDescripcion, String cisCosto) {
        this.idCita = idCita;
        this.idServicioo = idServicioo;
        this.serDescripcion = serDescripcion;       
        this.cisCosto = cisCosto;
    }
    

    public CitaServicioVO() {
    }
    
    
   
 
}
