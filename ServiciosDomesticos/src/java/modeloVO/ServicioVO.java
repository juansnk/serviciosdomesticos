/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloVO;

/**
 *
 * @author Admin
 */
public class ServicioVO {
     private String idServicio, serDescripcion, serCosto, categoria_idCategoria;

    public String getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(String idServicio) {
        this.idServicio = idServicio;
    }

    public String getSerDescripcion() {
        return serDescripcion;
    }

    public void setSerDescripcion(String serDescripcion) {
        this.serDescripcion = serDescripcion;
    }

    public String getCategoria_idCategoria() {
        return categoria_idCategoria;
    }

    public void setCategoria_idCategoria(String categoria_idCategoria) {
        this.categoria_idCategoria = categoria_idCategoria;
    }

    public String getSerCosto() {
        return serCosto;
    }

    public void setSerCosto(String serCosto) {
        this.serCosto = serCosto;
    }

    public ServicioVO(String idServicio, String serDescripcion, String serCosto, String categoria_idCategoria) {
        this.idServicio = idServicio;
        this.serDescripcion = serDescripcion;
        this.serCosto = serCosto;
        this.categoria_idCategoria = categoria_idCategoria;
    }

    
   
    public ServicioVO() {
    }
     
}

